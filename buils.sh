#!/usr/bin/env bash
rm -f *.js
rm -f *.woff
rm -f *.eot
rm -f *.ttf
rm -f *.svg
rm -f *.woff2
rm -f *.png
npm run-script build --prod
cp -R dist/* .
rm -rf dist
echo 'Done!
