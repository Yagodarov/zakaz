<!DOCTYPE html>
<html>
<head>

  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>Title</title>

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

  <style type="text/css">
    body {
      font-family: DejaVu Sans !important;
      font-size: 8pt;
      line-height: 90%;
    }
    td {
      border: 1px solid black;
    }
    table{
       width:100%;
    }
    #collapseTable {
      border-collapse: collapse;
    }
    .sign_text {
      width: 180px;
    }
    .stamp {
      width: 143px;
    }
    .row.extra-bottom-padding {
      margin-bottom: 15px;
    }
  </style>
</head>
<body>
<div class="row">
  <div class="text-center">
    <b>
      {{$legal->profile->name}}
    </b>
  </div>
</div>

<div class="row">
  <div class="text-center">
    <b>Детализация поездок трансфер</b>
  </div>
</div>

<div class="row extra-bottom-padding">
  <div class="text-center">
    <b>С {{$date_from}} по {{$date_to}}</b>
  </div>
</div>

<br>

<div class="row extra-bottom-padding">
  <table id="collapseTable">
    <tr>
      <td>Id</td>
      <td>Дата и время заказа</td>
      <td>Место отправления</td>
      <td>Место назначения</td>
      <td>Стоимость</td>
      <td>Автомобиль</td>
      <td>Водитель</td>
      <td>Тип оплаты</td>
      <td>Заказчик</td>
      <td>Статус</td>
    </tr>
	  <?
	  foreach ($orders as $order) {
	  ?>
    <tr>
      <td>{{$order['id']}}</td>
      <td>{{$order['datetime_order']}}</td>
      <td>{{$order['from']}}</td>
      <td>
        <div>
          @if (count($order['address']) === 1)
            {{$order['address'][0]['where']}}
          @elseif (count($order['address']) > 1)
            @foreach ($order['address'] as $key => $order_address)
              {{$order_address['where']}}@if (isset($order['address'][$key + 1])), @endif
            @endforeach
          @endif
        </div>
      </td>
      <td>{{$order['price']}}</td>
      <td>{{$order['car']['mark'].' '.$order['car']['model'].' '.$order['car']['color'].' '.$order['car']['car_number']}}</td>
      <td>
          {{$order['driver']['profile']['name']}}
      </td>
      <td>{{\App\Http\Models\Order::getPaymentLabels($order['payment'])}}</td>
      <td>{{$order['client']['profile']['name']}}</td>
      <td>{{\App\Http\Models\Order::getStatusLabels($order['status'])}}</td>
    </tr>
	  <?
	  }
	  ?>
  </table>
</div>

<div class="row">
  <br>
  <div>
    <p>Итого : {{$price}} ({{$message}}) {{$sumMessage}}</p>
    <br>
    <p>ООО "Трансфер Уфа"</p>
    <br>
    @if ($stamp_sign === 'true')
      <img class="sign_text" src="{{public_path('/prints/sign_text.png')}}"><img class="stamp" src="{{public_path('/prints/stamp.png')}}">
    @else
      <img class="sign_text" src="{{public_path('/prints/direktor_no_stamp.png')}}">
    @endif

  </div>
</div>

</body>
</html>
