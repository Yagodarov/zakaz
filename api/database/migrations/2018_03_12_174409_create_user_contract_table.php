<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;
use App\Http\Models\UserContract;
class CreateUserContractTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_contract', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('contract_number')->unsigned();
            $table->date('contract_period_start')->nullable();
            $table->date('contract_period_end')->nullable();
            $table->integer('request_price')->nullable();
            $table->integer('transfer')->nullable();
            $table->integer('order_cost')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestampsTZ();
        });

        $profiles = DB::table('profile')->get();
        foreach($profiles as $profile)
        {
            if ($profile->request_price 
                && $profile->order_cost 
                && $profile->transfer 
                && $profile->contract_period_start
                && $profile->contract_period_end
                && $profile->contract_number)
            {
                $userContract = new UserContract();
                $model = (array) $profile;
                $userContract->fill($model);
                $userContract->user_id = $profile->id;
                $userContract->save();
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_contract');
    }
}
