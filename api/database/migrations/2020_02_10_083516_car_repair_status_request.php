<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CarRepairStatusRequest extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('car_repairs', function (Blueprint $table) {
        	$table->increments('id');
        	
        	$table->text('comment');
		       
        	$table->integer('status');
        	
	        $table->integer('car_id')->unsigned();
        	$table->foreign('car_id')->references('id')->on('cars')->onDelete('restrict')->onUpdate('no action');
        	
	        $table->integer('changed_by_user_id')->unsigned()->nullable();
        	$table->foreign('changed_by_user_id')->references('id')->on('users')->onDelete('restrict')->onUpdate('no action');
        	
        	$table->integer('created_by_user_id')->unsigned();
	        $table->foreign('created_by_user_id')->references('id')->on('users')->onDelete('restrict')->onUpdate('no action');
        	
        	$table->timestampsTz();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('car_repairs');
    }
}
