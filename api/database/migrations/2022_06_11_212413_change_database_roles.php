<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeDatabaseRoles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $tableName = config('permission.table_names.model_has_roles');
        $modelHasRoles = DB::table($tableName)->get()->all();
        foreach ($modelHasRoles as $modelHasRole) {
            if ($modelHasRole->model_type === "App\Http\Models\User\User") {
                DB::table($tableName)->where([
                    'role_id' => $modelHasRole->role_id,
                    'model_id' => $modelHasRole->model_id
                ])->update([
                    'model_type' => \App\Http\Models\Site\User\User::class
                ]);
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
