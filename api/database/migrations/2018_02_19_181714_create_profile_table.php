<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profile', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('requisites');
            $table->string('phone');
            $table->string('address');
            $table->string('legal_address');
            $table->date('contract_period_start')->nullable();
            $table->date('contract_period_end')->nullable();
            $table->integer('contract_number')->nullable();
            $table->integer('weekend')->nullable();
            $table->integer('request_price')->nullable();
            $table->integer('transfer')->nullable();
            $table->integer('order_cost')->nullable();
            $table->foreign('id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profile');
    }
}
