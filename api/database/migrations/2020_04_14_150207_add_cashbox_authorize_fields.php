<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCashboxAuthorizeFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('options', function (Blueprint $table) {
            $table->string('cash_box_login');
            $table->string('cash_box_password');
            $table->string('cash_box_company_id');
            $table->string('cash_box_request_source');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('options', function (Blueprint $table) {
            $table->dropColumn('cash_box_login');
            $table->dropColumn('cash_box_password');
            $table->dropColumn('cash_box_company_id');
            $table->dropColumn('cash_box_request_source');
        });
    }
}
