<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Http\Models\User\User;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::all();
        foreach($users as $user)
        {
        	$role = @$user->getRoleNames()[0];
        	if (!$role)
        	{
        		$user->assignRole('driver');
        		$msg = 'User with id '.$user->id.' assigned to driver';
        		var_dump($msg);
        	}
        }
    }
}
