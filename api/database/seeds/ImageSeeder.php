<?php

use Illuminate\Database\Seeder;

class ImageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cars = \App\Http\Models\Car::all();
        foreach($cars as $car)
        {
            if ($car->image)
            {
                $car->image = 'image.jpg';
            }
        }
    }
}
