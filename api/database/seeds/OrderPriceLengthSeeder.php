<?php

use Illuminate\Database\Seeder;
use App\Http\Models\Order;

class OrderPriceLengthSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$orders = Order::all();
		foreach ($orders as $order) {
			// $addresses[] = ['name' => $order->from];
			$params = $order->getAttributes();
			$params['datetime_order']  = time();
			foreach(DB::table('order_address')->where('order_id', $order->id)->get() as $address)
			{
				$addresses[] = ['name' => $address->where];
			}
			$params['addresses'] = $addresses;

			if (is_null($order->length))
			{
				if ($order->scenario = 'legal')   	
				{				
					$order->price = $order->getPrice($params);
				}
				else
				{
					$order->price = $order->getDriverPrice($params);
				}
			}

			// unset($order->addresses);
			var_dump($order['length']);
			$order->save();
		}   
    }
}
