<?php

use App\Http\Models\Site\SmsSettings;
use Illuminate\Database\Seeder;

class CreateSmsSettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        DB::table('sms_settings')->truncate();
        $event_id = 0;
        $sms_statuses = [
          'Новый',
          'В обработке',
          'Принят',
          'На исполнении',
          'Выполнен',
          'Отменен',
          'Отменен с оплатой',
          'Ожидает подтверждения водителем',
          'Отменен водителем',
        ];
        foreach ($sms_statuses as $sms_status) {
        	$smsSetting = SmsSettings::where('event', $sms_status)->first();
        	if (!$smsSetting) {
		        $smsSetting = (new SmsSettings())->fill([
			        'event' => $sms_status,
			        'template' => SmsSettings::getExampleSmsMessage(),
			        'event_id' => $event_id
		        ]);
	        }
          $smsSetting->save();
          $event_id++;
        }
    }
}
