<?php

/**
 * Created by PhpStorm.
 * User: User
 * Date: 09.12.2017
 * Time: 22:10
 */
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolesAndPermissionsSeeder extends Seeder
{
    public function run(){
        app()['cache']->forget('spatie.permission.cache');

        DB::statement('SET FOREIGN_KEY_CHECKS=0');

        DB::table('roles')->truncate();
        $role= Role::create(['name' => 'admin']);
        $role= Role::create(['name' => 'driver']);
        $role= Role::create(['name' => 'driver-manager']);
        $role= Role::create(['name' => 'legal']);
        $role= Role::create(['name' => 'client']);

        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
