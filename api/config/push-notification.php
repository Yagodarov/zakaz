<?php

return [

    'appNameIOS'     => [
        'environment' =>'development',
        'certificate' =>'/path/to/certificate.pem',
        'passPhrase'  =>'password',
        'service'     =>'apns'
    ],
    'appNameAndroid' => [
        'environment' =>'production',
        'apiKey'      =>'AIzaSyA45KuAg6ujedPWTF1e7zUmz2YXvzBns_M',
        'service'     =>'gcm'
    ]

];
