<?php

namespace App\Http\Requests\API\v4;

use Auth;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;

class UpdateCarTariff extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
	    return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      $request = $this->request;
        return [
          'datetime_order' => ['required', 'date', function($attribute, $value, $fail) use ($request) {
            $user = Auth::user()->hasRole('client');
            if (!$user)
            {
                return $fail('Пользователь не является клиентом');
            }
          },],
          'number' => 'required',
          'from' => 'required',
          'addresses' => ['required', function($attribute, $value, $fail) {
            if (is_array($value))
            {
              foreach($value as $item)
              {
                if ($item['name'] != '')
                  return true;
              }
            }
            else
              return $fail("Заполните адрес(а) назначения");
            return $fail("Заполните адрес(а) назначения");
          },],
          'comment' => 'sometimes',
          'driver_id' => 'sometimes|numeric|nullable',
          'car_id' => ['sometimes', function($attribute, $value, $fail) use($request){
            if ($request->get('driver_id') && ($request->get('car_id') == ''))
              return $fail("Заполните это поле");
          }]
        ];
    }

	protected function failedValidation(Validator $validator) {
    	throw new HttpResponseException(response()->json([
    	  'errors' => $validator->errors()->all(),
        'status' => 'failure'
      ], 403));
    }
}
