<?php

namespace App\Http\Requests\v4;

use App\Http\Models\UserCars;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;

class UpdateCarTariff extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
	    return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => ['required', function($attribute, $value, $fail) {
	            $user_id = \Auth::user()->id;
	            $user_car = UserCars::where([
		            ['user_id', '=', $user_id],
		            ['car_id', '=', $value],
	            ])->first();
	            if (!$user_car) {
	            	$fail("Этот авто не найден у пользователя");
	            }
            }],
            'request_price' => 'required|integer',
            'transfer' => 'required|integer',
            'order_cost' => 'required|integer',
            'rent_price' => 'sometimes|integer',
            'repairing' => 'required|boolean',
            'comment' => 'sometimes|string|nullable',
        ];
    }

	protected function failedValidation(Validator $validator) {
    	throw new HttpResponseException(response()->json($validator->errors()->all(), 403));
    }
}
