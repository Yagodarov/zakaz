<?php

namespace App\Http\Requests\v4;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Validator as ModelValidator;
use Illuminate\Contracts\Validation\Validator;

class MassStoreSchedule extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
	    return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'schedules' => ['required', function($attribute, $values, $fail) {
//	            foreach ($values as $value) {
//	            	$schedule = new v3Schedule();
//	            	$validator = ModelValidator::make($value, $schedule->rules('POST'), $schedule->messages());
//		            if ($validator->fails()) {
//		            	$fail($validator->messages()->all()[0]);
//		            }
//	            }
            }]
        ];
    }
	
	protected function failedValidation(Validator $validator) {
    	throw new HttpResponseException(response()->json('validation error of v3MassStoreSchedule request', 403));
    }
}
