<?php


namespace App\Http\Requests\Site\Car;

use App\Http\Models\Site\Car\CarRepair;
use App\Http\Models\UserCars;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;

class CarRepairCreateFormRequest extends FormRequest
{
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}
	
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'car_id' => ['required', 'exists:cars,id', function($attribute, $carId, $fail) {
				if (!$this->determineAvailabilityToCreateStatusChangeRequestByCarId($carId)) {
					return $fail('Автомобиль не принадлежит водителю');
				} elseif ($this->getIsThereIsAnotherStatusChangeRequestCreatedButNotReviewed($carId)) {
					return $fail('Заявка на ремонт уже отправлена');
				}
			}]
		];
	}
	
	protected function determineAvailabilityToCreateStatusChangeRequestByCarId($carId) {
		$userId = \Auth::user()->getAuthIdentifier();
		$userCar = UserCars::where([
			['user_id', '=', $userId],
			['car_id', '=', $carId]
		])->first();
		return $userCar;
	}
	
	protected function getIsThereIsAnotherStatusChangeRequestCreatedButNotReviewed($carId) {
		$carStatusChangeRequest = CarRepair::where([
			['car_id', '=', $carId],
			['status', '=', CarRepair::STATUS_REQUESTED]
		])->first();
		if ($carStatusChangeRequest) {
			return true;
		} else {
			return false;
		}
	}
	
	protected function failedValidation(Validator $validator)
	{
		throw new HttpResponseException(response()->json([
			'status' => 'failure',
			'messages' => $validator->errors()->all()
		], 403));
	}
}
