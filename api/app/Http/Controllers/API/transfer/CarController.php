<?php

namespace App\Http\Controllers\API\transfer;

use App\Http\Controllers\Controller;
use App\Http\Models\transfer\Car;
use Illuminate\Http\Request;

class CarController extends Controller
{
    public function getCarsForOrder(Request $request)
    {
        return Car::searchOrdering($request);
    }

    public function getCarsForOrderTransfer(Request $request)
    {
        return Car::searchOrderingTransfer($request);
    }
}
