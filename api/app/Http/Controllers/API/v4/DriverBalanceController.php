<?php


namespace App\Http\Controllers\API\v4;


use App\Http\Controllers\Site\Controller;
use App\Http\Models\API\v4\DriverBalance;
use Illuminate\Http\Request;

class DriverBalanceController extends Controller
{
	public function list(Request $request) {
		$driverBalance = (new DriverBalance())->search($request);
		if ($driverBalance) {
			return response()->json([
				'status' => 'success',
				'data' => $driverBalance
			], 200);
		} else {
			return response()->json([
				'status' => 'failure',
				'message' => 'Не удалось получить список',
				'data' => $driverBalance
			], 422);
		}
	}
}
