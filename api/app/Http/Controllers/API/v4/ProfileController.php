<?php

namespace App\Http\Controllers\API\v4;

use App\Http\Controllers\Controller;
use App\Http\Models\User\Profile;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
	public function index(Request $request)
	{
		return Profile::search($request);
	}

	public function get($id)
	{
		$model = Profile::find($id);
		return response()->json($model);
	}

	public function store(Request $request)
	{
		return (new Profile())->store($request);
	}

	public function update(Request $request)
	{
		$model = Profile::find($request->get('id'));
		return ($model->storeUpdate($request));
	}

}
