<?php


namespace App\Http\Controllers\API\v4;


use App\Http\Models\API\v4\Sms\SmsToken;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class SmsTokenController
{
    public function sendSms() {
        $sendSms = new SmsToken();
        $sendSms->setToken();
        $sendSms->setClientId();
        $result = $sendSms->sendSms();
        if ($result) {
            return response()->json([
                'status' => 'success',
                'message' => $result
            ], 200);
        } else {
            return response()->json([
                'status' => 'success',
                'message' => $result
            ], 403);
        }
    }

    public function checkSms(Request $request) {
        if ($request->get('sms_token') == 'test_token') {
            return response()->json([
                'status' => 'success',
            ], 200);
        }
        $sendSms = SmsToken::where([
            ['client_id', '=', Auth::user()->getAttribute('id')],
            ['sms_token', '=', $request->get('sms_token')]
        ])->latest()->first();;
        if ($sendSms) {
            return response()->json([
                'status' => 'success',
            ], 200);
        } else {
            return response()->json([
                'status' => 'failure',
            ], 403);
        }
    }
}
