<?php

namespace App\Http\Controllers\API\v4;

use App\Http\Controllers\Site\Controller;
use App\Http\Models\Api\v4\User\User;
use Illuminate\Http\Request;


class UserController extends Controller
{
    public function index(Request $request)
    {
		return User::search($request);
    }

    public function getDrivers(Request $request)
    {
        return User::getDrivers($request);
    }

	public function get($id)
	{
		$user = User::find($id)->with('cars');
		$user->role = $user->getRoleNames()[0];
		return response()->json($user);
	}

    public function store(Request $request)
    {
	    return (new User())->store($request);
    }

    public function update(Request $request)
    {
        $user = User::find($request->get('id'));
	    return ($user->storeUpdate($request));
    }

    public function delete($id)
    {
	    return response()->json(User::find($id)->delete());
    }

    public function legalList(Request $request)
    {
        return User::getLegalsList($request);
    }
}
