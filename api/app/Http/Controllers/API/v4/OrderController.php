<?php

namespace App\Http\Controllers\API\v4;

use App\Http\Controllers\Site\Controller;
use App\Http\Models\v4\Order\Client\Order as ClientOrder;
use App\Http\Models\v4\Order\Order;
use App\Http\Requests\v4\OrderForStatusRequest;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    public function index(Request $request)
    {
        return Order::searchPhone($request);
    }

    public function indexClient(Request $request)
    {
        return ClientOrder::searchClient($request);
    }

    public function indexClientTrue(Request $request)
    {
        return ClientOrder::searchClientTrue($request);
    }

    public function createRent(Request $request)
    {
        return (new ClientOrder())->storeRent($request);
    }

    public function statusClient(Request $request)
    {
        if ($order = ClientOrder::where([
            ['id', '=', $request->get('id')],
            ['client_id', '=', Auth::user()->getAttribute('id')]
        ])->first()) {
            $order->setAttribute('status', $request->get('status'));
            if ($result = $order->save()) {
                return response()->json([
                    "status" => "success",
                ], 200);
            }
        }
        else {
            $result = "Не найден заказ с таким номером";
        }
        return response()->json([
            "status" => "failure",
            "errors" => [$result]
        ], 403);
    }

    public function createClient(Request $request)
    {
        return (new ClientOrder())->store($request);
    }

    public function getById(Request $request)
    {
        $model = Order::find($request->get('id'));
        if (!$model) {
            return response()->json([
                'status' => 'failure',
                'messages' => [
                    'Заказ не найден'
                ]
            ]);
        } else {
            $model->driver->profile;
            $model->car;
            $model->addresses = DB::table('order_address')
                ->select('where')
                ->where('order_id', '=', $request->get('id'))
                ->get();
            return response()->json([
                'model' => $model,
                'options' => [
                    'status' => Order::getStatusArray()
                ],
                'status' => 'success'
            ]);
        }
    }

    public function cancelOrder($id)
    {
        return (Order::find($id)->setCancelled());
    }

    public function status(Request $request)
    {
        if ($order = OrderForStatusRequest::find($request->get('id')))
            return ($order->changeStatus($request));
        else
            return response()->json([
                "status" => "failure",
                "errors" => ["Не найден заказ с таким номером"]
            ], 403);
    }

    public function declineOrder(Request $request)
    {
        $model = Order::find($request->get('id'));
        if (($model->decline($request))) {
            return response()->json(true, 200);
        } else {
            return response()->json([
                "status" => "failure",
                "errors" => ["Не найден заказ с таким номером"]
            ], 403);
        }
    }

    public function declineOrderClient(Request $request)
    {
        $model = ClientOrder::find($request->get('id'));
        if (($model->declineClient($request))) {
            return response()->json(true, 200);
        } else {
            return response()->json([
                "status" => "failure",
                "errors" => ["Не найден заказ с таким номером"]
            ], 403);
        }
    }
    public function delete($id)
    {
        return response()->json(Order::find($id)->delete());
    }
}
