<?php

namespace App\Http\Controllers\API\v4\Auth;

include_once "SMSC.php";

use App\Http\Controllers\Site\Controller;
use App\Http\Models\Api\v4\User\Profile;
use App\Http\Models\Api\v4\User\User;
use Auth;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Tymon\JWTAuth\Exceptions\JWTException;

class LoginController extends Controller
{
	public function allowedToLogin($user)
	{
		foreach ($user->getRoleNames()->toArray() as $roleName) {
			if (in_array($roleName, $this->allowRoleNames()))
				return true;
		}
		return false;
	}

	public function allowRoleNames()
	{
		return [
			'admin',
			'legal',
			'driver-manager',
			'driver'
		];
	}

	public function setPushToken(Request $request)
	{
		$validator = Validator::make(Input::all(), [
			'token' => 'required'], $this->messages());
		if ($validator->fails()) {
			return response()->json($validator->errors()->all(), 403);
		} else {
			try {
				$token = $request->get('token');
				$currentUserId = Auth::user()->id;
				$profile = Profile::where([
					['id', '=', $currentUserId],
				])->first();
				$profile->token = $token;
				return response()->json($profile->save(), 200);
			} catch (Exception  $er) {
				return response()->json(["Ошибка установления токена"], 403);
			}
		}

	}

	public function messages()
	{
		return [
			'required' => 'Заполните :attribute',
			'phone.required' => 'Заполните телефон',
			'code.required' => 'Заполните смс код',
			'token.required' => 'Заполните токен',
			'min' => 'Не менее :min символа(-ов)',
			'max' => 'Не более :max символа(-ов)',
			'unique' => 'Уже используется',
			'email' => 'Введите правильный формат email',
			'date' => 'Выберите дату',
			'numeric' => 'Заполните это поле'
		];
	}

	public function authenticatePhone(Request $request)
	{
		// grab credentials from the request
		$credentials = $request->only('phone', 'code', 'token');
		$validator = Validator::make(Input::all(), $this->rules($request), $this->messages());
		if ($validator->fails()) {
			return response()->json([
				'status' => 'failure',
				'errors' => $validator->errors()->all()
			], 403);
		} else {
			try {
				$phone_cutted = substr($request->get('phone'), -10);
				$profile = Profile::where([
					['phone', 'LIKE', "%{$phone_cutted}%"],
					['sms_code', '=', $request->get('code')]
				])->first();
				$token = $request->get('token');
				$profile->token = $token;
				$profile->save();
				$user = User::where('id', $profile->id)->with('profle')->with('cars')->first();
				if (!$token = auth()->login($user)) {
					return response()->json(['Невозможно создать токен'], 401);
				}
			} catch (JWTException $e) {
				// something went wrong whilst attempting to encode the token
				return response()->json(['Ошибка сервера'], 500);
			}
		}

		return response()->json([
			'token' => $token,
			'user' => $user
		]);
	}

	public function rules(Request $request)
	{
		return [
			'phone' => ["required", function ($attribute, $value, $fail) use ($request) {

				$phone_cutted = substr($value, -10);
				if (!$profile = DB::table('profile')->where('phone', 'LIKE', "%{$phone_cutted}%")->first()) {
					return $fail('Не найден пользователь с таким номером');
				} elseif (!$profile = DB::table('profile')->where([['phone', 'LIKE', "%{$phone_cutted}%"], ['sms_code', '=', $request->get('code')]])->first()) {
					return $fail('Не найден пользователь с таким номером');
				} else {
					$user = User::where('id', $profile->id)->first();
					if ($user->blocked) {
						return $fail('Пользователь заблокирован');
					}
				}
			}],
			'code' => ["required", function ($attribute, $value, $fail) use ($request) {
				$phone_cutted = substr($request->get('phone'), -10);
				if (!DB::table('profile')->where('sms_code', $value)->first()) {
					return $fail('Неправильный код');
				} elseif (!$profile = DB::table('profile')->where([
					['sms_code', '=', $value],
					['phone', 'LIKE', "%{$phone_cutted}%"]
				])->first()) {
					return $fail('Не найден пользователь с таким кодом');
				}
			}],
		];
	}

	public function authenticateSms(Request $request): \Illuminate\Http\JsonResponse
    {
		\Log::info($request);
		$phone_cutted = substr($request->get('phone'), -10);
		$profile = Profile::where('phone', 'LIKE', "%{$phone_cutted}%")->first();
		if ($profile) {
			$phone = $profile->phone;
			$smsCode = rand(1000, 9999);
			\Log::info($profile);
			$result = send_sms($phone, "Ваш код: " . $smsCode, 1);
			$profile->setAttribute('sms_code', $smsCode);
			$profile->save();
			\Log::info($profile);
			if ($result[0]) {
				try {
					$profile->setAttribute('sms_code', $smsCode);
					$profile->save();
				} catch (Exception $exception) {
					\Log::critical('auth sms error', $request);
					return response()->json([
						'message' => 'Не удалось сохранить профиль, лол',
						'status' => 'failure'
					], 200);
				}
				return response()->json(["Сообщение успешно отправлено"], 200);
			}
			return response()->json(["Ошибка! Сообщение не отправлено"], 403);
		} else {
			return response()->json([
				'status' => 'failure',
				'errors' => [
					"Не найден водитель с таким номером"
				]
			], 403);
		}
	}
}
