<?php

namespace App\Http\Controllers\API\v4;

use App\Http\Controllers\Site\Controller;
use App\Http\Models\Api\v4\Schedule;
use App\Http\Requests\v4\MassStoreSchedule;
use Illuminate\Http\Request;

class ScheduleController extends Controller
{
    public function index(Request $request)
    {
		return Schedule::search($request);
    }

	public function get(Request $request)
	{
        return Schedule::search($request);
	}

    public function store(Request $request)
    {
	    return (new Schedule())->store($request);
    }

    public function massStore(MassStoreSchedule $request) {
        return Schedule::batchInsert($request->get('schedules'));
    }

    public function update(Request $request)
    {
        $model = Schedule::find($request->get('id'));
	    return ($model->store($request));
    }

    public function delete(Request $request)
    {
	    return response()->json(Schedule::find($request->get('id'))->delete());
    }
}
