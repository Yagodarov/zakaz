<?php


namespace App\Http\Controllers\API\v4\Car;


use App\Http\Models\Site\Car\CarRepair;
use App\Http\Requests\Request;
use App\Http\Requests\Site\Car\CarRepairAcceptFormRequest;
use App\Http\Requests\Site\Car\CarRepairCreateFormRequest;
use App\Http\Requests\Site\Car\CarRepairDeclineFormRequest;

class CarRepairController
{
	public function search(Request $request) {
	
	}
	
	public function create(CarRepairCreateFormRequest $request) {
		return response()->json((new CarRepair())->createAndGetResult($request), 200);
	}
	
	public function accept(CarRepairAcceptFormRequest $request) {
		return response()->json((CarRepair::find($request->get('id')))->acceptAndGetResult(), 200);
	}
	
	public function decline(CarRepairDeclineFormRequest $request) {
		return response()->json((CarRepair::find($request->get('id')))->declineAndGetResult(), 200);
	}
}
