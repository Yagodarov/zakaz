<?php

namespace App\Http\Controllers\Site\Car;

use App\Http\Controllers\Site\Controller;
use App\Http\Models\Site\Car\Car;
use Illuminate\Http\Request;

class CarController extends Controller
{
	public function index(Request $request)
	{
		return Car::search($request);
	}

	public function getCarsForOrder(Request $request)
	{
		return Car::searchOrdering($request);
	}

	public function get($id)
	{
		$car = Car::find($id);
		return response()->json($car);
	}

	public function getCarsByOrderId($id)
	{
		return response()->json(Car::getCarsByOrderId($id));
	}

	public function store(Request $request)
	{
		return (new Car())->store($request);
	}

	public function update(Request $request)
	{
		$car = Car::find($request->get('id'));
		return $car->storeUpdate($request);
	}

	public function delete($id)
	{
		return response()->json(Car::find($id)->delete());
	}
}
