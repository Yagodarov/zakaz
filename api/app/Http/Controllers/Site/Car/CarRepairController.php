<?php


namespace App\Http\Controllers\Site\Car;


use App\Http\Models\Site\Car\CarRepair;
use App\Http\Models\Site\Car\CarRepairSearch;
use App\Http\Requests\Site\Car\CarRepairAcceptFormRequest;
use App\Http\Requests\Site\Car\CarRepairCreateFormRequest;
use App\Http\Requests\Site\Car\CarRepairDeclineFormRequest;
use App\Http\Requests\Site\Car\CarRepairDeleteFormRequest;
use Illuminate\Http\Request;

class CarRepairController
{
	public function index(Request $request) {
		return response()->json((new CarRepairSearch($request))->search(), 200);
	}

	public function create(CarRepairCreateFormRequest $request) {
		$status = (new CarRepair())->createAndGetResult($request);
		return response()->json([
			'status' => $status
		], 200);
	}


	public function delete(CarRepairDeleteFormRequest $request) {
		$status = (CarRepair::find($request->get('id')));
		return response()->json([
			'status' => $status
		], 200);
	}

	public function accept(CarRepairAcceptFormRequest $request) {
		$status = (CarRepair::find($request->get('id')))->acceptAndGetResult();
		return response()->json($status, 200);
	}

	public function decline(CarRepairDeclineFormRequest $request) {
		$status = (CarRepair::find($request->get('id')))->declineAndGetResult();
		return response()->json($status, 200);
	}
}
