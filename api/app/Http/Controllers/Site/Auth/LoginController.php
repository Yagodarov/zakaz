<?php

namespace App\Http\Controllers\Site\Auth;

include_once "SMSC.php";

use App\Http\Controllers\Site\Controller;
use App\Http\Models\Site\User\Profile;
use App\Http\Models\Site\User\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Tymon\JWTAuth\Exceptions\JWTException;

class LoginController extends Controller
{
	public function allowRoleNames(){
		return [
			'admin',
			'legal',
			'driver-manager'
		];
	}

	public function allowedToLogin(User $user){
        $roleNames = $user->getRoleNames()->toArray();
		foreach ($roleNames as $roleName) {
			if (in_array($roleName, $this->allowRoleNames()))
				return true;
		}
		return false;
	}
	public function authenticate(Request $request)
	{
		// grab credentials from the request
		$credentials = $request->only('name', 'password');
		try {
			$user = User::where('name', $request->get('name'))->first();
			if (!$user)
			{
				return response()->json('Неверные данные', 401);
			}
			if ( $this->allowedToLogin($user) )
			{
				if ($user->blocked)
				{
					return response()->json('Пользователь заблокирован', 401);
				}
				if (! $token = auth()->attempt($credentials)) {
					return response()->json('Неверные данные', 401);
				}
			}
			else
			{
				return response()->json('Нет доступа для этой роли', 401);
			}
		} catch (JWTException $e) {
			// something went wrong whilst attempting to encode the token
			return response()->json('Ошибка сервера', 500);
		}

		// all good so return the token
		$roles = $user->getRoleNames();
		return response()->json([
				'token' => $token,
				'roles' => $roles,
				'user' => [
						'id' => $user['id'],
						'name' => $user['name']
				],
		]);
	}


	public function setPushToken(Request $request)
	{
		$validator = Validator::make(Input::all(), [
				'token'=> 'required'], $this->messages());
		if ($validator->fails()) {
			return response()->json($validator->messages(), 403);
		}
		else
		{
			try
			{
				$token = $request->get('token');
				$currentUserId = \Auth::user()->id;
				$profile = Profile::where([
						['id', '=', $currentUserId],
				])->first();
				$profile->token = $token;
				return response()->json($profile->save(), 200);
			}
			catch (\Exception  $er)
			{
				return response()->json("Ошибка установления токена", 403);
			}
		}

	}

	public function authenticatePhone(Request $request)
	{
		// grab credentials from the request
		$credentials = $request->only('phone', 'code', 'token');
		$validator = Validator::make(Input::all(), $this->rules($request), $this->messages());
		if ($validator->fails()) {
			return response()->json($validator->messages(), 403);
		}
		else
		{
			try
			{
				$profile = Profile::where([
						['phone', '=', $request->get('phone')],
						['sms_code', '=', $request->get('code')]
				])->first();
				$token = $request->get('token');
				$profile->token = $token;
				$profile->save();
				$user = User::where('id', $profile->id)->first();
				if ($user->blocked)
				{
					return response()->json('Пользователь заблокирован', 403);
				}
				if (! $token = auth()->login($user))
				{
					return response()->json('Невозможно создать токен', 401);
				}
			}
			catch (JWTException $e)
			{
				// something went wrong whilst attempting to encode the token
				return response()->json('Ошибка сервера', 500);
			}
		}

		return response()->json([
				'token' => $token,
		]);
	}

	public function authenticateSms(Request $request)
	{
		if ($profile = (DB::table('profile')
				->select('profile.*')
				->where('phone', $request->get('phone'))
				->first()))
		{
			$phone = $profile->phone;
			$smsCode = rand(1000, 9999);
			$result = send_sms($phone, "Ваш код: ".$smsCode, 1);
			$profile = DB::table('profile')->where('phone', $phone)->update([
					'sms_code' => $smsCode
			]);
			if($result[0])
			{
				return response()->json("Сообщение успешно отправлено", 200);
			}
			{
				return response()->json("Ошибка! Сообщение не отправлено", 403);
			}
		}
		else
		{
			return response()->json("Не найден водитель с таким номером", 403);
		}
	}

	public function rules(Request $request)
	{
		return [
				'phone' => ["required", function($attribute, $value, $fail) use ($request) {
					if (!$profile = DB::table('profile')->where('phone', $value)->first())
					{
						return $fail('Не найден пользователь с таким номером');
					}
					elseif(!$profile = DB::table('profile')->where([['phone', '=',  $value], ['sms_code', '=', $request->get('code')]])->first())
					{
						return $fail('Не найден пользватель с такой парой телефон код');
					}
					elseif(!DB::table('users')->where([['id', '=', $profile->id]])->first())
					{
						return $fail('Пользователь удален');
					}
				}],
				'code' => ["required", function($attribute, $value, $fail) use ($request) {
					if (!DB::table('profile')->where('sms_code', $value)->first())
					{
						return $fail('Неправильный код');
					}
					elseif(!$profile = DB::table('profile')->where([['sms_code', '=',  $value], ['phone', '=', $request->get('phone')]])->first())
					{
						return $fail('Не найден пользватель с такой парой телефон код');
					}
					elseif(!DB::table('profile')->where([['sms_code', '=',  $value], ['phone', '=', $request->get('phone')]])->first())
					{
						return $fail('Не найден пользватель с такой парой телефон код');
					}
				}],
				'token' => ["required"],
		];
	}

	public function messages() {
		return [
				'required' => 'Заполните это поле',
				'min' => 'Не менее :min символа(-ов)',
				'max' => 'Не более :max символа(-ов)',
				'unique' => 'Уже используется',
				'email' => 'Введите правильный формат email',
				'date' => 'Выберите дату',
				'numeric' => 'Заполните это поле'
		];
	}
}
