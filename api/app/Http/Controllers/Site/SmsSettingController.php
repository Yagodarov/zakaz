<?php


namespace App\Http\Controllers\Site;


use App\Http\Models\Options;
use App\Http\Models\Site\SmsSettings;
use Illuminate\Http\Request;

class SmsSettingController
{
//  public function create(Request $request) {
//
//  }

  public function update(Request $request) {
    if ($result = SmsSettings::storeArray($request->get('templates'))) {
      $options = Options::latest()->first();
      if (!$options) {
      	$options = new Options();
      }
      if ($result = $options->store($request->get('settings'))) {
        return response()->json([
          'status' => 'success',
          'message' => 'Успешно сохранено'
        ], 200);
      } else {
        return response()->json([
          'status' => 'failure',
          'message' => 'Не удалось сохранить опции'
        ], 403);
      }
    } else {
      return response()->json([
        'status' => 'failure',
        'message' => 'Не удалось сохранить шаблоны смс'
      ], 403);
    }
  }

  public function list(Request $request) {
    $smsSettingsList = SmsSettings::search($request);
    $options = Options::orderBy('id', 'desc')->first();
    return response()->json([
      'sms_setting_list' => $smsSettingsList,
      'options' => $options
    ], 200);
  }
}
