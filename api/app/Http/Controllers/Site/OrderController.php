<?php

namespace App\Http\Controllers\Site;

use App\Http\Models\Site\Car\Car;
use App\Http\Models\Site\Order\Order;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class OrderController extends Controller
{
	public function index(Request $request)
	{
        $token = Auth::user()->getRoleNames()[0];
		if (strpos(auth()->payload()->get('iss'), 'auth-phone'))
		{
			return Order::searchPhone($request);
		}
		return Order::search($request);
	}

	public function get($id)
	{
		$model = Order::find($id);
		$model->addresses = DB::table('order_address')
			->select('where')
			->where('order_id', '=', $id)
            ->orderBy('id')
			->get();
		return response()->json($model);
	}

	public function getSelect($id)
	{
		$model = Order::find($id);

		$model->addresses = DB::table('order_address')
			->select('where')
			->where('order_id', '=', $id)
			->get();

		$datetime = $model->datetime_order;

		$cars = DB::table('cars')
			->select("cars.*")
			->leftJoin('user_cars', 'user_cars.car_id', '=', 'cars.id')
			->leftJoin('schedule as sc', 'sc.user_id', '=', 'user_cars.user_id')
			->where('user_cars.blocked', '=', '0')
			->whereRaw("sc.date_start <= '{$datetime}' AND '{$datetime}' <= sc.date_end")
			->get();

		$drivers = DB::table('users')
			->select('profile.*')
			->leftJoin('profile', 'profile.id', '=', 'users.id')
			->leftJoin('model_has_roles', 'model_has_roles.model_id', '=', 'users.id')
			->leftJoin('roles', 'model_has_roles.role_id', '=', 'roles.id')
			->where('roles.name', '=', 'driver')
			->get();

		$profile = $model->car_id ? Car::find($model->car_id)->first() : null;

		return response()->json([
			'model' => $model,
			'cars' => $cars,
			'drivers' => $drivers,
			'profile' => $profile
		]);
	}

	public function cancelOrder($id)
	{
		return (Order::find($id)->setCancelled());
	}

	public function status(Request $request)
	{
		if ($order = Order::find($request->get('id')))
			return ($order->changeStatus($request));
		else
			return response()->json(['id' => "Не найден заказ"], 403);
	}

	public function getPrice(Request $request)
	{
		return response()->json([
			'price' => ($order = new Order())->getPrice($request),
			'length' => $order->length,
			'serve_length' => $order->serve_length
		]);
	}

	public function getDriverPrice(Request $request)
	{
		return response()->json([
			'price' => ($order = new Order())->getDriverPrice($request),
			'length' => $order->length,
			'serve_length' => $order->serve_length
		]);
	}

	public function store(Request $request)
	{
		return (new Order())->store($request);
	}

	public function storeSelect(Request $request)
	{
		return (new Order())->storeSelect($request);
	}

	public function update(Request $request)
	{
		$model = Order::where('id', $request->get('id'))->first();
    	return $model->storeUpdate($request);
	}

	public function updateSelect(Request $request)
	{
		$model = Order::where('id', $request->get('id'))->first();
    	return $model->storeSelect($request);
	}

	public function declineDriver(Request $request)
	{
		$model = Order::find($request->get('id'));
    	return ($model->decline($request));
	}

	public function delete($id)
	{
		return response()->json(Order::find($id)->delete());
	}
}
