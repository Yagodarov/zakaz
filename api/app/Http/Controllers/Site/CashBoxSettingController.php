<?php


namespace App\Http\Controllers\Site;


use App\Http\Models\Options;
use App\Http\Models\Site\CashBoxSettings;
use Illuminate\Http\Request;

class CashBoxSettingController
{
    public function update(Request $request)
    {
        $result = CashBoxSettings::storeArray($request->get('templates'));
        if ($result['status'] == 'success') {
          $options = Options::latest()->first();
	        if (!$options) {
		        $options = new Options();
	        }
            if ($result = $options->store($request->get('cash_box_settings'))) {
                return response()->json([
                    'status' => 'success',
                    'message' => 'Успешно сохранено'
                ], 200);
            } else {
                return response()->json([
                    'status' => 'failure',
                    'message' => 'Не удалось сохранить опции'
                ], 403);
            }
        } else {
            return response()->json([
                'status' => 'failure',
                'message' => 'Не удалось сохранить шаблоны уведомлений',
                'data' => $result['data']
            ], 403);
        }
    }

    public function list(Request $request)
    {
        $cashSettingsList = CashBoxSettings::search($request);
        $options = Options::orderBy('id', 'desc')->first();
        return response()->json([
            'cash_box_settings' => $cashSettingsList,
            'options' => $options
        ], 200);
    }
}
