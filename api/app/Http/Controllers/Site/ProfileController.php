<?php

namespace App\Http\Controllers\Site;

use App\Http\Models\Site\User\Profile;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
	public function index(Request $request)
	{
		return Profile::search($request);
	}

	public function get($id)
	{
		$model = Profile::find($id);
		$model->role = $model->user->getRoleNames()[0];
		return response()->json($model);
	}

	public function store(Request $request)
	{
		return (new Profile())->store($request);
	}

	public function update(Request $request)
	{
		$model = Profile::find($request->get('id'));
		return ($model->storeUpdate($request));
	}

	// public function delete($id)
	// {
	// 	return response()->json(Profile::find($id)->delete());
	// }
}
