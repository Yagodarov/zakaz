<?php

namespace App\Http\Controllers\Site\User;

use App\Http\Controllers\Site\Controller;
use App\Http\Models\Site\User\User;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;


class UserController extends Controller
{
    public function index(Request $request)
    {
		return User::search($request);
    }

	public function get($id)
	{
		$user = User::find($id);
        $user->cars = $user->cars;
		$user->role = $user->getRoleNames()[0];
        $user->roles = $user->getRoleNames();
		return response()->json($user);
	}

    public function store(Request $request)
    {
	    return (new User())->store($request);
    }

    public function update(Request $request)
    {
        $user = User::find($request->get('id'));
	    return ($user->storeUpdate($request));
    }

    public function delete($id)
    {
	    return response()->json(User::find($id)->delete());
    }

    public function legalList(Request $request)
    {
        return User::getLegalsList($request);
    }
}
