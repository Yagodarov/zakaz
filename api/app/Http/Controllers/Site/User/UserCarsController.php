<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Site\Controller;
use App\Http\Models\UserCars;
use App\Http\Models\User\User;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\DB;

class UserCarsController extends Controller
{
    public function index(Request $request)
    {
		return UserCars::search($request);
    }

    public function block($id)
    {
        $car = UserCars::find($id);
        $car->blocked = !$car->blocked;
        return response()->json(($car->save()));
    }

    public function default($id)
    {
        $car = UserCars::find($id);
        return response()->json(($car->setDefault()));
    }

	public function get(Request $request)
	{
        return UserCars::search($request);
	}

    public function store(Request $request)
    {
	    return (new UserCars())->store($request);
    }

    public function update(Request $request)
    {
        $user = UserCars::find($request->get('id'));
	    return ($user->storeUpdate($request));
    }

    public function delete($id)
    {
	    return response()->json(UserCars::find($id)->delete());
    }
}
