<?php

namespace App\Http\Controllers\Site\User;

use App\Http\Controllers\Site\Controller;
use App\Http\Models\Site\UserContract;
use Illuminate\Http\Request;

class UserContractController extends Controller
{
    public function index(Request $request)
    {
		return UserContract::search($request);
    }

	public function get($id)
	{
        return response()->json($user = UserContract::find($id));
	}

    public function store(Request $request)
    {
	    return (new UserContract())->store($request);
    }

    public function update(Request $request)
    {
        $user = UserContract::find($request->get('id'));
	    return ($user->storeUpdate($request));
    }

    public function delete($id)
    {
	    return response()->json(UserContract::find($id)->delete());
    }
}
