<?php


namespace App\Http\Controllers\Site;


use App\Http\Controllers\Site\Controller;
use App\Http\Models\API\Site\Balance\DriverBalance;
use Illuminate\Http\Request;

class DriverBalanceController extends Controller
{
	public function list(Request $request) {
		$driverBalance = (new DriverBalance())->search($request);
		if ($driverBalance) {
			return response()->json([
				'status' => 'success',
				'data' => $driverBalance
			], 200);
		} else {
			return response()->json([
				'status' => 'failure',
				'errors' => ['Не удалось получить список'],
				'data' => $driverBalance
			], 422);
		}
	}

	public function store(Request $request) {
		$driverBalance = new DriverBalance();
		$result = $driverBalance->store($request);
		if ($result) {
			return response()->json([
				'status' => 'success',
				'data' => $driverBalance
			], 200);
		} else {
			return response()->json([
				'status' => 'failure',
				'errors' => $driverBalance->getMessages()
			], 403);
		}
	}
}
