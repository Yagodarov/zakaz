<?php


namespace App\Http\Controllers\Site;


use App\Http\Controllers\Site\Controller;
use App\Http\Models\Site\Client;
use Request;

class ClientController extends Controller
{
    public function index(Request $request)
    {
        return [
            'models' => Client::all([
                'id',
                'first_name',
                'last_name'
            ]),
            'status' => 'success'
        ];
    }
}
