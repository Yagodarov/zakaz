<?php


namespace App\Http\Controllers\Site;



use App\Http\Models\Reports;
use Illuminate\Http\Request;

class ReportsController extends Controller
{
  public function index(Request $request) {
    return response()->json(Reports::getDriverReports($request), 200);
  }
  public function legalPdf(Request $request) {
    return response()->json(Reports::getLegalReportsPdf($request), 200);
  }
  public function legalTable(Request $request) {
    return response()->json(Reports::getLegalTable($request), 200);
  }
  public function paymentOrDebt(Request $request) {
    return response()->json(Reports::paymentOrDebt($request), 200);
  }
}
