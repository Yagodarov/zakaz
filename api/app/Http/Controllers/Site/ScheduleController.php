<?php

namespace App\Http\Controllers\Site;

use App\Http\Models\Schedule;
use Illuminate\Http\Request;

class ScheduleController extends Controller
{
  public function index(Request $request)
  {
    return Schedule::search($request);
  }

  public function get(Request $request)
  {
    return Schedule::search($request);
  }

  public function store(Request $request)
  {
    return (new Schedule())->store($request);
  }

  public function update(Request $request)
  {
    $model = Schedule::find($request->get('id'));
    return ($model->store($request));
  }

  public function delete($id)
  {
    return response()->json(Schedule::find($id)->delete());
  }
}
