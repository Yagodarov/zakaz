<?php

namespace App\Http\Models\transfer;

use App\Http\Models\Car as BaseCar;
use App\Http\Models\UserCars;
use App\Http\Requests\v4\UpdateCarTariff;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Car extends BaseCar
{
	public $file;
	protected $table = 'cars';
	protected $fillable = [
		'request_price',
		'transfer',
		'order_cost',
		'rent_price',
		'repairing'
	];

	protected $hidden = [
		'commission'
	];

	protected $casts = [
		'baby_chair' => 'boolean',
		'conditioner' => 'boolean',
		'bank_card_payment' => 'boolean',
		'repairing' => 'boolean',
	];

	public function setRepairingAttribute($value) {
		$this->attributes['repairing'] = $value ? 1 : 0;
	}
    public static function searchOrdering(Request $request)
    {
        $rows =  DB::table('cars')
            ->when($request->get('datetime_order'), function($rows) use ($request) {
                $datetime = Carbon::createFromTimestamp($request->get('datetime_order')/1000);
                $cars = DB::table('cars')
                    ->select("cars.*")
                    ->leftJoin('user_cars', 'user_cars.car_id', '=', 'cars.id')
                    ->leftJoin('schedule as sc', 'sc.user_id', '=', 'user_cars.user_id')
                    ->where('user_cars.blocked', '=', '0')
                    ->whereRaw("sc.date_start <= '{$datetime}' AND '{$datetime}' <= sc.date_end")
                    ->get();
                if ($request->get('from'))
                {
                    $carIds = [];
                    foreach($cars as $key => $value)
                    {
                        $result = (new Order())->getLengthForTwo($request->get('from'), $value->address, $request->get('datetime_order')/1000);
                        if ($result['length'] < (max([$request->get('length'), 25000])))
                        {
                            $carIds[] = $value->id;
                        }
                    }
                    $rows->select("cars.*")
                        ->addSelect("profile.id as driver_id")
                        ->addSelect("profile.name as driver_name")
                        ->leftJoin('user_cars', 'user_cars.car_id', '=', 'cars.id')
                        ->leftJoin('profile', 'profile.id', '=', 'user_cars.user_id')
                        ->whereIn("cars.id", $carIds)
                        ->get();
                }
                else
                {
                    $rows->select("cars.*")
                        ->addSelect("profile.id as driver_id")
                        ->addSelect("profile.name as driver_name")
                        ->leftJoin('profile', 'profile.id', '=', 'user_cars.user_id')
                        ->leftJoin('user_cars', 'user_cars.car_id', '=', 'cars.id')
                        ->leftJoin('schedule as sc', 'sc.user_id', '=', 'user_cars.user_id')
                        ->where('user_cars.blocked', '=', '0')
                        ->whereRaw("sc.date_start <= '{$datetime}' AND '{$datetime}' <= sc.date_end")
                        ->get();
                }
            })
            ->when($request->get('payment') == 'card', function($rows) use ($request) {
                return $rows->where('cars.bank_card_payment', '=', '1');
            })
            ->when(!$request->get('orderBy'), function ($rows) use ($request) {
                return $rows
                    ->orderBy('cars.id', 'desc');
            })
            ->groupBy('id');

        $count = $rows->get()->count();
        $rows = $rows->get();
        foreach($rows as $row)
        {
            $newRequest = $request;
            $newRequest['car_id'] = $row->id;
            $order = new Order();
            $result = ($order)->getDriverPrice($newRequest);
            $row->price = $result['price'];
            $row->serve_time = $result['serve_time'];
            $row->serve_price = $order->serve_price;
        }
//        abort(403, $rows);
        return response()->json([
            'models' => $rows,
            'count' => $count,
            'status' => 'success'
        ]);
    }

    public static function searchOrderingTransfer(Request $request)
    {
        $rows =  self::when($request->get('datetime_order'), function($rows) use ($request) {
                $datetime = Carbon::createFromTimestamp($request->get('datetime_order')/1000);
                $cars = DB::table('cars')
                    ->select("cars.*")
                    ->leftJoin('user_cars', 'user_cars.car_id', '=', 'cars.id')
                    ->leftJoin('schedule as sc', 'sc.user_id', '=', 'user_cars.user_id')
                    ->where('user_cars.blocked', '=', '0')
                    ->whereRaw("sc.date_start <= '{$datetime}' AND '{$datetime}' <= sc.date_end")
                    ->get();
                if ($request->get('from'))
                {
                    $carIds = [];
                    foreach($cars as $key => $value)
                    {
                        $result = (new Order())->getLengthForTwo($request->get('from'), $value->address, $request->get('datetime_order')/1000);
                        if ($result['length'] < (max([$request->get('length'), 25000])))
                        {
                            $carIds[] = $value->id;
                        }
                    }
                    $rows->select("cars.*")
                        ->addSelect("profile.id as driver_id")
                        ->addSelect("profile.name as driver_name")
                        ->leftJoin('user_cars', 'user_cars.car_id', '=', 'cars.id')
                        ->leftJoin('profile', 'profile.id', '=', 'user_cars.user_id')
                        ->whereIn("cars.id", $carIds)
                        ->get();
                }
                else
                {
                    $rows->select("cars.*")
                        ->addSelect("profile.id as driver_id")
                        ->addSelect("profile.name as driver_name")
                        ->leftJoin('profile', 'profile.id', '=', 'user_cars.user_id')
                        ->leftJoin('schedule as sc', 'sc.user_id', '=', 'user_cars.user_id')
                        ->where('user_cars.blocked', '=', '0')
                        ->whereRaw("sc.date_start <= '{$datetime}' AND '{$datetime}' <= sc.date_end")
                        ->get();
                }
            })
            ->when($request->get('payment') == 'card', function($rows) use ($request) {
                return $rows->where('cars.bank_card_payment', '=', '1');
            })
            ->when(!$request->get('orderBy'), function ($rows) use ($request) {
                return $rows
                    ->orderBy('cars.id', 'desc');
            })
            ->groupBy('id');

        $count = $rows->get()->count();
        $rows = $rows->get();
        foreach($rows as $row)
        {
            if ($request->get('order_type') == 1) {
                $newRequest = $request;
                $newRequest['car_id'] = $row->id;
                $row->price = $row['rent_price'] * $request->get('rent_hours');

                $order = new \App\Http\Models\transfer\Order();
                $result = ($order)->getDriverPrice($newRequest);

                $row->serve_time = $result['serve_time'];

                $row->serve_price = ceil($result['serve_price']);
                $row->serve_price = ceil($row->serve_price / 50) * 50;
            }
            if ($request->get('order_type') == 0) {
                $newRequest = $request;
                $newRequest['car_id'] = $row->id;
                $order = new \App\Http\Models\transfer\Order();
                $result = ($order)->getDriverPrice($newRequest);
                $row->price = $result['price'];
                $row->price = ceil($row->price);
                $row->price = ceil($row->price / 50) * 50;

                $row->serve_time = $result['serve_time'];

                $row->serve_price = ceil($result['serve_price']);
                $row->serve_price = ceil($row->serve_price / 50) * 50;
            }
        }
        return response()->json([
            'models' => $rows,
            'count' => $count,
            'status' => 'success'
        ]);
    }

	public function rules(Request $request)
	{
		return [
			'request_price' => 'required|numeric',
			'transfer' => 'required|numeric',
			'order_cost' => 'required|numeric',
			'rent_price' => 'sometimes|numeric',
		];
	}

	public static function driverCars(Request $request) {
		$models = self::whereHas('UserCars', function ($query){
				$query->where('user_id', '=', Auth::user()->getAttribute('id'));
			})->get();
		return $models;
	}

	public function storeTariff(UpdateCarTariff $request)
	{
		$this->fill($request->all());
		if ($result = $this->save())
		{
			return response()->json($result, 200);
		}
		else
		{
			return response()->json($result, 403);
		}
	}

	public function UserCars() {
		return $this->hasMany(UserCars::class);
	}

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
}
