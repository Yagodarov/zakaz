<?php

namespace App\Http\Models\transfer;

use App\Http\Models\Car;
use App\Http\Models\Maps;
use App\Http\Models\Order\OrderSms;
use App\Http\Models\OrderAddress;
use App\Http\Models\PushNotification;
use App\Http\Models\Site\Report\Report;
use App\Http\Models\User\User;
use Config;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class Order extends Model
{
    const STATUS_NEW = 0;
    const STATUS_IN_PROCESSING = 1;
    const STATUS_ACCEPTED = 2;
    const STATUS_PERFORMING = 3;
    const STATUS_COMPLETED = 4;
    const STATUS_CANCELLED = 5;
    const STATUS_CANCELLED_WITH_PAYMENT = 6;
    const STATUS_WAITING_BY_DRIVER = 7;
    const STATUS_CANCELLED_BY_DRIVER = 8;
    protected $table = 'orders';


    const TIP_ZAKAZA_TRANSFER = 0;
    const TIP_ZAKAZA_RENT = 1;
    const TIP_ZAKAZA_DOSTAVKA = 2;

    protected $fillable = [
        'datetime_order', 'number', 'from', 'comment', 'payment', 'price',
        'legal_id', 'driver_id', 'car_id', 'status', 'driver_comment', 'scenario',
        'driver_payment', 'length', 'serve_length', 'client_id', 'rent_hours', 'order_type', 'activity'
    ];

    public static function getStatusLabels($id)
    {
        return self::getStatusArray()[$id];
    }

    public static function getStatusArray() {
        return [
            '0' => 'Новый',
            '1' => 'В обработке',
            '2' => 'Принят',
            '3' => 'На исполнении',
            '4' => 'Выполнен',
            '5' => 'Отменен',
            '6' => 'Отменен с оплатой',
            '7' => 'Ожидает подтверждения водителем',
            '8' => 'Отменен водителем',
        ];
    }

    public static function getPaymentLabels($id)
    {
        return [
            'cash' => 'Наличный',
            'noncash' => 'Безналичный',
            'card' => 'Банковская карта',
        ][$id];
    }

    public static function boot()
    {
        parent::boot();

        self::creating(function ($model) {
            // ... code here
        });

        self::created(function (Order $model) {
            $model->pushMessagesOnCreate($model);

            self::sendSmsByStatus($model);
            $model->sendCheck($model);
        });

        self::updating(function (Order $model) {
            if ($token = $model->isSetDriverToken($model))
                $model->pushMessageUpdating($model, $token);

        });
        self::updated(function (Order $model) {
            //Если водитель отменил заказ
            self::sendSmsByStatus($model);
            $model->sendCheck($model);
        });

        self::deleting(function ($model) {
            // ... code here
        });

        self::deleted(function ($model) {
            // ... code here
        });
    }

	public function sendCheck($model) {
		if ($model->getAttribute('status') == self::STATUS_COMPLETED && $model->getOriginal('status') != self::STATUS_COMPLETED) {
			(new Report($model))->sendReport();
		}
		if ($model->getAttribute('status') == self::STATUS_CANCELLED_WITH_PAYMENT && $model->getOriginal('status') != self::STATUS_CANCELLED_WITH_PAYMENT) {
			(new Report($model))->sendReport();
		}
	}

    public function pushMessagesOnCreate(Order $model)
    {
        if ($model->getAttribute('status') == 7) {
            if ($token = $model->isSetDriverToken($model)) {
                $model->pushMessageCreated($model, $token);
            }
        }
        if ($model->getAttribute('status') == 1) {
            $driver_managers = User::role('driver-manager')->get();
            foreach ($driver_managers as $driver_manager) {
                $token = $driver_manager->profile->token;
                $model->pushMessageCreated($model, $token);
            }
        }
    }

    public function isSetDriverToken($model)
    {
        $row = (DB::table('profile')->where('id', $model->driver_id)->first());
        if ($row && $row->token) {
            return $row->token;
        } else return false;
    }

    public function pushMessageCreated($model, $token)
    {
        $serverApiKey = Config::get("push-notification.appNameAndroid")["apiKey"];
        $message = "Новый заказ";
        $user = Auth::user();
        $log = [
            "message" => $message,
            "push-token" => $token,
            'id' => $model->id,
            "apiKey" => $serverApiKey,
            'actor' => 'client'
        ];
        Log::useDailyFiles(storage_path() . '/logs/push.log');
        Log::info('Отправка push сообщения', $log);

        (new PushNotification())->push($token, $message, 'Уведомление о заказе', $model->id, 'client');
    }

    public static function sendSmsByStatus($model)
    {
        try {
            new OrderSms($model);
        } catch (Exception $exception) {
            //something here
        }
    }

    public function pushMessageUpdating($model, $token)
    {
        if ((in_array($model->getOriginal('status'), [2, 3])) && $model->status == 5) {
            $serverApiKey = Config::get("push-notification.appNameAndroid")["apiKey"];
            $message = "Заказ отменен";
            $user = Auth::user();
            $log = [
                "message" => $message,
                "push-token" => $token,
                'id' => $model->id,
                "apiKey" => $serverApiKey,
                'actor' => 'client'
            ];

            Log::useDailyFiles(storage_path() . '/logs/push.log');
            Log::info('Отправка push сообщения', $log);

            (new PushNotification())->push($token, $message, 'Уведомление о заказе', $model->id, 'client');
        }
        if (($model->status == 2 && $model->driver_id) && ($model->isDirty('status') || $model->isDirty('driver_id'))) {
            $serverApiKey = Config::get("push-notification.appNameAndroid")["apiKey"];
            $message = "Новый заказ";
            $log = [
                "message" => $message,
                "push-token" => $token,
                'id' => $model->id,
                "apiKey" => $serverApiKey
            ];

            Log::useDailyFiles(storage_path() . '/logs/push.log');
            Log::info('Отправка push сообщения', $log);

            (new PushNotification())->push($token, $message, 'Уведомление о заказе', $model->id);
        }
    }

    public function pushMessageCancelled($model, $token)
    {

        $serverApiKey = Config::get("push-notification.appNameAndroid")["apiKey"];
        $message = "Заказ отменен";
        $user = Auth::user();
        $log = [
            "message" => $message,
            "push-token" => $token,
            'id' => $model->id,
            "apiKey" => $serverApiKey,
            'actor' => 'client'
        ];
        Log::useDailyFiles(storage_path() . '/logs/push.log');
        Log::info('Отправка push сообщения', $log);

        (new PushNotification())->push($token, $message, 'Уведомление о заказе', $model->id);
    }

    public function pushMessageCancelledByDriver(Order $model)
    {
        if ($model->status == 8 && $model->getOriginal('status') != 8) {
            $client = User::find($model->getAttribute('client_id'));
            if ($client) {
                if (isset($client->profile) && $token = $client->profile->getAttribute('token')) {
                    $serverApiKey = Config::get("push-notification.appNameAndroid")["apiKey"];
                    $message = "Заказ отменен водителем";
                    $user = Auth::user();
                    if ($user->hasRole('driver-manager')) {
                        $actor = 'driver-manager';
                    } elseif ($user->hasRole('driver')) {
                        $actor = 'driver';
                    } else {
                        $actor = $user->getRoleNames()[0];
                    }
                    $log = [
                        "message" => $message,
                        "push-token" => $token,
                        'id' => $model->id,
                        "apiKey" => $serverApiKey,
                        'actor' => $actor
                    ];
                    Log::useDailyFiles(storage_path() . '/logs/push.log');
                    Log::info('Отправка push сообщения', $log);

                    (new PushNotification())->push($token, $message, 'Уведомление о заказе', $model->id);
                }
            }
        }
    }

    public function getLengthForTwo($from, $to, $time)
    {
        return $this->getLength([$from, $to], $time);
    }

    public function getLength($addresses, $time)
    {
        return (new Maps())->getLength($addresses, $time);
    }

    public function changeStatus(Request $request)
    {
        $user = Auth::user();
        $validator = Validator::make($request->only('id', 'status', 'driver_comment'), [
            'id' => ['required', function ($attribute, $value, $fail) use ($user) {
                if (!($user->id == $this->driver_id)) {
                    return $fail("Заказ не принадлежит водителю");
                }
                return true;
            }],
            'driver_comment' => 'sometimes',
            'status' => ['required', function ($attribute, $value, $fail) {
                if ($this->status == 2) {
                    if (!($value == 3 || $value == 5)) {
                        return $fail("Невозможно изменить статус");
                    }
                } elseif ($this->status == 3) {
                    if (!($value == 4 || $value == 5)) {
                        return $fail("Невозможно изменить статус");
                    }
                } elseif ($this->status == 5) {
                    if (!($value == 5))
                        return $fail("Невозможно изменить статус");
                } else {
                    return $fail("Невозможно изменить статус");
                }
            }],
        ], $this->messages());
        if ($validator->fails()) {
            return response()->json($validator->messages(), 403);
        } else {
            $this->status = $request->get('status');
            $this->driver_comment = $request->get('driver_comment');
            return response()->json($this->save());
        }
    }

    public function messages()
    {
        return [
            'required' => 'Заполните это поле',
            'min' => 'Не менее :min символа(-ов)',
            'max' => 'Не более :max символа(-ов)',
            'unique' => 'Уже используется',
            'email' => 'Введите правильный формат email',
            'date' => 'Выберите дату',
            'numeric' => 'Заполните это поле'
        ];
    }

    public function store(Request $request)
    {
        $this->scenario = 'legal';
        $validator = Validator::make(Input::all(), $this->rules($request), $this->messages());
        if ($validator->fails()) {
            return response()->json($validator->messages(), 403);
        } else {
            $this->fill($request->only($this->fillable));
        }

        $this->status = 7;
        $this->setPrice($request);
        $this->setClientId();
        $this->setDriverPayment();
        $this->setLegalId();
        if ($this->price <= 0) {
            return response()->json([
                'status' => 'failure',
                'messages' => [
                    'Не удалось рассчитать стоимость заказа'
                ]
            ], 403);
        }

        if ($result = $this->save()) {
            $this->saveAdresses($request->get('addresses'));
            return response()->json([
                'status' => 'success',
                'model' => $this
            ], 200);
        } else
            return response()->json([
                'status' => 'failure',
                'messages' => [$result]
            ], 403);
    }

    public function storeRent(Request $request)
    {
        $this->scenario = 'legal';
        $validator = Validator::make(Input::all(), $this->rules($request), $this->messages());
        if ($validator->fails()) {
            return response()->json($validator->messages(), 403);
        } else {
            $this->fill($request->only($this->fillable));
        }

        $this->status = 7;
        $this->setClientId();
        $this->setDriverPayment();
        $this->setLegalId();
        if ($result = $this->save()) {
            return response()->json([
                'status' => 'success',
                'model' => $this
            ], 200);
        } else
            return response()->json([
                'status' => 'failure',
                'messages' => [$result]
            ], 403);
    }

    public function setLegalId() {
        $this->setAttribute('legal_id', 24);
    }

    public function rules(Request $request)
    {
        return [
            'datetime_order' => ['required', 'integer', function ($attribute, $value, $fail) use ($request) {
                $date = Carbon::createFromTimestamp($request->get('datetime_order'))->toDateTimeString();
                if (!$date) {
                    return "Неправильная дата";
                }
            }],
            'rent_hours' => ['required_if:order_type,1'],
            'order_type' => ['required', 'min:0', 'max:1'],
            'number' => 'required',
            'from' => 'required',
            'addresses' => ['required_if:order_type,0'],
            'comment' => 'sometimes',
            'driver_id' => 'sometimes|numeric|nullable',
            'car_id' => ['sometimes', function ($attribute, $value, $fail) use ($request) {
                if ($request->get('driver_id') && ($request->get('car_id') == ''))
                    return $fail("Заполните это поле");
            }]
        ];
    }

    public function decline(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'comment' => 'required'
        ], $this->messages());
        if ($validator->fails()) {
            response()->json($validator->errors(), 403);
        } else {
            $this->setAttribute('status', self::STATUS_CANCELLED);
            $this->fill($request->only('comment'));
            if (!$result = $this->save()) {
                return response()->json($result, 403);
            } else {
                return response()->json($result, 200);
            }

        }
        return response()->json("false", 403);
    }

    public function setClientId() {
        $this->setAttribute('client_id', Auth::user()->getAttribute('id'));
    }

    public function setPrice(Request $request) {
        $this->price = $this->getDriverPrice($request);
        $this->setAttribute('price', ceil($this->getAttribute('price')));
        $this->setAttribute('price', ceil($this->getAttribute('price') / 50) * 50);
    }

    public function setDriverPayment() {
        if ($this->getAttribute('order_type') == 0) {
            $price = $this->getAttribute('price') - ( $this->getAttribute('price') * $this->car->commission / 100 );
            $this->setAttribute('driver_payment', $price);
            $this->setAttribute('driver_payment', ceil($this->getAttribute('driver_payment')));
            $this->setAttribute('driver_payment', ceil($this->getAttribute('driver_payment') / 50) * 50);
        } else {
            $this->setAttribute('price', $this->getAttribute('rent_hours') * $this->car->rent_price);
            $price = $this->getAttribute('price') - ( $this->getAttribute('price') * $this->car->commission / 100 );
            $this->setAttribute('driver_payment', $price);
            $this->setAttribute('driver_payment', ceil($this->getAttribute('driver_payment')));
            $this->setAttribute('driver_payment', ceil($this->getAttribute('driver_payment') / 50) * 50);
        }
    }

    public function saveAdresses($addresses)
    {
        DB::table('order_address')->where('order_id', '=', $this->id)->delete();
        foreach ($addresses as $item) {
            DB::table('order_address')->insert(
                ['order_id' => $this->id, 'where' => $item['name']]
            );
        }
    }

    public function getDriverByCarId($id)
    {
        $row = DB::table('cars')->select('users.*')
            ->leftJoin('user_cars', 'user_cars.car_id', '=', 'cars.id')
            ->leftJoin('users', 'user_cars.user_id', '=', 'users.id')
            ->where('cars.id', '=', $id)
            ->first();
        return ($row->id);
    }


    public static function searchPhone(Request $request)
    {
        $rows =  DB::table('orders')
            ->select(
                'orders.id',
                'orders.car_id',
                'orders.comment',
                'orders.created_at',
                'orders.datetime_order',
                'orders.driver_comment',
                'orders.driver_id',
                'orders.from',
                'orders.legal_id',
                'orders.client_id',
                'orders.number',
                'orders.payment',
                'orders.price',
                'orders.length',
                'orders.serve_length',
                'orders.scenario',
                'orders.status',
                'orders.updated_at',
                'orders.driver_payment',
                DB::raw('(SELECT GROUP_CONCAT(`where` SEPARATOR " - ") as `to` FROM `order_address` WHERE `order_id` = orders.id) as `to`'),
                'profile.name as legal_name',
                DB::raw('CONCAT_WS(" ", cars.mark, cars.model, cars.color, cars.car_number) as car_name'),
                DB::raw("
					CONCAT_WS(
						' ',
						SUBSTRING_INDEX(SUBSTRING_INDEX(dp.name, ' ', 1), ' ', -1),
						SUBSTRING_INDEX(SUBSTRING_INDEX(dp.name, ' ', 2), ' ', -1)
					) as driver_name

					"),
                'dp.phone as driver_phone'
            )
            ->leftJoin('profile', 'profile.id', '=', 'orders.legal_id')
            ->leftJoin('profile as dp', 'dp.id', '=', 'orders.driver_id')
            ->leftJoin('cars', 'cars.id', '=', 'orders.car_id')
            ->when(Auth::user()->hasRole('driver') && !Auth::user()->hasRole('driver-manager'), function($rows) use ($request) {
                $user = Auth::user();
                return $rows
                    ->where('driver_id', '=', $user->id);
            })
            ->when($request->get('id'), function($rows) use ($request){
                return $rows->where('orders.id', '=', $request->id);
            })
            ->when($request->get('updated_at'), function($rows) use ($request){
                return $rows->where('orders.updated_at', '>', Carbon::createFromTimestamp(intval($request->get('updated_at')))->toDateTimeString());
            })
            ->when($request->get('datetime_order'), function($rows) use ($request){
                return $rows->where('orders.datetime_order', 'LIKE', "{$request->datetime_order}%");
            })
            ->when($request->get('from'), function($rows) use ($request){
                return $rows->where('orders.from', 'LIKE', "%{$request->from}%");
            })
            ->when($request->get('payment'), function($rows) use ($request){
                return $rows->where('orders.payment', '=', $request->payment);
            })
            ->when($request->get('to'), function($rows) use ($request){
                return $rows->whereIn('orders.id', function($rows) use ($request){
                    $rows->select('order_id')
                        ->from('order_address')
                        ->where('order_address.where', 'LIKE', "{$request->to}%");
                });
            })
            ->when($request->get('price'), function($rows) use ($request){
                return $rows->where('orders.price', '>=', $request->price);
            })
            ->when($request->get('price2'), function($rows) use ($request){
                return $rows->where('orders.price', '<=', $request->price2);
            })
            ->when($request->get('driver_payment'), function($rows) use ($request){
                return $rows->where('orders.driver_payment', '>=', $request->driver_payment);
            })
            ->when($request->get('driver_payment2'), function($rows) use ($request){
                return $rows->where('orders.driver_payment', '<=', $request->driver_payment2);
            })
            ->when($request->get('car_id'), function($rows) use ($request){
                return $rows->where('orders.car_id', '=', $request->car_id);
            })
            ->when($request->get('driver_id'), function($rows) use ($request){
                return $rows->where('orders.driver_id', '=', $request->driver_id);
            })
            ->when($request->get('legal_id'), function($rows) use ($request){
                return $rows->where('orders.legal_id', '=', $request->legal_id);
            })
            ->when($request->get('status') > -1, function($rows) use ($request){
                return $rows->where('orders.status', '=', $request->status);
            })
            ->when($request->get('orderBy'), function ($rows) use ($request) {
                return $rows
                    ->orderBy($request->get('orderBy'), $request->get('desc') == 'true' ? 'desc' : 'asc');
            })
            ->when(!$request->get('orderBy'), function ($rows) use ($request) {
                return $rows
                    ->orderBy('orders.datetime_order', 'desc');
            })
            ->groupBy('id');

        $count = $rows->get()->count();
        $rows = $rows->get();
        return response()->json([
            'models' => $rows,
            'count' => $count,
            "status"=> "success",
        ]);
    }

    public static function searchClient(Request $request)
    {
        $rows =  DB::table('orders')
            ->select(
                'orders.id',
                'orders.car_id',
                'orders.comment',
                'orders.created_at',
                'orders.datetime_order',
                'orders.driver_comment',
                'orders.driver_id',
                'orders.from',
                'orders.client_id',
                'orders.number',
                'orders.payment',
                'orders.price',
                'orders.length',
                'orders.serve_length',
                'orders.scenario',
                'orders.status',
                'orders.updated_at',
                'orders.driver_payment',
                DB::raw('(SELECT GROUP_CONCAT(`where` SEPARATOR " - ") as `to` FROM `order_address` WHERE `order_id` = orders.id) as `to`'),
                'profile.name as legal_name',
                DB::raw('CONCAT_WS(" ", cars.mark, cars.model, cars.color, cars.car_number) as car_name'),
                DB::raw("
					CONCAT_WS(
						' ',
						SUBSTRING_INDEX(SUBSTRING_INDEX(dp.name, ' ', 1), ' ', -1),
						SUBSTRING_INDEX(SUBSTRING_INDEX(dp.name, ' ', 2), ' ', -1)
					) as driver_name

					"),
                'dp.phone as driver_phone'
            )
            ->leftJoin('profile', 'profile.id', '=', 'orders.legal_id')
            ->leftJoin('profile as dp', 'dp.id', '=', 'orders.driver_id')
            ->leftJoin('cars', 'cars.id', '=', 'orders.car_id')
            ->where('orders.client_id', '=', Auth::user()->getAttribute('id'))
            ->when($request->get('id'), function($rows) use ($request){
                return $rows->where('orders.id', '=', $request->id);
            })
            ->when($request->get('updated_at'), function($rows) use ($request){
                return $rows->where('orders.updated_at', '>', Carbon::createFromTimestamp(intval($request->get('updated_at')))->toDateTimeString());
            })
            ->when($request->get('datetime_order'), function($rows) use ($request){
                return $rows->where('orders.datetime_order', 'LIKE', "{$request->datetime_order}%");
            })
            ->when($request->get('from'), function($rows) use ($request){
                return $rows->where('orders.from', 'LIKE', "%{$request->from}%");
            })
            ->when($request->get('payment'), function($rows) use ($request){
                return $rows->where('orders.payment', '=', $request->payment);
            })
            ->when($request->get('to'), function($rows) use ($request){
                return $rows->whereIn('orders.id', function($rows) use ($request){
                    $rows->select('order_id')
                        ->from('order_address')
                        ->where('order_address.where', 'LIKE', "{$request->to}%");
                });
            })
            ->when($request->get('price'), function($rows) use ($request){
                return $rows->where('orders.price', '>=', $request->price);
            })
            ->when($request->get('price2'), function($rows) use ($request){
                return $rows->where('orders.price', '<=', $request->price2);
            })
            ->when($request->get('driver_payment'), function($rows) use ($request){
                return $rows->where('orders.driver_payment', '>=', $request->driver_payment);
            })
            ->when($request->get('driver_payment2'), function($rows) use ($request){
                return $rows->where('orders.driver_payment', '<=', $request->driver_payment2);
            })
            ->when($request->get('car_id'), function($rows) use ($request){
                return $rows->where('orders.car_id', '=', $request->car_id);
            })
            ->when($request->get('driver_id'), function($rows) use ($request){
                return $rows->where('orders.driver_id', '=', $request->driver_id);
            })
            ->when($request->get('legal_id'), function($rows) use ($request){
                return $rows->where('orders.legal_id', '=', $request->legal_id);
            })
            ->when($request->get('status') > -1, function($rows) use ($request){
                return $rows->where('orders.status', '=', $request->status);
            })
            ->when($request->get('orderBy'), function ($rows) use ($request) {
                return $rows
                    ->orderBy($request->get('orderBy'), $request->get('desc') == 'true' ? 'desc' : 'asc');
            })
            ->when(!$request->get('orderBy'), function ($rows) use ($request) {
                return $rows
                    ->orderBy('orders.id', 'desc');
            })
            ->when($request->get('page') >= 0 && !$request->get('all'), function ($users) use ($request){
                return $users->skip($request->get('page') * 10)->take(10);
            })
            ->groupBy('id');

        $count = $rows->get()->count();
        $rows = $rows->get();
        if ($count > 0)
        {
            return response()->json([
                'models' => $rows,
                'count' => $count,
                "status"=> "success",
            ]);
        }
        else
        {
            return response()->json([
                'models' => $rows,
                'count' => $count,
                "status"=>"failure",
            ]);
        }
    }

    public function storeUpdate(Request $request)
    {
        $validate = Validator::make($request->all(), $this->rules($request), $this->messages());
        if (!$validate->fails()) {
            $this->fill($request->all());
            if (!Auth::user()->hasRole('admin')) {
                $this->price = $this->getPrice($request);
                if ($this->price <= 0) {
                    return response()->json([
                        'price' => ['Не удалось рассчитать стоимость заказа по заданным адресам']
                    ], 403);
                }
            } else {
                $this->getPrice($request);
            }
            $result = $this->save();
            if ($result) {
                $this->saveAdresses($request->get('addresses'));
                return response()->json($result, 200);
            } else {
                return response()->json($result, 403);
            }
        } else {
            return response()->json($validate->errors(), 403);
        }
    }

    public function setNullDriverAndCar()
    {
        $newDriver = null;
        $this->driver_id = $newDriver;
        $this->car_id = null;
        $this->status = self::STATUS_CANCELLED_BY_DRIVER;
    }

    public function setDriverByCallSign($callsign)
    {
        if ($callsign) {
            $profile = DB::table('profile')
                ->where([
                    ['callsign', '=', $callsign]
                ])
                ->first();
            if ($profile) {
                return $profile->id;
            }
        }
    }

    public function setCancelled()
    {
        $user = Auth::user();
        if ($user->hasRole('legal')) {
            if ($this->status != 4 && $this->status != 5) {
                $this->status = 5;
                $this->car_id = null;
                return response()->json($this->save(), 200);
            } else {
                return response()->json(['message' => 'Нельзя отменить'], 403);
            }
        } else if ($user->hasRole('admin')) {
            $this->status = 5;
            $this->car_id = null;
            return response()->json($this->save(), 200);
        } else
            return response()->json(['message' => 'Нельзя отменить'], 403);
    }

    public function getDriverPrice($params)
    {
        $time = $params['datetime_order'];

        $car = \App\Http\Models\Api\v4\Car::find($params['car_id']);
        if (!$car)
        {
            $this->length = 0;
            return 'Не заполнено поле авто';
        }
        elseif (!$car->address)
        {
            $this->length = 0;
            return 'У авто отсутвует адрес выезда';
        }
        $carAddresses[] = $car->address;
        $carAddresses[] = $params['from'];
        if (count($carAddresses) > 1) {
            $maps = new Maps();
            $carToStartLength = $maps->getLength($carAddresses, $time);
        }
        else
            return 'Отсутствует адрес выезда';
        $carToStartPrice = intval(($carToStartLength * $car->request_price)/1000);

        $wordAddresses[] = $params['from'];
        foreach($params['addresses'] as $item)
        {
            if ($item['name'])
                $wordAddresses[] = $item['name'];
        }

        $length = $this->getLength($wordAddresses, $time);

        if ($car)
        {
            $price = intval(($length * $car->transfer)/1000);
            $price += $carToStartPrice;
            if ($price < intval($car->order_cost)) $price = intval($car->order_cost);
        }
        else
        {
            $price = 0;
        }
        if ($params['addresses']) {
            $addresses = (array) $params->get('addresses');
            $last = array_pop($addresses);
            $backToStartLength = $this->getLength([$last['name'], $car->address], $time);
            $backToStartPrice = intval(($backToStartLength * $car->request_price)/1000);
            $price += $backToStartPrice;
        }
        $this->length = intval($length/1000);
        $this->serve_length = intval($carToStartLength/1000);
        $this->serve_price = $carToStartPrice;
        $this->serve_time = $maps->serveTime;
        return [
            'price' => $price,
            'serve_price' => $carToStartPrice,
            'serve_time' => $maps->serveTime
        ];
    }

    public function getAddressesArray()
    {
        $addresses = $this->address()->get()->implode('where', ', ');
        return $addresses;
    }

    public function setDateTimeOrderAttribute($value) {
        $this->attributes['datetime_order'] = Carbon::createFromTimestamp($value/1000);
    }

    public function address()
    {
        return $this->hasMany(OrderAddress::class, 'order_id');
    }

    public function client()
    {
        return $this->belongsTo(User::class, 'legal_id');
    }

    public function driver()
    {
        return $this->belongsTo(User::class, 'driver_id');
    }

    public function car()
    {
        return $this->belongsTo(Car::class, 'car_id');
    }
}
