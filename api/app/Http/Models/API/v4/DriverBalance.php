<?php


namespace App\Http\Models\API\v4;


use App\Http\Models\Api\v4\User\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DriverBalance extends \Eloquent
{
	protected $table = 'driver_balance';
	protected $messages = [];
	protected $fillable = [
		'type',
		'driver_id',
		'user_id',
		'count',
		'value',
		'comment',
		'added_at'
	];

	public function driver() {
		return $this->hasOne(User::class, 'driver_id');
	}

	public function user() {
		return $this->hasOne(User::class, 'user_id');
	}

	public function search(Request $request) {
		$query = self::where('driver_id', Auth::user()->getAttribute('id'));
		$rowsCount = $request->get('rowCount') ?? 100;
		$page = $request->get('page') ?? 0;
		$models = $query->skip($page * $rowsCount)->take($rowsCount)->get();
		$count = $models->count();
		return [
			'models' => $models,
			'count' => $count
		];
	}
	public function getMessages() {
		return $this->messages;
	}
}
