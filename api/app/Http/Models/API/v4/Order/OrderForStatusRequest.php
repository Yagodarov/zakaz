<?php

namespace App\Http\Requests\v4;

use App\Http\Models\Order;
use App\Http\Models\Site\Report\Report;
use App\Http\Models\User\User;
use App\Http\Models\UserCars;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Log;

class OrderForStatusRequest extends Order
{
	const STATUS_NEW = 0;
	const STATUS_IN_PROCESSING = 1;
	const STATUS_ACCEPTED = 2;
	const STATUS_PERFORMING = 3;
	const STATUS_COMPLETED = 4;
	const STATUS_CANCELLED = 5;
	const STATUS_CANCELLED_WITH_PAYMENT = 6;
	const STATUS_WAITING_BY_DRIVER = 7;
	const STATUS_CANCELLED_BY_DRIVER = 8;
	public $fillable = [
		'driver_id',
		'comment',
		'driver_comment',
		'driver_payment',
		'car_id',
		'status',
		'price'];
	protected $table = 'orders';

	public static function boot()
	{

		self::created(function (OrderForStatusRequest $model) {
			if ($token = $model->isSetDriverToken($model))
				$model->pushMessageCreated($model, $token);
			$driver_managers = User::role('driver-manager')->get();
			foreach ($driver_managers as $driver_manager) {
				$token = $driver_manager->profile->token;
				$model->pushMessageCreated($model, $token);
			}
			self::sendSmsByStatus($model);
		});
		self::updating(function (OrderForStatusRequest $model) {

		});

		self::updated(function (OrderForStatusRequest $model) {
			$model->sendPushMessages($model);
            $model->changeBalanceByStatus($model);
			$model->sendCheck($model);
			self::sendSmsByStatus($model);
		});

		self::deleting(function ($model) {
			// ... code here
		});

		self::deleted(function ($model) {
			// ... code here
		});
	}

	public function sendPushMessages($model) {
        if ($model->status == 5 && Auth::user()->hasRole('driver-manager') && $model->getOriginal('status') != 5) {
            if ($token = $model->isSetDriverToken($model))
                $model->pushMessageCancelled($model, $token);
        }
        if ($model->status == 3 && Auth::user()->hasRole('driver-manager') && $model->getOriginal('status') != 3) {
            if ($token = $model->isSetDriverToken($model))
                $model->pushMessageCreated($model, $token);
        }
        if ($model->status == 5 && Auth::user()->hasRole('driver') && $model->getOriginal('status') != 5) {
            $driver_managers = User::role('driver-manager')->get();
            foreach ($driver_managers as $driver_manager) {
                $token = $driver_manager->profile->token;
                $model->pushMessageCancelled($model, $token);
            }
        }
    }

    public function changeBalanceByStatus($model) {
        if ($model->getAttribute('status') == self::STATUS_CANCELLED_WITH_PAYMENT && $model->getOriginal('status') != self::STATUS_CANCELLED_WITH_PAYMENT ||
            $model->getAttribute('status') == self::STATUS_COMPLETED && $model->getOriginal('status') != self::STATUS_COMPLETED
        )
        {
            if ($model->getAttribute('payment') == 'cash') {
                $model->changeBalance('decrement_by_order_status', intval($model->getAttribute('price')) - intval($model->getAttribute('driver_payment')));
            } elseif ($model->getAttribute('payment') == 'noncash' || $model->getAttribute('payment') == 'card') {
                $model->changeBalance('increment_by_status', $model->getAttribute('driver_payment'));
            }
        }
    }

	public function sendCheck($model) {
		if ($model->getAttribute('status') == self::STATUS_COMPLETED && $model->getOriginal('status') != self::STATUS_COMPLETED) {
			(new Report($model))->sendReport();
		}
		if ($model->getAttribute('status') == self::STATUS_CANCELLED_WITH_PAYMENT && $model->getOriginal('status') != self::STATUS_CANCELLED_WITH_PAYMENT) {
			(new Report($model))->sendReport();
		}
	}

	public function changeStatus(Request $request)
	{
		$user = Auth::user();
		if ($user->hasRole('admin') || $user->hasRole('driver-manager')) {
			$validator = Validator::make(Input::all(), $this->validateAdmin($user, $request), $this->messages());
			if ($validator->fails()) {
				return response()->json([
					'status' => 'failure',
					'errors' => $validator->errors()->all()
				], 403);
			} else {
				$this->fill($request->all());
				if ($result = $this->save()) {
					return response()->json($result, 200);
				} else {
					return response()->json($result, 403);
				}
			}
		} elseif ($user->hasRole('driver')) {
			$validator = Validator::make(Input::all(), $this->validateDriver($user, $request), $this->messages());
			if ($validator->fails()) {
				return response()->json([
					'status' => 'failure',
					'errors' => $validator->errors()->all()
				], 403);
			} else {
				$this->status = $request->get('status');
				$this->driver_comment = $request->get('driver_comment');
				return response()->json($this->save());
			}
		}

	}

	public function validateAdmin($user, $request)
	{
		return [
			'driver_id' => ['sometimes', function ($attribute, $value, $fail) {
				if (!($user = User::where('id', '=', $value)->first())) {
					$fail('Водитель не найден');
				} else {
					if (!($user->hasRole('driver') || $user->hasRole('driver-manager')))
						$fail('Водитель не найден');
				}
			}
			],
			'comment' => ['sometimes'],
			'driver_payment' => ['nullable', 'numeric'],
			'car_id' => ['sometimes', 'exists:cars,id', function ($attribute, $value, $fail) use ($request) {
				if ($request->driver_id && $request->car_id) {
					if (!DB::table('user_cars')->where([
						['user_id', '=', $request->driver_id],
						['car_id', '=', $request->car_id]
					])->first()) {
						$fail('Авто не принадлежит водителю');
					}
				}
			}],
			'status' => ['required', 'numeric', 'min:1', 'max:8'],
			'price' => ['sometimes', 'numeric']
		];
	}

	public function messages()
	{
		return [
			'required' => 'Заполните это поле',
			'status.required' => 'Заполните статус',
			'status.min' => 'Неправильный статус',
			'status.max' => 'Неправильный статус',
			'id.required' => 'Заполните номер заказа',
			'min' => 'Не менее :min символа(-ов)',
			'max' => 'Не более :max символа(-ов)',
			'unique' => 'Уже используется',
			'email' => 'Введите правильный формат email',
			'date' => 'Выберите дату',
			'numeric' => 'Заполните это поле',
			'driver_payment.numeric' => 'Оплата водителю должно быть числом',
			'car_id.exists' => 'Нет такого автомобиля'
		];
	}

	public function validateDriver($user, $request)
	{
		return [
			'id' => ['required', function ($attribute, $value, $fail) use ($user) {
				if (!($user->id == $this->getAttribute('driver_id'))) {
					$fail("Заказ не принадлежит водителю");
				}
			}],
			'driver_comment' => 'sometimes',
			'status' => ['required', 'numeric', 'min:1', 'max:8', function ($attribute, $value, $fail) {
				if ($this->getAttribute('status') == $value) {
					return true;
				}
				if ($this->getAttribute('status') == self::STATUS_ACCEPTED) {
					if (!($value == self::STATUS_PERFORMING || $value == self::STATUS_CANCELLED)) {
						Log::info('validate error', [
							'value' => $value,
							'attributes' => $this->getAttributes()
						]);
						return $fail("Невозможно изменить статус");
					}
				} elseif ($this->getAttribute('status') == self::STATUS_PERFORMING) {
					if (!($value == self::STATUS_COMPLETED || $value == self::STATUS_CANCELLED || $value == self::STATUS_CANCELLED_BY_DRIVER)) {
						Log::info('validate error', [
							'value' => $value,
							'attributes' => $this->getAttributes()
						]);
						return $fail("Невозможно изменить статус");
					}
				} elseif ($this->getAttribute('status') == self::STATUS_CANCELLED) {
					if (!($value == self::STATUS_CANCELLED)) {
						Log::info('validate error', [
							'value' => $value,
							'attributes' => $this->getAttributes()
						]);
						return $fail("Невозможно изменить статус");
					}
				} elseif ($this->getAttribute('status') != self::STATUS_WAITING_BY_DRIVER) {
					Log::info('validate error', [
						'value' => $value,
						'attributes' => $this->getAttributes()
					]);
					return $fail("Невозможно изменить статус");
				}
			}]
		];
	}

	public function setDriversCars($user)
	{
		if (!$this->driver_id) {
			if ($car = UserCars::leftJoin('cars', 'user_cars.car_id', '=', 'cars.id')
				->where([
					['user_cars.user_id', '=', $user->id],
					['user_cars.blocked', '=', 0],
				])
				->first()) {
				$this->driver_id = $user->id;
			}
		}
		if (!$this->car_id) {
			if ($this->driver_id) {
				if ($car = UserCars::leftJoin('cars', 'user_cars.car_id', '=', 'cars.id')
					->where([
						['user_cars.user_id', '=', $this->driver_id],
						['user_cars.blocked', '=', 0],
					])
					->first()) {
					$this->car_id = $car->car_id;
				} else {
					$this->driver_id = null;
				}
			}
		}
	}
}
