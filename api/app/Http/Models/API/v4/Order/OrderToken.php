<?php


namespace App\Http\Models\API\v4\Order;


use App\Http\Requests\Request;
use Illuminate\Database\Eloquent\Model;

class OrderToken extends Model
{
    protected $fillable = [
        'order_id',
        'token'
    ];

    public function store(Request $request) {
        $this->fill($request->only(['order_id']));
        return $this->store();
    }

    public function setToken() {
        $this->setAttribute('token', str_random(4));
    }

    public function sendSmsToken() {

    }
}
