<?php


namespace App\Http\Models\API\v4\Sms;


use App\Http\Models\API\v4\Client;
use App\Http\Models\Api\v4\User\User;
use App\Http\Requests\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class SmsToken extends Model
{
    protected $fillable = [
        'sms_token', 'client_id'
    ];

    public function store(Request $request) {
        $this->fill($request->all());
        return $this->save();
    }

    public $login = "";
    public $password = "";
    public $message = "";
    protected $options;
    public function __construct()
    {
        $this->setOptions();
        parent::__construct();
    }

    public function setClientId() {
        $this->setAttribute('client_id', Auth::user()->getAttribute('id'));
    }

    public function setToken() {
        $this->sms_token = rand(1000, 9999);
    }

    public function sendSms() {
        $options = [
            'login' => $this->getLogin(),
            'psw' => $this->getPassword(),
            'phones' => $this->getClientNumber(),
            'mes' => $this->getMessage(),
            'sender' => $this->getSender()
        ];
        $soapClient = new \SoapClient('https://smsc.ru/sys/soap.php?wsdl');
        $result = $soapClient->send_sms($options);
        $this->save();
        return $result;
    }

    public function getClientNumber() {
        return $this->client->getAttribute('phone');
    }

    public function getMessage() {
        return $this->getAttribute('sms_token');
    }

    public function getLogin() {
        return $this->options->getAttribute('sms_login');
    }

    public function getPassword() {
        return $this->options->getAttribute('sms_password');
    }

    public function getSender() {
        return $this->options->getAttribute('sms_sender');
    }

    protected function setOptions(): void {
        $options = \App\Http\Models\Options::orderBy('id', 'desc')->first();
        $this->options = $options;
    }

    public function client() {
        return $this->belongsTo(Client::class, 'client_id');
    }
}
