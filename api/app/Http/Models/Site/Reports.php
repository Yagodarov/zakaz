<?php


namespace App\Http\Models\Site;

use App\Http\Models\Site\Order\Order;
use App\Http\Models\Site\User\User;
use Barryvdh\DomPDF\Facade as PDF;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use MessageFormatter;
use Storage;

class Reports
{
  public static function getDriverReports(Request $request)
  {
    if ($request->get('date_from') && $request->get('date_to')) {
      return DB
        ::select(DB::raw('
                select
                sum(driver_payment) as driver_payment, profile.name as driver_name
                from orders
                left join profile on orders.driver_id = profile.id
                where datetime_order > ? and datetime_order < ? and status = 4 or status = 6
                group by driver_id'), [
	        Carbon::parse($request->get('date_from'))->hour(0)->second(0)->minute(0),
	        Carbon::parse($request->get('date_to'))->hour(23)->second(59)->minute(59),
        ]);
    }
  }

  public static function getDriverPaymentAndDebtReports(Request $request)
  {
    if ($request->get('date_from') && $request->get('date_to')) {
      return DB
        ::select(DB::raw('
                select
                sum(driver_payment) as driver_payment, profile.name as driver_name
                from orders
                left join profile on orders.driver_id = profile.id
                where datetime_order > ? and datetime_order < ? and status = 4 or status = 6
                group by driver_id'), [
                	Carbon::parse($request->get('date_from'))->hour(0)->second(0)->minute(0),
	                Carbon::parse($request->get('date_to'))->hour(23)->second(59)->minute(59),
                ]);
    }
  }

  public static function getLegalReportsPdf(Request $request)
  {
    $orders = Order
      ::where([
        ['legal_id', '=', $request->get('legal')],
        ['datetime_order', '>', Carbon::parse($request->get('date_from'))->hour(0)->second(0)->minute(0)],
        ['datetime_order', '<', Carbon::parse($request->get('date_to'))->hour(23)->second(59)->minute(59)],
      ])
      ->whereIn('status', [4, 6])
      ->with('driver')
      ->with('driver.profile')
      ->with('client')
      ->with('client.profile')
      ->with('car')
      ->with('address')
      ->get();

    $legal = User::where('id', '=', $request->get('legal'))->with('profile')->first();

    $price = self::getTotal($orders);
    $message = (new MessageFormatter('ru-RU', '{n, spellout}'))->format(['n' => $price]);
    $sumMessage = self::num2word($price, ['рубль', 'рубля', 'рублей']);
    $pdf = PDF::loadView('report_legal', [
      'legal' => $legal,
      'date_from' => $request->get('date_from'),
      'date_to' => $request->get('date_to'),
      'orders' => $orders,
      'price' => $price,
      'message' => $message,
      'sumMessage' => $sumMessage,
      'stamp_sign' => $request->get('stamp_sign')
    ])->setPaper('a4', 'landscape');
    $path = self::getPathName($legal, Carbon::parse($request->get('date_from'))->format("dmY"), Carbon::parse($request->get('date_to'))->format("dmY"), $request->get('stamp_sign'));
    Storage::delete('public/pdf/'.$path);
    Storage::put('public/pdf/'.$path, $pdf->output());

    return $path;
  }

  public static function num2word($num, $words)
  {
    $num = abs($num) % 100; // для правильного отображения отрицательного значения
    if ($num > 19) {
      $num = $num % 10;
    }
    switch ($num) {
      case 1:
      {
        return ($words[0]);
      }
      case 2:
      case 3:
      case 4:
      {
        return ($words[1]);
      }
      default:
      {
        return ($words[2]);
      }
    }
  }

  public static function getTotal($orders)
  {
    $result = 0;
    foreach ($orders as $order) {
      $result += $order['price'];
    }
    return $result;
  }

  public static function getPathName($legal, $date_from, $date_to, $stamp_sign)
  {
    $path = '';
    $profile_name = str_replace(" ", "_", $legal['profile']['name']);
    $result = $path . $profile_name . '_' . $date_from . '-' . $date_to;
    $stamp_sign == 'true' ? $result .= '_ПП': null;
    $result  .= '.pdf';
    return $result;
  }

  public static function getLegalTable(Request $request)
  {
    $orders = Order
      ::where([
        ['legal_id', '=', $request->get('legal')],
	      ['datetime_order', '>', Carbon::parse($request->get('date_from'))->hour(0)->second(0)->minute(0)],
	      ['datetime_order', '<', Carbon::parse($request->get('date_to'))->hour(23)->second(59)->minute(59)],
      ])
      ->whereIn('status', [4, 6])
      ->with('driver')
      ->with('driver.profile')
      ->with('client')
      ->with('client.profile')
      ->with('car')
      ->with('address')
      ->get();
    return response()->json($orders, 200);
  }

  public static function paymentOrDebt(Request $request)
  {
    $orders = Order
      ::where([
	      ['datetime_order', '>', Carbon::parse($request->get('date_from'))->hour(0)->second(0)->minute(0)],
	      ['datetime_order', '<', Carbon::parse($request->get('date_to'))->hour(23)->second(59)->minute(59)],
      ])
      ->with('driver')
      ->with('driver.profile')
      ->with('client')
      ->with('client.profile')
      ->with('car')
      ->with('address');

    if (is_array($request->get('legal'))) {
      $orders->whereIn('orders.legal_id', $request->get('legal'));
    } else {
      $orders->where('orders.legal_id', '=', $request->get('legal'));
    }

    if (is_array($request->get('drivers'))) {
      $orders->whereIn('orders.driver_id', $request->get('drivers'));
    } else {
      $orders->where('orders.driver_id', '=', $request->get('drivers'));
    }

    $orders = $orders->get();
    $array = [];
    foreach ($orders as $order) {
      if (!isset($array[$order['driver']['id']])) {
        $array[$order['driver']['id']] = [
          'profile_name' => $order['driver']['profile']['name'],
          'payment' => 0,
        ];
      }
      if ($order['payment'] === 'noncash') {
        $array[$order['driver']['id']]['payment'] += $order['driver_payment'];
      }
      if ($order['payment'] === 'cash') {
        $minus = $order['price'] - $order['driver_payment'];
        $array[$order['driver']['id']]['payment'] -= $minus;
      }
      if ($order['payment'] === 'card') {
        $minus = $order['price'] - $order['driver_payment'];
        $array[$order['driver']['id']]['payment'] -= $minus;
      }
    }
    return response()->json($array, 200);
  }
}
