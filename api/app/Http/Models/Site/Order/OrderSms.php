<?php


namespace App\Http\Models\Site\Order;


class OrderSms
{
  protected $order;
  protected $smsSettings;
  public function __construct($order)
  {
    $this->order = $order;
    $this->setSmsSettings();
    $this->sendSms();
  }

  public function sendSms() {
  	if ($this->smsSettings) {
	    if (!$this->isEnabled()) return false;
	    $sms = new Sms($this->getTemplateByStatus(), $this->order);
	    return $sms->sendSms();
    }
  }

  public function setSmsSettings() {
    $this->smsSettings = SmsSettings::where('event_id', $this->getOrderStatus())->first();
  }

  public function getTemplateByStatus() {
    return $this->smsSettings->getAttribute('template');
  }

  protected function isEnabled() {
    return $this->smsSettings->getAttribute('send_sms');
  }

  protected function getOrderStatus() {
    return $this->order->status;
  }
}
