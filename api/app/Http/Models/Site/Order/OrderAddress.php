<?php


namespace App\Http\Models\Site\Order;


use Illuminate\Database\Eloquent\Model;

class OrderAddress extends Model {
    public $timestamps = false;
    protected $table = 'order_address';
    protected $fillable = ['order_id', 'where'];
}
