<?php

namespace App\Http\Models\Site\Order;

use App\Http\Models\Site\Maps\Maps;
use App\Http\Models\Site\User\User;
use Config;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class Order extends Model
{
    const STATUS_NEW = 0;
    const STATUS_IN_PROCESSING = 1;
    const STATUS_ACCEPTED = 2;
    const STATUS_PERFORMING = 3;
    const STATUS_COMPLETED = 4;
    const STATUS_CANCELLED = 5;
    const STATUS_CANCELLED_WITH_PAYMENT = 6;
    const STATUS_WAITING_BY_DRIVER = 7;
    const STATUS_CANCELLED_BY_DRIVER = 8;
    protected $fillable = [
        'datetime_order', 'number', 'from', 'comment', 'payment', 'price',
        'legal_id', 'driver_id', 'car_id', 'status', 'driver_comment', 'scenario',
        'driver_payment', 'length', 'serve_length', 'client_id', 'order_type'
    ];

    protected $legal = [
        'datetime_order', 'number', 'from', 'comment', 'payment',
        'legal_id', 'scenario',
    ];

    public static function getStatusLabels($id)
    {
        return [
            '0' => 'Новый',
            '1' => 'В обработке',
            '2' => 'Принят',
            '3' => 'На исполнении',
            '4' => 'Выполнен',
            '5' => 'Отменен',
            '6' => 'Отменен с оплатой',
            '7' => 'Ожидает подтверждения водителем',
        ][$id];
    }

    public static function getPaymentLabels($id)
    {
        return [
            'cash' => 'Наличный',
            'noncash' => 'Безналичный',
            'card' => 'Банковская карта',
        ][$id];
    }

    public static function boot()
    {
        parent::boot();

        self::creating(function ($model) {
        });
        self::created(function (Order $model) {
            if ($token = $model->isSetDriverToken($model))
                $model->pushMessageCreated($model, $token);
            $driver_managers = User::role('driver-manager')->get();
            foreach ($driver_managers as $driver_manager) {
                $token = $driver_manager->profile->token;
                $model->pushMessageCreated($model, $token);
            }
            self::sendSmsByStatus($model);
            $model->sendCheck($model);
            $model->changeBalanceByStatus($model);
        });

        self::updating(function (Order $model) {
            if ($token = $model->isSetDriverToken($model))
                $model->pushMessageUpdating($model, $token);

        });
        self::updated(function (Order $model) {
            //Если водитель отменил заказ
            if ($model->status == 5 && $model->getOriginal('status') != 5 && Auth::user()->hasRole('driver')) {
                $driver_managers = User::role('driver-manager')->get();
                foreach ($driver_managers as $driver_manager) {
                    $token = $driver_manager->profile->token;
                    $model->pushMessageCancelled($model, $token);
                }
            }
            $model->changeBalanceByStatus($model);
            $model->sendCheck($model);
            self::sendSmsByStatus($model);
        });

        self::deleting(function ($model) {
            // ... code here
        });

        self::deleted(function ($model) {
            // ... code here
        });
    }

    public function sendCheck($model) {
	    if ($model->getAttribute('status') == $model::STATUS_COMPLETED &&
            ($model->getOriginal('status') != $model::STATUS_COMPLETED)
        ) {
		    (new Report($model))->sendReport();
	    }
	    if ($model->getAttribute('status') == $model::STATUS_CANCELLED_WITH_PAYMENT &&
            ($model->getOriginal('status') != $model::STATUS_CANCELLED_WITH_PAYMENT)
        ) {
		    (new Report($model))->sendReport();
	    }
    }


    public function changeBalanceByStatus($model) {
        if ($model->getAttribute('status') == self::STATUS_CANCELLED_WITH_PAYMENT && $model->getOriginal('status') != self::STATUS_CANCELLED_WITH_PAYMENT ||
            $model->getAttribute('status') == self::STATUS_COMPLETED && $model->getOriginal('status') != self::STATUS_COMPLETED
        )
        {
            if ($model->getAttribute('payment') == 'cash') {
                $model->changeBalance('decrement_by_order_status', intval($model->getAttribute('price')) - intval($model->getAttribute('driver_payment')));
            } elseif ($model->getAttribute('payment') == 'noncash' || $model->getAttribute('payment') == 'card') {
                $model->changeBalance('increment_by_status', $model->getAttribute('driver_payment'));
            }
        }
    }
    public function changeBalance($type, $value) {
        $driverBalance = new \App\Http\Site\Order\Models\API\v4\DriverBalance();
        $driverBalance->setAttribute('type', $type);
        $driverBalance->setAttribute('value', $value);
        $driverBalance->setAttribute('driver_id', $this->getAttribute('driver_id'));
        $driverBalance->setAttribute('order_id', $this->getAttribute('id'));
        if (!$driverBalance->getAttribute('user_id')) {
            $driverBalance->setAttribute('user_id', Auth::user()->getAttribute('id'));
        }
        $driverBalance->save();
    }

    public function isSetDriverToken($model)
    {
        $row = (DB::table('profile')->where('id', $model->driver_id)->first());
        if ($row && $row->token) {
            return $row->token;
        } else return false;
    }

    public function pushMessageCreated($model, $token)
    {
        $serverApiKey = Config::get("push-notification.appNameAndroid")["apiKey"];
        $message = "Новый заказ";
        $user = Auth::user();
        if ($user->hasRole('driver-manager')) {
            $actor = 'driver-manager';
        } elseif ($user->hasRole('driver')) {
            $actor = 'driver';
        } else {
            $actor = $user->getRoleNames()[0];
        }
        $log = [
            "message" => $message,
            "push-token" => $token,
            'id' => $model->id,
            "apiKey" => $serverApiKey,
            'actor' => $actor
        ];
        Log::useDailyFiles(storage_path() . '/logs/push.log');
        Log::info('Отправка push сообщения', $log);

        (new PushNotification())->push($token, $message, 'Уведомление о заказе', $model->id);
    }

    public static function sendSmsByStatus($model)
    {
        try {
            new OrderSms($model);
        } catch (Exception $exception) {
            //something here
        }
    }

    public function pushMessageUpdating($model, $token)
    {
        if ((in_array($model->getOriginal('status'), [2, 3])) && $model->status == 5) {
            $serverApiKey = Config::get("push-notification.appNameAndroid")["apiKey"];
            $message = "Заказ отменен";
            $user = Auth::user();
            if ($user->hasRole('driver-manager')) {
                $actor = 'driver-manager';
            } elseif ($user->hasRole('driver')) {
                $actor = 'driver';
            } else {
                $actor = $user->getRoleNames()[0];
            }
            $log = [
                "message" => $message,
                "push-token" => $token,
                'id' => $model->id,
                "apiKey" => $serverApiKey,
                'actor' => $actor
            ];

            Log::useDailyFiles(storage_path() . '/logs/push.log');
            Log::info('Отправка push сообщения', $log);

            (new PushNotification())->push($token, $message, 'Уведомление о заказе', $model->id);
        }
        if (($model->status == 2 && $model->driver_id) && ($model->isDirty('status') || $model->isDirty('driver_id'))) {
            $serverApiKey = Config::get("push-notification.appNameAndroid")["apiKey"];
            $message = "Новый заказ";
            $log = [
                "message" => $message,
                "push-token" => $token,
                'id' => $model->id,
                "apiKey" => $serverApiKey
            ];

            Log::useDailyFiles(storage_path() . '/logs/push.log');
            Log::info('Отправка push сообщения', $log);

            (new PushNotification())->push($token, $message, 'Уведомление о заказе', $model->id);
        }
    }

    public function pushMessageCancelled($model, $token)
    {

        $serverApiKey = Config::get("push-notification.appNameAndroid")["apiKey"];
        $message = "Заказ отменен";
        $user = Auth::user();
        if ($user->hasRole('driver-manager')) {
            $actor = 'driver-manager';
        } elseif ($user->hasRole('driver')) {
            $actor = 'driver';
        } else {
            $actor = $user->getRoleNames()[0];
        }
        $log = [
            "message" => $message,
            "push-token" => $token,
            'id' => $model->id,
            "apiKey" => $serverApiKey,
            'actor' => $actor
        ];
        Log::useDailyFiles(storage_path() . '/logs/push.log');
        Log::info('Отправка push сообщения', $log);

        (new PushNotification())->push($token, $message, 'Уведомление о заказе', $model->id);
    }

    public static function search(Request $request)
    {
        $rows = DB::table('orders')
            ->select(
                'orders.id',
                'orders.car_id',
                'orders.comment',
                'orders.created_at',
                'orders.datetime_order',
                'orders.driver_comment',
                'orders.driver_id',
                'orders.from',
                'orders.legal_id',
                'orders.number',
                'orders.payment',
                'orders.price',
                'orders.length',
                'orders.serve_length',
                'orders.scenario',
                'orders.status',
                'orders.updated_at',
                'orders.driver_payment',
                DB::raw('(SELECT GROUP_CONCAT(`where` SEPARATOR " - ") as `to` FROM `order_address` WHERE `order_id` = orders.id  order by `id` asc) as `to` '),
                'profile.name as legal_name',
                DB::raw('CONCAT_WS(" ", cars.mark, cars.model, cars.color, cars.car_number) as car_name'),
                DB::raw("
					CONCAT_WS(
						' ',
						SUBSTRING_INDEX(SUBSTRING_INDEX(dp.name, ' ', 1), ' ', -1),
						SUBSTRING_INDEX(SUBSTRING_INDEX(dp.name, ' ', 2), ' ', -1)
					) as driver_name

					"),
                'dp.phone as driver_phone',
                DB::raw('CONCAT_WS(" ", clients.first_name, clients.last_name) as client_name')
            )
            ->leftJoin('profile', 'profile.id', '=', 'orders.legal_id')
            ->leftJoin('clients', 'clients.id', '=', 'orders.client_id')
            ->leftJoin('profile as dp', 'dp.id', '=', 'orders.driver_id')
            ->leftJoin('cars', 'cars.id', '=', 'orders.car_id')
            ->when(Auth::user()->hasRole('legal') && !(Auth::user()->hasRole('admin') || Auth::user()->hasRole('driver-manager')), function ($rows) use ($request) {
                $user = Auth::user();
                return $rows
                    ->where('legal_id', '=', $user->id);
            })
            ->when(Auth::user()->hasRole('driver') && !(Auth::user()->hasRole('admin') || Auth::user()->hasRole('driver-manager')), function ($rows) use ($request) {
                $user = Auth::user();
                return $rows
                    ->where('driver_id', '=', $user->id);
            })
            ->when($request->get('id'), function ($rows) use ($request) {
                return $rows->where('orders.id', '=', $request->id);
            })
            ->when($request->get('datetime_order'), function ($rows) use ($request) {
                return $rows->where('orders.datetime_order', 'LIKE', "{$request->datetime_order}%");
            })
            ->when($request->get('from'), function ($rows) use ($request) {
                return $rows->where('orders.from', 'LIKE', "%{$request->from}%");
            })
            ->when($request->get('payment'), function ($rows) use ($request) {
                return $rows->where('orders.payment', '=', $request->payment);
            })
            ->when($request->get('to'), function ($rows) use ($request) {
                return $rows->whereIn('orders.id', function ($rows) use ($request) {
                    $rows->select('order_id')
                        ->from('order_address')
                        ->where('order_address.where', 'LIKE', "{$request->to}%");
                });
            })
            ->when($request->get('price'), function ($rows) use ($request) {
                return $rows->where('orders.price', '>=', $request->price);
            })
            ->when($request->get('price2'), function ($rows) use ($request) {
                return $rows->where('orders.price', '<=', $request->price2);
            })
            ->when($request->get('driver_payment'), function ($rows) use ($request) {
                return $rows->where('orders.driver_payment', '>=', $request->driver_payment);
            })
            ->when($request->get('driver_payment2'), function ($rows) use ($request) {
                return $rows->where('orders.driver_payment', '<=', $request->driver_payment2);
            })
            ->when($request->get('car_id'), function ($rows) use ($request) {
                return $rows->where('orders.car_id', '=', $request->car_id);
            })
            ->when($request->get('driver_id'), function ($rows) use ($request) {
                return $rows->where('orders.driver_id', '=', $request->driver_id);
            })
            ->when($request->get('legal_id') > -1, function ($rows) use ($request) {
                return $rows->where('orders.legal_id', '=', $request->get('legal_id'));
            }
            )
            ->when($request->get('status') > -1, function ($rows) use ($request) {
                return $rows->where('orders.status', '=', $request->status);
            })
            ->when($request->get('orderBy'), function ($rows) use ($request) {
                return $rows
                    ->orderBy($request->get('orderBy'), $request->get('desc') == 'true' ? 'desc' : 'asc');
            })
            ->when(!$request->get('orderBy'), function ($rows) use ($request) {
                return $rows
                    ->orderBy('orders.datetime_order', 'desc');
            })
            ->groupBy('id');
        // return response()->json($rows->toSql(), 403);
        $count = $rows->get()->count();
        $rows = $rows
            ->when($request->get('page') >= 0 && !$request->get('all'), function ($rows) use ($request) {
                return $rows->skip($request->get('page') * 10)->take(10);
            })
            ->get();
        return response()->json([
            'models' => $rows,
            'count' => $count
        ]);
    }

    public static function searchPhone(Request $request)
    {
        $rows = DB::table('orders')
            ->select(
                'orders.id',
                'orders.car_id',
                'orders.comment',
                'orders.created_at',
                'orders.datetime_order',
                'orders.driver_comment',
                'orders.driver_id',
                'orders.from',
                'orders.legal_id',
                'orders.client_id',
                'orders.number',
                'orders.payment',
                'orders.price',
                'orders.length',
                'orders.serve_length',
                'orders.scenario',
                'orders.status',
                'orders.updated_at',
                'orders.driver_payment',

                DB::raw('(SELECT GROUP_CONCAT(`where` SEPARATOR " - ") as `to` FROM `order_address` WHERE `order_id` = orders.id  order by `id` asc) as `to`'),
                'profile.name as legal_name',
                DB::raw('CONCAT_WS(" ", cars.mark, cars.model, cars.color, cars.car_number) as car_name'),
                DB::raw("
					CONCAT_WS(
						' ',
						SUBSTRING_INDEX(SUBSTRING_INDEX(dp.name, ' ', 1), ' ', -1),
						SUBSTRING_INDEX(SUBSTRING_INDEX(dp.name, ' ', 2), ' ', -1)
					) as driver_name

					"),
                'dp.phone as driver_phone'
            )
            ->leftJoin('profile', 'profile.id', '=', 'orders.legal_id')
            ->leftJoin('profile as dp', 'dp.id', '=', 'orders.driver_id')
            ->leftJoin('cars', 'cars.id', '=', 'orders.car_id')
            ->when(Auth::user()->hasRole('driver') && !Auth::user()->hasRole('driver-manager'), function ($rows) use ($request) {
                $user = Auth::user();
                return $rows
                    ->where('driver_id', '=', $user->id);
            })
            ->when(Auth::user()->hasRole('admin'), function ($rows) use ($request) {
                return $rows
                    ->where('orders.id', '=', 0);
            })
            ->when(Auth::user()->hasRole('legal'), function ($rows) use ($request) {
                return $rows
                    ->where('orders.id', '=', 0);
            })
            ->when($request->get('id'), function ($rows) use ($request) {
                return $rows->where('orders.id', '=', $request->id);
            })
            ->when($request->get('updated_at'), function ($rows) use ($request) {
                return $rows->where('orders.updated_at', '>', Carbon::createFromTimestamp(intval($request->get('updated_at')))->toDateTimeString());
            })
            ->when($request->get('datetime_order'), function ($rows) use ($request) {
                return $rows->where('orders.datetime_order', 'LIKE', "{$request->datetime_order}%");
            })
            ->when($request->get('from'), function ($rows) use ($request) {
                return $rows->where('orders.from', 'LIKE', "%{$request->from}%");
            })
            ->when($request->get('payment'), function ($rows) use ($request) {
                return $rows->where('orders.payment', '=', $request->payment);
            })
            ->when($request->get('to'), function ($rows) use ($request) {
                return $rows->whereIn('orders.id', function ($rows) use ($request) {
                    $rows->select('order_id')
                        ->from('order_address')
                        ->where('order_address.where', 'LIKE', "{$request->to}%");
                });
            })
            ->when($request->get('price'), function ($rows) use ($request) {
                return $rows->where('orders.price', '>=', $request->price);
            })
            ->when($request->get('price2'), function ($rows) use ($request) {
                return $rows->where('orders.price', '<=', $request->price2);
            })
            ->when($request->get('driver_payment'), function ($rows) use ($request) {
                return $rows->where('orders.driver_payment', '>=', $request->driver_payment);
            })
            ->when($request->get('driver_payment2'), function ($rows) use ($request) {
                return $rows->where('orders.driver_payment', '<=', $request->driver_payment2);
            })
            ->when($request->get('car_id'), function ($rows) use ($request) {
                return $rows->where('orders.car_id', '=', $request->car_id);
            })
            ->when($request->get('driver_id'), function ($rows) use ($request) {
                return $rows->where('orders.driver_id', '=', $request->driver_id);
            })
            ->when($request->get('legal_id'), function ($rows) use ($request) {
                return $rows->where('orders.legal_id', '=', $request->legal_id);
            })
            ->when($request->get('status') > -1, function ($rows) use ($request) {
                return $rows->where('orders.status', '=', $request->status);
            })
            ->when($request->get('orderBy'), function ($rows) use ($request) {
                return $rows
                    ->orderBy($request->get('orderBy'), $request->get('desc') == 'true' ? 'desc' : 'asc');
            })
            ->when(!$request->get('orderBy'), function ($rows) use ($request) {
                return $rows
                    ->orderBy('orders.id', 'desc');
            })
            ->groupBy('id');

        $count = $rows->get()->count();
        $rows = $rows->get();
        if ($count > 0) {
            return response()->json([
                'models' => $rows,
                'count' => $count,
                "status" => "success",
            ]);
        } else {
            return response()->json([
                'models' => $rows,
                'count' => $count,
                "status" => "failure",
            ]);
        }
    }

    public function getLengthForTwo($from, $to, $date)
    {
        $time = strtotime(Carbon::createFromFormat('Y-m-d\TH:i', $date)->toDateTimeString());
        return $this->getLength([$from, $to], $time);
    }

    public function getLength($addresses, $time)
    {
        return (new Maps())->getLength($addresses, $time);
    }

    public function changeStatus(Request $request)
    {
        $user = Auth::user();
        $validator = Validator::make($request->only('id', 'status', 'driver_comment'), [
            'id' => ['required', function ($attribute, $value, $fail) use ($user) {
                if (!($user->id == $this->driver_id)) {
                    return $fail("Заказ не принадлежит водителю");
                }
                return true;
            }],
            'driver_comment' => 'sometimes',
            'status' => ['required', function ($attribute, $value, $fail) {
                if ($this->status == 2) {
                    if (!($value == 3 || $value == 5)) {
                        return $fail("Невозможно изменить статус");
                    }
                } elseif ($this->status == 3) {
                    if (!($value == 4 || $value == 5)) {
                        return $fail("Невозможно изменить статус");
                    }
                } elseif ($this->status == 5) {
                    if (!($value == 5))
                        return $fail("Невозможно изменить статус");
                } else {
                    return $fail("Невозможно изменить статус");
                }
            }],
        ], $this->messages());
        if ($validator->fails()) {
            return response()->json($validator->messages(), 403);
        } else {
            $this->status = $request->get('status');
            $this->driver_comment = $request->get('driver_comment');
            return response()->json($this->save());
        }
    }

    public function messages()
    {
        return [
            'required' => 'Заполните это поле',
            'min' => 'Не менее :min символа(-ов)',
            'max' => 'Не более :max символа(-ов)',
            'unique' => 'Уже используется',
            'email' => 'Введите правильный формат email',
            'date' => 'Выберите дату',
            'numeric' => 'Заполните это поле'
        ];
    }

    public function store(Request $request)
    {
        $this->scenario = 'legal';
        $validator = Validator::make(Input::all(), $this->rules($request), $this->messages());
        if ($validator->fails()) {
            return response()->json($validator->messages(), 403);
        } else {
            if (Auth::user()->hasRole('admin')) {
                $this->fill($request->only($this->fillable));
            } else {
                $this->fill($request->only($this->legal));
            }
        }
        // return response()->json($this->getPrice($request), 403);
        if (!Auth::user()->hasRole('admin')) {
            $this->status = 0;
            $this->price = $this->getPrice($request);
            if ($this->price <= 0) {
                return response()->json([
                    'errors' => [
                        'price' => 'Не удалось рассчитать стоимость заказа'
                    ]
                ], 403);
            }
        }

        if ($result = $this->save()) {
            $this->saveAdresses($request->get('addresses'));
            return response()->json($result, 200);
        } else
            return response()->json($result, 403);
    }

    public function rules(Request $request)
    {
        return [
            'datetime_order' => ['required', 'date', function ($attribute, $value, $fail) use ($request) {
                $user = Auth::user()->hasRole('admin');
                if (!$user) {
                    $date = Carbon::createFromFormat('Y-m-d\TH:i', $request->get('datetime_order'))->toDateTimeString();
                    $query = DB::table('user_contract')
                        ->where([
                            ['user_id', '=', $request->get('legal_id')],
                            ['contract_period_start', '<=', "{$date}"],
                            ['contract_period_end', '>=', "{$date}"],
                        ])
                        ->first();
                    if (!$query)
                        return $fail('Отсутсвует договор на эту дату и время');
                }
                return true;
            },],
            'number' => 'required',
            'from' => 'required',
            'addresses' => ['required_if:order_type,0'],
            'comment' => 'sometimes',
            'payment' => 'required_if:order_type,0|string',
            'price' => 'required_if:order_type,0|numeric',
            'driver_payment' => [function ($attribute, $value, $fail) use ($request) {
                if ($user = Auth::user()->hasRole('admin') || $user = Auth::user()->hasRole('driver-manager')) {
                    if (($request->driver_id && !$request->get('driver_payment')) || !is_numeric($request->get('driver_payment'))) {
                        return $fail("Заполните это поле");
                    }
                }
            }],
            'legal_id' => 'sometimes|numeric|nullable',
            'driver_id' => 'sometimes|numeric|nullable',
            'car_id' => ['sometimes', function ($attribute, $value, $fail) use ($request) {
                if ($request->get('driver_id') && ($request->get('car_id') == ''))
                    return $fail("Заполните это поле");
            }],
            'status' => ['required', 'numeric', function ($attribute, $value, $fail) use ($request) {
                if (Auth::user()->hasRole('legal')) {
                    $id = $request->get('id');
                    $data = DB::table('orders')
                        ->select('status')
                        ->where('id', $id)
                        ->first();
                    if ($data && ($data->status == '4' || $data->status == '5' || $data->status == '6'))
                        return $fail("Нельзя сменить статус выполненного или отмененного заказа");
                }
            }]
        ];
    }

    public function getPrice($params)
    {
        if (!is_int($params['datetime_order'])) {
            $time = strtotime(Carbon::createFromFormat('Y-m-d\TH:i', $params['datetime_order'])->toDateTimeString());
            $date = Carbon::createFromFormat('Y-m-d\TH:i', $params['datetime_order'])->toDateTimeString();
        } else {
            $time = $params['datetime_order'];
            $date = Carbon::createFromTimestamp($params['datetime_order'])->toDateTimeString();
        }

        $wordAddresses[] = $params['from'];
        foreach ($params['addresses'] as $item) {
            if ($item['name'])
                $wordAddresses[] = $item['name'];
        }

        $length = $this->getLength($wordAddresses, $time);
        if ($params['legal_id']) {
            if ($query = DB::table('user_contract')
                ->where([
                    ['user_id', '=', $params['legal_id']],
                    ['contract_period_start', '<=', "{$date}"],
                    ['contract_period_end', '>=', "{$date}"],
                ])
                ->first()) {
                $price = intval(($length * $query->transfer / 1000) + $query->request_price);
                if ($price < $query->order_cost) $price = $query->order_cost;
            } else {
                $price = 'Отсутствует контракт у юр.лица';
            }
        } else
            $price = 0;
        if ($length != 0) {
            $this->length = intval($length / 1000);
            return $price;
        } else {
            $this->length = 0;
            return 0;
        }
    }

    public function saveAdresses($addresses)
    {
        info('addresses', $addresses);
        DB::table('order_address')->where('order_id', '=', $this->id)->delete();
        foreach ($addresses as $item) {
            $orderAddress = new OrderAddress();
            $orderAddress->setAttribute('order_id', $this->getAttribute('id'));
            $orderAddress->setAttribute('where', $item['name']);
            $orderAddress->save();
        }
    }

    public function storeSelect(Request $request)
    {
        $this->scenario = 'driver';
        $validator = Validator::make(Input::all(), $this->rulesSelect($request), $this->messages());
        if ($validator->fails()) {
            return response()->json($validator->messages(), 403);
        } else {
            $this->fill($request->all());
            $this->driver_id = $this->getDriverByCarId($this->car_id);
            if (!Auth::user()->hasRole('admin')) {
                $this->status = 7;
                $this->price = $this->getDriverPrice($request);
            }
            $this->setDriverPaymentPrice();

            if ($result = $this->save()) {
                $this->saveAdresses($request->get('addresses'));
                return response()->json($result, 200);
            } else
                return response()->json($result, 403);
        }
    }

    public function rulesSelect(Request $request)
    {
        return [
            'datetime_order' => ['required', 'date'],
            'number' => 'required',
            'from' => 'required',
            'addresses' => ['required', function ($attribute, $value, $fail) {
                if (is_array($value)) {
                    foreach ($value as $item) {
                        if ($item['name'] != '')
                            return true;
                    }

                } else
                    return $fail("Заполните это поле");
                return $fail("Заполните это поле");
            },],
            'comment' => 'sometimes',
            'payment' => 'required|string',
            'price' => 'required|numeric',
            'legal_id' => 'sometimes|numeric|nullable',
            'driver_id' => 'sometimes|numeric|nullable',
            'driver_payment' => [function ($attribute, $value, $fail) use ($request) {
                if ($user = Auth::user()->hasRole('admin') || $user = Auth::user()->hasRole('driver-manager')) {
                    if (($request->driver_id && !$request->get('driver_payment')) || !is_numeric($request->get('driver_payment'))) {
                        return $fail("Заполните это поле");
                    }
                }
            }],
            'car_id' => ['sometimes', function ($attribute, $value, $fail) use ($request) {
                if ($request->get('driver_id') && ($request->get('car_id') == ''))
                    return $fail("Заполните это поле");
            }],
            'status' => ['required', 'numeric', function ($attribute, $value, $fail) use ($request) {
                if (Auth::user()->hasRole('legal')) {
                    $id = $request->get('id');
                    $data = DB::table('orders')
                        ->select('status')
                        ->where('id', $id)
                        ->first();
                    if ($data && ($data->status == '4' || $data->status == '5' || $data->status == '6'))
                        return $fail("Нельзя сменить статус выполненного или отмененного заказа");
                }
            }]
        ];
    }

    public function getDriverByCarId($id)
    {
        $row = DB::table('cars')->select('users.*')
            ->leftJoin('user_cars', 'user_cars.car_id', '=', 'cars.id')
            ->leftJoin('users', 'user_cars.user_id', '=', 'users.id')
            ->where('cars.id', '=', $id)
            ->first();
        return ($row->id);
    }

    public function getDriverPrice($params)
    {
        if (!is_int($params['datetime_order'])) {
            $time = strtotime(Carbon::createFromFormat('Y-m-d\TH:i', $params['datetime_order'])->toDateTimeString());
            $date = Carbon::createFromFormat('Y-m-d\TH:i', $params['datetime_order'])->toDateTimeString();
        } else {
            $time = $params['datetime_order'];
            $date = Carbon::createFromTimestamp($params['datetime_order'])->toDateTimeString();
        }

        $car = Car::find($params['car_id']);
        if (!$car) {
            $this->length = 0;
            return 'Не заполнено поле авто';
        } elseif (!$car->address) {
            $this->length = 0;
            return 'У авто отсутвует адрес выезда';
        }
        $carAddresses[] = $car->address;
        $carAddresses[] = $params['from'];
        if (count($carAddresses) > 1)
            $carToStartLength = $this->getLength($carAddresses, $time);
        else
            return 'Отсутствует адрес выезда';
        $carToStartPrice = intval(($carToStartLength * $car->request_price) / 1000);

        $wordAddresses[] = $params['from'];
        foreach ($params['addresses'] as $item) {
            if ($item['name'])
                $wordAddresses[] = $item['name'];
        }

        $length = $this->getLength($wordAddresses, $time);

        if ($car) {
            $price = intval(($length * $car->transfer) / 1000);
            $price += $carToStartPrice;
            if ($price < intval($car->order_cost)) $price = intval($car->order_cost);
        } else {
            $price = 0;
        }
        $this->length = intval($length / 1000);
        $this->serve_length = intval($carToStartLength / 1000);
        return $price;
    }

    public function setDriverPaymentPrice()
    {
        if (Auth::user()->hasRole('legal')) {
            if (!$this->getAttribute('driver_payment')) {
                $this->setAttribute('driver_payment',
                    $this->getAttribute('price') - (($this->getAttribute('price') * $this->car->getAttribute('commission')) / 100)
                );
                $this->setAttribute('driver_payment', ceil($this->getAttribute('driver_payment')));
                $this->setAttribute('driver_payment', ceil($this->getAttribute('driver_payment') / 50) * 50);
                $x = 1;
            }
        }
    }

    public function storeSelectUpdate(Request $request)
    {
        $validator = Validator::make(Input::all(), $this->rulesSelect($request), $this->messages());
        if ($validator->fails()) {
            return response()->json($validator->messages(), 403);
        } else {
            $this->fill($request->all());
            $this->driver_id = $this->getDriverByCarId($this->car_id);
            if (!Auth::user()->hasRole('admin')) {
                $this->price = $this->getDriverPrice($request);
            } else {
                $this->getDriverPrice($request);
            }
            $this->scenario = 'driver';
            if ($result = $this->save()) {
                $this->saveAdresses($request->get('addresses'));
                return response()->json($result, 200);
            } else
                return response()->json($result, 403);
        }
    }

    public function storeUpdate(Request $request)
    {
        $validate = Validator::make($request->all(), $this->rules($request), $this->messages());
        if (!$validate->fails()) {
            $this->fill($request->all());
            if (!Auth::user()->hasRole('admin')) {
                $this->price = $this->getPrice($request);
                if ($this->price <= 0) {
                    return response()->json([
                        'price' => ['Не удалось рассчитать стоимость заказа по заданным адресам']
                    ], 403);
                }
            } else {
                $this->getPrice($request);
            }
            $result = $this->save();
            if ($result) {
                $this->saveAdresses($request->get('addresses'));
                return response()->json($result, 200);
            } else {
                return response()->json($result, 403);
            }
        } else {
            return response()->json($validate->errors(), 403);
        }
    }

    public function decline(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'driver_comment' => 'sometimes',
            'callsign' => ['sometimes', function ($attribute, $value, $fail) {
                $profile = DB::table('profile')
                    ->where([
                        ['callsign', '=', $value]
                    ])
                    ->get();
                if (!$profile)
                    return $fail("Не найден водитель с таким позывным");
                else
                    return true;
            }],
        ], $this->messages());
        if ($validator->fails()) {
            response()->json($validator->errors(), 403);
        } else {
            $profile = null;
            if ($callsign = $request->get('callsign')) {
                $profile = DB::table('profile')
                    ->where([
                        ['callsign', '=', $callsign]
                    ])
                    ->first();
                $profile = $profile->id;
            }
            $this->driver_comment = $request->get('driver_comment');
            $this->driver_id = $profile;
            $this->car_id = null;
            if (!$result = $this->save()) {
                return response()->json($result, 403);
            } else {
                return response()->json($result, 200);
            }

        }
        return response()->json("false", 403);
    }

    public function setCancelled()
    {
        $user = Auth::user();
        if ($user->hasRole('legal')) {
            if ($this->status != 4 && $this->status != 5) {
                $this->status = 5;
                $this->car_id = null;
                return response()->json($this->save(), 200);
            } else {
                return response()->json(['message' => 'Нельзя отменить'], 403);
            }
        } else if ($user->hasRole('admin')) {
            $this->status = 5;
            $this->car_id = null;
            return response()->json($this->save(), 200);
        } else
            return response()->json(['message' => 'Нельзя отменить'], 403);
    }

    public function getAddressesArray()
    {
        $addresses = $this->address()->get()->implode('where', ', ');
        return $addresses;
    }

    public function address(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(OrderAddress::class, 'order_id')->orderBy('id');
    }

    public function client(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(User::class, 'legal_id');
    }

    public function driver(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(User::class, 'driver_id');
    }

    public function car(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Car::class, 'car_id');
    }
}
