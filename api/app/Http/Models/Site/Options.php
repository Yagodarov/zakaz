<?php


namespace App\Http\Models\Site;


use Illuminate\Support\Facades\Input;
use PhpParser\Node\Expr\Array_;
use Validator;

class Options extends \Eloquent
{
  protected $fillable = [
    'sms_login',
    'sms_password',
    'sms_sender',
    'cash_box_login',
    'cash_box_password',
    'cash_box_company_id',
    'cash_box_request_source',
  ];

  protected function rules(\Request $request = null) {
    return [
      'sms_login' => 'sometimes',
      'sms_password' => 'sometimes',
      'sms_sender' => 'sometimes',
      'cash_box_login' => 'sometimes',
      'cash_box_password' => 'sometimes',
      'cash_box_company_id' => 'sometimes',
      'cash_box_request_source' => 'sometimes',
    ];
  }

  public function store(Array $array) {
    $validator = Validator::make($array, $this->rules());
    if ($validator->fails()) {
      return response()->json($validator->messages(), 403);
    }
    else
    {
      $this->fill($array);
      if ($result = $this->save())
      {
        return response()->json($result, 200);
      }
      else
        return response()->json($result, 403);
    }
  }

}
