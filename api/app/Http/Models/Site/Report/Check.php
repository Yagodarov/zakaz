<?php


namespace App\Http\Models\Site\Report;


use App\Http\Models\Site\Order\Order;
use Illuminate\Database\Eloquent\Model;

class Check extends Model
{
    protected $order;
    protected $checkTemplate;
    protected $fillable = [
        'id',
        'calculation_type',
        'sum',
        'cash_sum',
        'emoney_sum',
        'subject_name',
    ];

    function __construct(Order $order)
    {
        $this->order = $order;
        $this->setId();
        $this->setAttributesFromOrder();
        parent::__construct();
    }

    protected function setId()
    {
        $this->setAttribute('id', $this->order->getAttribute('id'));
    }

    protected function setAttributesFromOrder()
    {
        $price = $this->order->getAttribute('price') * 100;
        if ($this->order->getAttribute('payment') == 'cash') {
            $this->setAttribute('cash_sum', $price);
        } else {
            $this->setAttribute('emoney_sum', $price);
        }
        $this->setAttribute('sum', $price);
        $this->setAttribute('calculation_type', 1);
        $this->setAttribute('tax', '6');

        $this->checkTemplate = (new CheckTemplate($this->order));
    }

    public function returnObjectForUrl() {
        $subjectName = $this->checkTemplate->getFilteredMessage();
        $object = (object)[
            'CheckId' => $this->getAttribute('id'),
            'CalculationType' => $this->getAttribute('calculation_type'),
            'Phone' => $this->order->getAttribute('number'),
            'Sum' => $this->getAttribute('sum'),
            'Pay' => (object)[
                'CashSum' => $this->getAttribute('cash_sum'),
                'EMoneySum' => $this->getAttribute('emoney_sum'),
            ],
            'Subjects' => [ (object)[
                'SubjectName' => $subjectName,
                'Price' => $this->order->getAttribute('price') * 100,
                'Quantity' => 1,
                'Tax' => 6
            ]]
        ];
        return $object;
    }

    public function canSend() {
        return $this->checkTemplate->getCanSend();
    }

    public function canSendByOrderPayment() {
        if ($this->order->getAttribute('payment') == 'cash' || $this->order->getAttribute('payment') == 'card') {
            return true;
        } else {
            return false;
        }
    }

}
