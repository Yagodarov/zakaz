<?php


namespace App\Http\Models\Site\Report;


use App\Http\Models\Site\Order\Order;
use Ixudra\Curl\Facades\Curl;

class ReportStatus
{
    protected $order;
    protected $checkRequest;
    protected $urlGetRequestStatus = "https://api.kit-invest.ru/WebService.svc/StateCheck";
    function __construct(Order $order) {
        $this->order = $order;
        $this->setParameters();;
    }
    public function setParameters() {
        $this->checkRequest = (new CheckRequest($this->order));
    }
    public function getStatus() {
        $reportObject = (object)[
            'Request' => $this->checkRequest->getParamsObject(),
//            'CheckNumber' => $this->order->getAttribute('id')
            'CheckNumber' => $this->order->getAttribute('id')
        ];
        return $response = Curl::to($this->urlGetRequestStatus)
            ->withData(json_encode($reportObject))
            ->post();
    }
}
