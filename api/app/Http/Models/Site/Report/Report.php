<?php
namespace App\Http\Models\Site\Report;

use Ixudra\Curl\Facades\Curl;

/**
 * Class Report
 * @var $check Check
 * @package App\Http\Models\Site\Report
 */
class Report
{
	protected $checkRequest;
	protected $check;
	protected $order;
    protected $urlSendRequest = "https://api.kit-invest.ru/WebService.svc/SendCheck";
	function __construct($order) {
        $this->order = $order;
        $this->setCheckAndRequest();
	}

	public function setCheckAndRequest() {
		$this->checkRequest = (new CheckRequest($this->order));
		$this->check = new Check($this->order);
	}

	public function sendReport() {
        $reportObject = (object) [
            'Request' => $this->checkRequest->getParamsObject(),
            'Check' => $this->check->returnObjectForUrl()
        ];

    \Log::info('response', [
      'can_send' => $this->check->canSend(),
      'payment' => $this->check->canSendByOrderPayment()
    ]);
        if ($this->check->canSend() && $this->check->canSendByOrderPayment()) {
            $response = Curl::to($this->urlSendRequest)
                ->withData(json_encode($reportObject))
                ->post();

          \Log::info('response', [$response]);
          \Log::info('object', [$reportObject]);
            return $response;
        }
	}
}
