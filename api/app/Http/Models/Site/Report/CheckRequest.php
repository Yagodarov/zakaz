<?php

namespace App\Http\Models\Site\Report;

use App\Http\Models\Options;
use App\Http\Models\Site\Order\Order;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\Model;

class CheckRequest extends Model
{
    public $md5;
    protected $order;
    protected $fillable = [
        'company_id',
        'login',
        'sign',
        'password',
        'request_source',
        'hash'
    ];

    public function __construct(Order $order) {
        parent::__construct();
        $this->order = $order;
        $this->setAuthorizeParams();
        $this->setSignAttribute();
        $this->setRequestId();
    }

    public function setAuthorizeParams() {
        $options = Options::latest()->first();
        $this->setAttribute('company_id', $options->getAttribute('cash_box_company_id'));
        $this->setAttribute('login', $options->getAttribute('cash_box_login'));
        $this->setAttribute('request_source', $options->getAttribute('cash_box_request_source'));
        $this->setAttribute('password', $options->getAttribute('cash_box_password'));
    }

    public function setSignAttribute() {
        $string = $this->getAttribute('company_id');
        $string .= $this->getAttribute('password');
        $string .= $this->order->getAttribute('id');
        $md5 = md5($string);
        $this->md5 = $md5;
    }

    public function setRequestId() {
        $this->setAttribute('id', $this->order->getAttribute('id'));
    }

    public function getParamsObject() {
        $object = (object)[
            "CompanyId"      => $this->getAttribute("company_id"),
            "RequestId"      => $this->getAttribute("id"),
            "UserLogin"      => $this->getAttribute("login"),
            "Sign"           => $this->md5,
            "RequestSource"  => $this->getAttribute("request_source"),
        ];
        return $object;
    }
}
