<?php


namespace App\Http\Models\Site\Balance;


use App\Http\Models\Site\User\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class DriverBalance extends \Eloquent
{
	protected $table = 'driver_balance';
	protected $errors = [];
	protected $fillable = [
		'type',
		'driver_id',
		'user_id',
		'count',
		'value',
		'comment',
		'added_at'
	];

	public static function getCurrentDriverBalance() {
	    $balances = DriverBalance::all()->where('driver_id', '=', \Auth::user()->getAuthIdentifier());
	    $result = 0;
	    foreach ($balances as $balance) {
	        if ($balance['type'] == 'increment_by_user' || $balance['type'] == 'increment_by_status') {
                $result += $balance['value'];
            } else {
	            $result -= $balance['value'];
            }
        }
	    return $result;
    }

	public function driver() {
		return $this->hasOne(User::class, 'driver_id');
	}

	public function user() {
		return $this->belongsTo(User::class, 'user_id');
	}

	public function search(Request $request) {
		$query = self::where('driver_id', $request->get('driver_id'))
    ->with('user.profile');
		$models = $query->get();
		$count = $models->count();
		return [
			'models' => $models,
			'count' => $count
		];
	}

	public function store(Request $request)
	{
		$this->fill($request->only($this->fillable));
		if (!$this->comment) {
		  $this->errors = [
		    'Введите комментарий'
      ];
		  return false;
    }
		$this->setAttribute('user_id', \Auth::user()->getAttribute('id'));
		$this->setAttribute('added_at', Carbon::now()->toDateTimeString());
		try {
			$this->save();
			return true;
		} catch (\Exception $exception) {
			$this->errors = $exception->getMessage();
			return false;
		}
	}

	public function getMessages() {
		return $this->errors;
	}
}
