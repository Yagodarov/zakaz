<?php


namespace App\Http\Models\Site\Car\CarRepair;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

/**
 * Class CarRepairSearch
 * @property CarRepair $mainModel
 * @property Builder $query
 * @property Request $request
 * @package App\Http\Models\Site\Car
 */
class CarRepairSearch
{
	protected $mainModel;
	protected $query;
	protected $request;
	protected $searchable = [
		'car_id',
		'changed_by_user_id',
		'created_by_user_id',
		'status'
	];

	public function __construct(Request $request) {
		$this->setRequest($request);
		$this->setMainModel();
	}

	public function setRequest(Request $request) {
		$this->request = $request;
	}

	public function setMainModel() {
		$this->mainModel = new CarRepair();
	}

	public function setWhereSearch() {
		$queryWhere = [];
		foreach ($this->searchable as $key => $value) {
			if ($this->request->get($value)) {
				$queryWhere[] = [
					$value, '=', $this->request->get($value)
				];
			}
		}
		$this->query->where($queryWhere);
	}

	public function getModels() {
		return $this->query->get();
	}

	public function initQuery() {
		$this->query = $this->mainModel->newQuery();
	}

	public function getResult() {
		return $this->getModels();
	}

	public function getRelations() {
		$this
			->query
			->with('changedBy.profile')
			->with('car')
			->with('createdBy.profile');
	}

	public function search() {
		$this->initQuery();
		$this->setWhereSearch();
		$this->getRelations();
		return [
			'models' => $this->getResult()
		];
	}
}
