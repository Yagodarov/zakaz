<?php


namespace App\Http\Models\Site\Car\CarRepair;


use App\Http\Models\Car;
use App\Http\Requests\Site\Car\CarRepairCreateFormRequest;
use App\Http\Requests\Site\Car\CarRepairDeclineFormRequest;
use Illuminate\Support\Facades\Auth;

class CarRepair extends \Eloquent
{
	const STATUS_DECLINED = 0;
	const STATUS_REQUESTED = 1;
	const STATUS_ACCEPTED = 2;
	const FILL_ON_CREATE = [
		'car_id',
		'changed_by_user_id',
		'created_by_user_id',
		'status',
		'comment'
	];

	public function fillCreateAttributes($request) {
		$this->setAttribute('status', self::STATUS_REQUESTED);
		$this->setAttribute('created_by_user_id', Auth::user()->getAttribute('id'));
		$this->setAttribute('car_id', $request->post('car_id'));
		$this->setAttribute('comment', $request->post('comment'));
	}

	public static function createAndGetResult(CarRepairCreateFormRequest $request) {
		try {
			$model = new self();
			$model->fillCreateAttributes($request);
			$model->save();
		} catch (\Exception $exception) {
			return [
				'message' => $exception->getMessage(),
				'status' => 'failure'
			];
		}
		return [
			'message' => 'Успешно сохранено',
			'status' => 'success'
		];
	}
	public function declineAndGetResult() {
		try {
			$this->setStatusAndUserId(self::STATUS_DECLINED);
		} catch (\Exception $exception) {
			return [
				'message' => $exception->getMessage(),
				'status' => 'failure'
			];
		}
		return [
			'message' => 'Успешно отменено',
			'status' => 'success'
		];
	}
	public function acceptAndGetResult() {
		try {
			if ($this->changeCarStatusById(1)) {
				$this->setStatusAndUserId(self::STATUS_ACCEPTED);
			}
		} catch (\Exception $exception) {
			return [
				'message' => $exception->getMessage(),
				'status' => 'failure'
			];
		}
		return [
			'message' => 'Успешно принято',
			'status' => 'success'
		];
	}

	public function setStatusAndUserId($status) {
		$this->setAttribute('status', $status);
		$this->setAttribute('changed_by_user_id', Auth::user()->getAttribute('id'));
		$this->save();
	}

	public function changeCarStatusById($status) {
		return $this->car->setAttribute('repairing', $status)->save();
	}

	public function changedBy() {
		return $this->belongsTo(User::class, 'changed_by_user_id');
	}

	public function car() {
		return $this->belongsTo(Car::class);
	}

	public function createdBy() {
		return $this->belongsTo(User::class, 'created_by_user_id');
	}
}
