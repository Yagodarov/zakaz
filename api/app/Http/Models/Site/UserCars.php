<?php

namespace App\Http\Models\Site;

use App\Http\Models\Api\v4\Car;
use App\Http\Models\Site\User\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
class UserCars extends Model
{
    protected $table = 'user_cars';

    public static function search(Request $request)
	{
		$rows = DB::table('user_cars as uc')
            ->select('uc.id as id', 'uc.blocked', 'uc.default', DB::raw('CONCAT_WS(" ", cars.mark, cars.model, cars.car_number) as name'))
            ->leftJoin('cars', 'cars.id', '=', 'uc.car_id')
            ->where('uc.user_id', '=', $request->get('id'))
            ->when($request->get('orderBy'), function ($rows) use ($request) {
				return $rows
					->orderBy($request->get('orderBy'), $request->get('desc') == 'true' ? 'desc' : 'asc');
			})
			->when(!$request->get('orderBy'), function ($rows) use ($request) {
				return $rows
					->orderBy('uc.id', 'desc');
			})
			->groupBy('id')
            ->get();
		return response()->json([
			'models' => $rows
		]);
	}

	public function setDefault()
	{
		$this->default = !$this->default;
		if ($this->default == 1)
			DB::table('user_cars')->where('user_id', $this->user_id)->update(['default' => 0]);
		$this->save();
	}

    public function rules(Request $request)
    {
    	return [
    		'user_id' => 'required',
    		'car_id' => 'min:1|required|numeric|unique:user_cars,car_id'
    	];
    }
	public function messages() {
		return [
			'required' => 'Заполните это поле',
			'min' => 'Не менее :min символа(-ов)',
			'max' => 'Не более :max символа(-ов)',
			'car_id.min' => 'Заполните это поле',
			'unique' => 'Уже используется',
			'car_id.unique' => 'Уже используется или используется у другого водителя',
			'email' => 'Введите правильный формат email',
		];
	}


    protected $fillable = [
        'user_id', 'car_id', 'blocked', 'default'
    ];

    public function store(Request $request)
    {
		$validator = Validator::make(Input::all(), $this->rules($request), $this->messages());
		if ($validator->fails()) {
			return response()->json($validator->messages(), 403);
		}
		else
		{
			$this->fill($request->all());
			if ($result = $this->save())
			{
				return response()->json($result, 200);
			}
			else
				return response()->json($result, 403);
		}
    }

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function car() {
        return $this->hasOne(Car::class);
    }
}
