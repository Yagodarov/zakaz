<?php


namespace App\Http\Models\Site;


use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Validator;
class SmsSettings extends \Eloquent
{
  protected $table = "sms_settings";
  protected $fillable = ['event', 'template', 'send_sms', 'event_id'];

  public function rules(Request $request = null)
  {
    return [
      'event' => 'required',
      'template' => 'required',
      'send_sms' => 'required',
      'event_id' => 'required',
    ];
  }

  public static function search(Request $request)
  {
    return self::orderBy('id', 'asc')->take(8)->get();
  }

  public function store(Request $request)
  {
    $validator = Validator::make(Input::all(), $this->rules($request));
    if ($validator->fails()) {
      return response()->json($validator->messages(), 403);
    } else {
      $this->fill($request->all());
      if ($result = $this->save()) {
        $this->saveFile($request);
        return response()->json($result, 200);
      } else
        return response()->json($result, 403);
    }
  }

  public static function storeArray(Array $array): bool {
    $ids = [];
    if ($array) {
      usort($array, function($a, $b) {
        if ($a['event_id'] == $b['event_id'] ) {
          return 0;
        }
        return ($a['event_id']  < $b['event_id'] ) ? -1 : 1;
      });
      foreach ($array as $template) {
        $validator = Validator::make($template, (new self())->rules());
        if ($validator->fails()) {
          self::deleteByIds($ids);
          return false;
        } else {
          $newModel = (new self())->fill($template);
          if (!$result = $newModel->save()) {
            (new self())->deleteByIds($ids);
            return false;
          } else {
            $ids[] = $newModel->getAttribute('id');
          }
        }
      }
    }
    self::deleteFirsts();
    return true;
  }

  public static function deleteFirsts() {
    \DB::table('sms_settings')->orderBy('id', 'asc')->take(8)->delete();
  }

  public static function deleteByIds($ids) {
    \DB::table('sms_settings')->whereIn('id', $ids)->delete();
  }

  public static function getExampleSmsMessage() {
    return "Example message";
  }
}
