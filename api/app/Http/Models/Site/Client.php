<?php


namespace App\Http\Models\Site;


use Exception;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Client extends Authenticatable implements JWTSubject
{
    protected $fillable = [
        'first_name',
        'last_name',
        'phone',
        'token'
    ];

    protected $hidden = [
        'password',
        'token'
    ];

    public function saveUid(Request $request) {
        if (!$uid = ClientUid::where('uid', $request->get('uid'))->first()) {
            $client_uid = new ClientUid();
            $client_uid->setAttribute('uid', $request->get('uid'));
            $client_uid->setAttribute('client_id', $this->getAttribute('id'));
            $client_uid->save();
        }
    }

    public function store(Request $request)
    {
        $this->fill($request->only($this->fillable));
        try {
            $this->save();
            return true;
        } catch (Exception $exception) {
            return false;
        }
    }

    public function uid()
    {
        return $this->hasMany(ClientUid::class);
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
