<?php

namespace App\Http\Models\Site;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
class Schedule extends Model
{
	public $timestamps = false;
    protected $table = 'schedule';

    public static function search(Request $request)
	{
		$rows = self::query()
            ->where('user_id', '=', $request->get('user_id'))
            ->when($request->get('orderBy'), function ($rows) use ($request) {
				return $rows
					->orderBy($request->get('orderBy'), $request->get('desc') == 'true' ? 'desc' : 'asc');
			})
			->when(!$request->get('orderBy'), function ($rows) use ($request) {
				return $rows
					->orderBy('schedule.id', 'desc');
			})
			->groupBy('id')
            ->get();
		return response()->json([
			'models' => $rows
		]);
	}

    public function rules(Request $request)
    {
    	return [
    		'user_id' => 'required',
    		'date_start' => 'required|date',
    		'date_end' => 'required|date',
    	];
    }
	public function messages() {
		return [
			'required' => 'Заполните это поле',
			'min' => 'Не менее :min символа(-ов)',
			'max' => 'Не более :max символа(-ов)',
			'car_id.min' => 'Заполните это поле',
			'date' => 'Введите время',
			'unique' => 'Уже используется',
			'email' => 'Введите правильный формат email',
		];
	}

//
//	protected $casts = [
//	    'date_start' => 'timestamp',
//	    'date_end' => 'timestamp'
//	];

    protected $fillable = [
        'id', 'user_id', 'date_start', 'date_end'
    ];

    public function store(Request $request)
    {
		$validator = Validator::make(Input::all(), $this->rules($request), $this->messages());
		if ($validator->fails()) {
			return response()->json($validator->messages(), 403);
		}
		else
		{
			$this->fill($request->all());
			if ($result = $this->save())
			{
				return response()->json($this, 200);
			}
			else
				return response()->json($result, 403);
		}
    }

	public function setDateStartAttribute($value) {
		$this->attributes['date_start'] = (new Carbon($value))->toDateTimeString();
	}

	public function setDateEndAttribute($value) {
		$this->attributes['date_end'] = (new Carbon($value))->toDateTimeString();
	}
}
