<?php
/**
 * Created by PhpStorm.
 * User: Alexey
 * Date: 08.04.2019
 * Time: 0:05
 */

namespace App\Http\Models\Site;

use Illuminate\Database\Eloquent\Model;

class ClientUid extends Model
{
    public $table = 'uid';

    protected $fillable = [
        'id',
        'uid',
        'token',
        'client_id',
    ];

}
