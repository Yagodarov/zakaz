<?php

namespace App\Http\Models\Site\Maps;

abstract class MapLength
{
    abstract public function getLength(array $addresses, int $time): int;
}
