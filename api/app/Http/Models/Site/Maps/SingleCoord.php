<?php

namespace App\Http\Models\Site\Maps;

class SingleCoord
{
    public $lat;
    public $lng;
    public function __construct(string $lat, string $lng) {
        $this->lat = $lat;
        $this->lng = $lng;
    }
}
