<?php

namespace App\Http\Models\Site\Maps;

use App\Http\Models\Site\Maps\Google\GoogleMap as GoogleMap;
use App\Http\Models\Site\Maps\Yandex\YandexMap as YandexMap;

class Maps
{
  public $serveTime = 0;

  public function getLength($addresses, $time)
  {
    $time = 0;
    $mapClass = @\Config::get("maps.default");
    $className = $mapClass;
    $instance = \App::make($className);
    return $instance->getLength($addresses, $time);
  }

}
