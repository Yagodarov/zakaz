<?php

namespace App\Http\Models\Site\Maps\Coords\DaData;

use App\Http\Models\Site\Maps\MapCoords;
use App\Http\Models\Site\Maps\SingleCoord;

class DaDataCoords extends MapCoords
{
    public function getCoords(array $addresses): array {
      $coords = [];
      $token = env("DADATA_TOKEN");
      $secret = env("DADATA_SECRET");
      $dadata = new \Dadata\DadataClient($token, $secret);
      foreach ($addresses as $address) {
        $result = $dadata->clean("address", $address);
        $coords[] = new SingleCoord($result['geo_lat'], $result['geo_lon']);
      }
      return $coords;
    }
}
