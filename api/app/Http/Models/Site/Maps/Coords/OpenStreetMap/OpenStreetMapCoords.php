<?php

namespace App\Http\Models\Site\Maps\Coords\OpenStreetMap;

use App\Http\Models\Site\Maps\MapCoords;
use App\Http\Models\Site\Maps\SingleCoord;

class OpenStreetMapCoords extends MapCoords
{
    public function getCoords(array $addresses): array
    {
        $coords = [];
        foreach ($addresses as $address) {
            $coords[] = $this->parseAddress($address);
        }
        return $coords;
    }

    protected function parseAddress(string $address): SingleCoord
    {
        $query = str_replace(" ","+", $address);
        $search_url = "https://nominatim.openstreetmap.org/?addressdetails&q=".$query."&format=json&limit=1";

        $httpOptions = [
            "http" => [
                "method" => "GET",
                "header" => "User-Agent: Nominatim-Test"
            ]
        ];

        $streamContext = stream_context_create($httpOptions);
        $json = file_get_contents($search_url, false, $streamContext);

        $decoded = json_decode($json, true);
        $lat = $decoded[0]['lat'];
        $lng = $decoded[0]['lon'];
        try {
            $singleCoord = new SingleCoord($lat, $lng);
            return $singleCoord;
        } catch (\Exception $exception) {
            throw new HttpException("Не удалось рассчитать маршрут", 422);
        }
    }

    protected function getQuery($addresses)
    {

    }
}
