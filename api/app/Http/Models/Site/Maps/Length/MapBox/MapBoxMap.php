<?php

namespace App\Http\Models\Site\Maps\Length\MapBox;

use App\Http\Models\Site\Maps\MapCoords;
use App\Http\Models\Site\Maps\MapLength;

class MapBoxMap extends MapLength
{
    protected $mapCoords;
    public function __construct(MapCoords $daDataCoords)
    {
        $this->mapCoords = $daDataCoords;
    }

    public function getLength(array $addresses, int $time): int
    {
        $coords = $this->getCoords($addresses);
        $endpoint = "https://api.mapbox.com/directions/v5/mapbox/driving/
      " . $coords;
        $client = new \GuzzleHttp\Client();

        $response = $client->request('GET', $endpoint, [
            'query' => [
                'alternatives' => 'false',
                'geometries' => 'geojson',
                'language' => 'en',
                'overview' => 'simplified',
                'steps' => 'false',
                'access_token' => env('MAPBOX_TOKEN')
            ]
        ]);
        $content = json_decode($response->getBody(), true);
        $distance = $content['routes'][0]['distance'];
        return $distance;
    }

    public function getCoords($addresses): string
    {
        $urlStringAddress = "";
        $coords = $this->mapCoords->getCoords($addresses);
        foreach ($coords as $coord) {
            $urlStringAddress .= $coord->lng . "," . $coord->lat . ";";
        }
        $resultString = substr_replace($urlStringAddress, "", -1);
        return $resultString;
    }
}
