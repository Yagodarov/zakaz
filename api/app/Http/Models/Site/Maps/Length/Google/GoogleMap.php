<?php

namespace App\Http\Models\Site\Maps\Length\Google;

use App\Http\Models\Site\Maps\MapLength;

class GoogleMap extends MapLength
{
  public function getLength(array $addresses, int $time): int
  {
    $apiKey = \Config::get("maps.google-map")["apiKey"][0];

    $length = 0;
    if ($time < time() || $time == null) {
      $time = null;
    }
    foreach ($addresses as $key => $value) {
      if ($key > 0 && $addresses[$key - 1]) {
        $from = str_replace(' ', '+', $addresses[$key - 1]);
        $to = str_replace(' ', '+', $addresses[$key]);
        $url = 'https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=' . $from . '&destinations=' . $to . '&key=' . $apiKey . '&mode=driving';
        if ($time) {
          $url = $url . '&departure_time=' . $time;
        }
        $ch = curl_init();
        // set URL and other appropriate options
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $output = json_decode(curl_exec($ch));
        $lengthCurrent = @$output->rows[0]->elements[0]->distance->value;
        $this->serveTime = @$output->rows[0]->elements[0]->duration->value;

        if ($lengthCurrent) {
          $length += $lengthCurrent;
        } else {
          return $length = 0;
        }
      }
    }

    return $length;
  }
}
