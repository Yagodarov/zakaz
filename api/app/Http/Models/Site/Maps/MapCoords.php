<?php

namespace App\Http\Models\Site\Maps;

abstract class MapCoords
{
    protected $coords;
    abstract public function getCoords(array $addresses): array;
}
