<?php

namespace App\Http\Models\Site\User;

use App\Http\Models\Site\Balance\DriverBalance;
use Illuminate\Support\Facades\App;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
	use HasRoles;
	use Notifiable;
	protected $guard_name = 'api';
	public $pw;

	public static function search(Request $request)
	{
		$users =  User::with('roles')
			->with('driver_balance')
			->select('users.id', 'users.name', 'users.email', 'roles.name as role', 'profile.name as profile_name', 'profile.phone', 'users.blocked')
			->leftJoin('model_has_roles', 'model_has_roles.model_id', '=', 'users.id')
			->leftJoin('roles', 'model_has_roles.role_id', '=', 'roles.id')
			->leftJoin('profile', 'profile.id', '=', 'users.id')
			->when($request->get('id'), function($users) use ($request){
				return $users->where('users.id', 'LIKE', "%{$request->get('id')}%");
			})
			->when($request->get('profile_name'), function($users) use ($request){
				return $users->where('profile.name', 'LIKE', "%{$request->get('profile_name')}%");
			})
			->when($request->get('phone'), function($users) use ($request){
				return $users->where('profile.phone', 'LIKE', "%{$request->get('phone')}%");
			})
			->when($request->get('email'), function($users) use ($request){
				return $users->where('users.email', 'LIKE', "%{$request->get('email')}%");
			})
			->when($request->get('orderBy'), function ($users) use ($request) {
				return $users
					->orderBy($request->get('orderBy'), $request->get('desc') == 'true' ? 'desc' : 'asc');
			})
			->when(!$request->get('orderBy'), function ($users) use ($request) {
				return $users
					->orderBy('users.id', 'desc');
			})
			->when($request->get('blocked') > -1, function ($users) use ($request){
				return $users->where('users.blocked', '=', $request->get('blocked'));
			})
			->when($request->get('role'), function ($users) use ($request){
				return $users->where('roles.name', '=', $request->get('role'));
			})
			->when(!$request->get('role'), function ($users) {
				return $users->whereIn('roles.name', [
					'driver-manager',
					'admin',
					'legal',
				]);
			})
			->groupBy('id');

		$count = $users->get()->count();
		$users = $users
			->with("roles")
			->when($request->get('page') >= 0 && !$request->get('all'), function ($users) use ($request){
				return $users->skip($request->get('page') * 10)->take(10);
			})
			->get();
		return response()->json([
			'users' => $users,
			'count' => $count
		]);
	}

	public static function getLegalsList(Request $request)
	{
		$users =  DB::table('users')
		->select('users.id as id', 'profile.name as name')
		->leftJoin('model_has_roles', 'model_has_roles.model_id', '=', 'users.id')
		->leftJoin('roles', 'model_has_roles.role_id', '=', 'roles.id')
		->leftJoin('profile', 'users.id', '=', 'profile.id')
		->where('roles.name', '=', 'legal')
		->get();
		return response()->json([
			'models' => $users,
		]);
	}

	public function rules(Request $request)
	{
		if ($request->isMethod('post')) {
			return [
				'name' => 'required|min:4|max:191|unique:users,name',
				'password' => 'nullable|min:4|max:191',
				'email' => 'required|email|unique:users,email',
				'rolesUpdate' => 'required'
			];
		}
		elseif ($request->isMethod('put')) {
			return [
				'name' => 'required|min:4|max:191|unique:users,name,'.$request->get('id'),
				'password' => 'sometimes|min:4|max:191',
				'email' => 'required|email|unique:users,email,'.$request->get('id'),
				'rolesUpdate' => 'required'
			];
		}
		else {
			abort(403);
		}
	}

	public function store(Request $request)
	{
		$validator = Validator::make(Input::all(), $this->rules($request), $this->messages());
		if ($validator->fails()) {
			return response()->json($validator->messages(), 403);
		}
		else
		{
			$this->fill($request->all());
			if ($this->password == ''){
                $this->pw = str_random(6);
	            $this->password = bcrypt($this->pw);
            }
            else
            {
                $this->pw = $request->password;
	            $this->password = bcrypt($request->password);
            }
			if ($result = $this->save())
			{
				$profile = Profile::create(['id' => $this->id]);
				$profile->save();

				$this->setRoles($request);
				$this->sendRegistrationEmail($this);
				return response()->json($result, 200);
			}
			else
				return response()->json($result, 403);
		}
	}

	public function storeUpdate(Request $request)
    {
        $validate = Validator::make($request->all(), $this->rules($request), $this->messages());
        if (!$validate->fails() && $user = User::find($request->get('id')))
        {
        	$this::find($request->get('id'));
	        $this->fill($request->all());
	        $this->pw = $request->password;
	        if (!$request->password)
	        {
		        $this->password = $this->getOriginal('password');
	        }
	        else
	        {
		        $this->password = bcrypt($request->password);
	        }
	        if ($this->pw || ($this->getOriginal('name') != $this->name)) $this->sendUpdateUserEmail($this);
            if ($result = $this->save())
            {
	            $this->setRoles($request);
                return response()->json($result, 200);
            }
            else
            {
                return response()->json($result, 403);
            }
        }
        else
        {
            return response()->json($validate->errors(), 403);
        }
    }

    public function setRoles(Request $request)
    {
    	$userRoles = $this->getRoleNames();
   		foreach ($userRoles as $userRole) {
   			$this->removeRole($userRole);
   		}

    	$newRoles = $request->get('rolesUpdate');
    	foreach ($newRoles as $roleName => $boolean) {
    		if ($newRoles[$roleName] == true)
    		{
    			$this->assignRole($roleName);
    		}
    	}
    	$this->setTrueRoles($newRoles);
    }

    public function getRolesAccordingDiverManager() {
	    $result = $this->getRoleNames()[0];
	    $allRoles =  $this->getRoleNames();
	    if(count($allRoles) == 2 && $allRoles->contains('driver-manager') && $allRoles->contains('driver')) {
            $result = 'driver-manager';
        }
	    return $result;
    }

    public function setTrueRoles($newRoles)
    {
    	if (
    		$this->hasRoleInArray($newRoles, 'driver-manager')
    		&&
    		!$this->hasRoleInArray($newRoles, 'driver')
    		)
    	{
    		$this->assignRole('driver');
    	}
    }

    public function hasRoleInArray($roles, $roleName)
    {
    	return (array_key_exists($roleName, $roles) && ($roles[$roleName] == true));
    }

    public function cars()
    {
    	return $this->belongsToMany('App\Http\Models\Car', 'user_cars', 'user_id', 'car_id')->withPivot('car_id');
    }

    public function driver_balance() {
		return $this->hasMany(DriverBalance::class, 'driver_id');
    }

	public function messages() {
		return [
			'required' => 'Заполните это поле',
			'min' => 'Не менее :min символа(-ов)',
			'max' => 'Не более :max символа(-ов)',
			'unique' => 'Уже используется',
			'email' => 'Введите правильный формат email',
		];
	}

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */

    protected $fillable = [
        'id', 'name', 'email', 'password', 'blocked'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function sendUpdateUserEmail($user)
    {
	    if ($this->pw)
	    {
		    $messsage = 'Логин: '.$user->name.'. Пароль: '.$this->pw.'.'.' Для сайта '.$this->getUrl();
	    }
	    else
	    {
		    $messsage = 'Логин: '.$user->name.' Для сайта '.$this->getUrl();
	    }
        return Mail::raw($messsage, function ($message) use ($user){
            $message->to($user->email);
            $message->subject('Данные для входа на сайт были обновлены');
        });
    }

    public function sendRegistrationEmail($user)
    {
        $messsage = 'Логин: '.$user->name.'. Пароль: '.$this->pw.'.'.' Для сайта '.$this->getUrl();
        return Mail::raw($messsage, function ($message) use ($user){
	            $message->to($user->email);
            $message->subject('Данные для входа на сайт');
        });
    }
    public function getUrl()
    {
    	$url = App::make('url')->to('/');
    	return parse_url($url, PHP_URL_SCHEME).'://'.str_replace('api', '', parse_url($url, PHP_URL_HOST));
    }

	public function getJWTIdentifier()
	{
		return $this->getKey();
	}

	/**
	 * Return a key value array, containing any custom claims to be added to the JWT.
	 *
	 * @return array
	 */
	public function getJWTCustomClaims()
	{
		return [];
	}

	public function profile()
	{
		return $this->hasOne('App\Http\Models\User\Profile', 'id');
	}

	public static function getDrivers(Request $request)
	{
		$query = User::role('driver');
		$count = $query->get()->count();
		$drivers = $query->with('cars')->with('profile')->get();
//		foreach($drivers as $driver)
//		{
//			$driver->cars;
//			$driver->profile;
//		}
		return response()->json([
			'models' => $drivers,
			'count' => $count
		], 200);
	}
}
