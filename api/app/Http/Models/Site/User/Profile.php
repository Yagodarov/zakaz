<?php
namespace App\Http\Models\Site\User;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;

class Profile extends \Eloquent
{
	protected $table = 'profile';
	public $timestamps = false;
	protected $legal = [
		'id',
		'name',
		'requisites',
		'phone',
		'address',
		'legal_address',
		'contract_period_start',
		'contract_period_end',
		'contract_number',
		'weekend',
		'request_price',
		'transfer',
		'order_cost'
	];

	protected $driver = [
		'id',
		'name',
		'requisites',
		'phone',
		'address',
		'legal_address',
		'contract_period_start',
		'contract_period_end',
		'contract_number',
		'weekend',
		'request_price',
		'transfer',
		'order_cost'
	];

	protected $fillable = [
		'id',
		'name',
		'requisites',
		'phone',
		'address',
		'legal_address',
		'contract_period_start',
		'contract_period_end',
		'contract_number',
		'weekend',
		'request_price',
		'transfer',
		'order_cost',
		'callsign',
		'token',
		'sms_code'
	];

	public function rules(Request $request)
	{
		if ($request->isMethod('post')) {
			return [
				'name' => 'sometimes|max:191',
				'requisites' => 'sometimes',
				'phone' => 'sometimes|max:191|unique:profile,phone',
				'address' => 'sometimes|max:191',
				'legal_address' => 'sometimes|max:191',
				'contract_period_start' => 'sometimes|date|nullable',
				'contract_period_end' => 'sometimes|date|nullable',
				'contract_number' => 'sometimes|numeric|nullable',
				'weekend' => 'sometimes|numeric|nullable',
				'request_price' => 'sometimes|numeric|nullable',
				'transfer' => 'sometimes|numeric|nullable',
				'order_cost' => 'sometimes|numeric|nullable',
				'callsign' => 'sometimes|max:191',
			];
		}
		elseif ($request->isMethod('put')) {
			return [
				'name' => 'sometimes|max:191',
				'requisites' => 'sometimes',
				'phone' => 'sometimes|unique:profile,phone,'.$request->get('id'),
				'address' => 'sometimes|max:191',
				'legal_address' => 'sometimes|max:191',
				'contract_period_start' => 'sometimes|date|nullable',
				'contract_period_end' => 'sometimes|date|nullable',
				'contract_number' => 'sometimes|numeric|nullable',
				'weekend' => 'sometimes|numeric|nullable',
				'request_price' => 'sometimes|numeric|nullable',
				'transfer' => 'sometimes|numeric|nullable',
				'order_cost' => 'sometimes|numeric|nullable',
				'callsign' => 'sometimes|max:191,'.$request->get('id'),
			];
		}
	}

	public function user()
    {
        return $this->hasOne('App\Http\Models\User\User', 'id');
    }

	public function store(Request $request)
	{
		$validator = Validator::make(Input::all(), $this->rules($request), $this->messages());
		if ($validator->fails()) {
			return response()->json($validator->messages(), 403);
		}
		else
		{
			$this->fill($request->all());
			if ($result = $this->save())
			{
				return response()->json($result, 200);
			}
			else
				return response()->json($result, 403);
		}
	}

	public function storeUpdate(Request $request)
	{
		$validate = Validator::make($request->all(), $this->rules($request), $this->messages());
		if (!$validate->fails())
		{
			$this->fill($request->all());
			if ($result = $this->save())
			{
				return response()->json($result, 200);
			}
			else
			{
				return response()->json($result, 403);
			}
		}
		else
		{
			return response()->json($validate->errors(), 403);
		}
	}


	public function messages() {
		return [
			'required' => 'Заполните это поле',
			'min' => 'Не менее :min символа(-ов)',
			'max' => 'Не более :max символа(-ов)',
			'unique' => 'Уже используется',
			'email' => 'Введите правильный формат email',
		];
	}

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
}
