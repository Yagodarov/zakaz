<?php

namespace App\Http\Models\Site;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
class UserContract extends Model
{
    protected $table = 'user_contract';

    public static function search(Request $request)
	{
		$rows = DB::table('user_contract as uc')
            ->where('uc.user_id', '=', $request->get('user_id'))
            ->when($request->get('date_now'), function ($rows) use ($request) {
            	return $rows->whereRaw("uc.contract_period_start <= curdate()")
            	->whereRaw("uc.contract_period_end >= curdate()");
            })
            ->when($request->get('orderBy'), function ($rows) use ($request) {
				return $rows
					->orderBy($request->get('orderBy'), $request->get('desc') == 'true' ? 'desc' : 'asc');
			})
			->when(!$request->get('orderBy'), function ($rows) use ($request) {
				return $rows
					->orderBy('uc.id', 'desc');
			})
			->groupBy('id')
            ->get();
		return response()->json([
			'models' => $rows
		]);
	}

    public function rules(Request $request)
    {
    	return [
    		'user_id' => 'required',
    		'contract_number' => 'required|min:1|numeric|unique:user_contract,contract_number,NULL,id,user_id,'.$request->get('user_id'),
    		'contract_period_start' => ['required','date','nullable','checkDates'],
			'contract_period_end' => ['required','date','nullable','checkDates'],
			'request_price' => 'required|numeric|nullable',
			'transfer' => 'required|numeric|nullable',
			'order_cost' => 'required|numeric|nullable',
    	];
    }

    public function rulesUpdate(Request $request)
    {
    	return [
    		'user_id' => 'required',
    		'contract_number' => 'required|min:1|numeric|unique:user_contract,contract_number,'.$request->get('id').',id,user_id,'.$request->get('user_id'),
    		'contract_period_start' => ['required','date','nullable','checkDates'],
			'contract_period_end' => ['required','date','nullable', 'checkDates'],
			'request_price' => 'required|numeric|nullable',
			'transfer' => 'required|numeric|nullable',
			'order_cost' => 'required|numeric|nullable',
    	];
    }

	public function messages() {
		return [
			'required' => 'Заполните это поле',
			'min' => 'Не менее :min символа(-ов)',
			'max' => 'Не более :max символа(-ов)',
			'car_id.min' => 'Заполните это поле',
			'unique' => 'Уже используется',
			'email' => 'Введите правильный формат email',
			'check_dates' => 'Даты совпадают с другим контрактом'
		];
	}


    protected $fillable = [
        'user_id', 'contract_number', 'contract_period_start', 'contract_period_end', 'request_price', 'order_cost', 'transfer'
    ];

    public function store(Request $request)
    {
    	$this->extendValidators($request);
		$validator = Validator::make(Input::all(), $this->rules($request), $this->messages());
		if ($validator->fails()) {
			return response()->json($validator->messages(), 403);
		}
		else
		{
			$this->fill($request->all());
			if ($result = $this->save())
			{
				return response()->json($result, 200);
			}
			else
				return response()->json($result, 403);
		}
    }

    public function storeUpdate(Request $request)
    {
    	$this->extendValidators($request);
		$validator = Validator::make(Input::all(), $this->rulesUpdate($request), $this->messages());
		if ($validator->fails()) {
			return response()->json($validator->messages(), 403);
		}
		else
		{
			$this->fill($request->all());
			if ($result = $this->save())
			{
				return response()->json($result, 200);
			}
			else
				return response()->json($result, 403);
		}
    }

    public function extendValidators($request)
    {
    	Validator::extend('checkDates', function($attribute, $value, $fail) use($request){
			$rows = DB::table('user_contract')
				->where([
					['user_id', '=', $request->get('user_id')],
					['contract_number', '<>', $request->get('contract_number')]
				])
				->get();
			$date_start = $request->get('contract_period_start');
			$date_end = $request->get('contract_period_end');
			if ($date_start && $date_end)
				if ($rows)
				{
					foreach($rows as $row)
					{
						//Если дата конца входит в промежуток
						if (((strtotime($row->contract_period_end) >= strtotime($date_end)) &&  (strtotime($date_end) >= strtotime($row->contract_period_start)))
						//Если дата начала входит в промежуток
							|| ((strtotime($row->contract_period_start) <= strtotime($date_start)) &&  (strtotime($date_start) <= strtotime($row->contract_period_end)))
						//Если дата начала меньше даты начала а дата конца больше даты конца
							|| ((strtotime($row->contract_period_start) >= strtotime($date_start)) && (strtotime($date_end) >= strtotime($row->contract_period_end)))
						//Если дата начала меньше даты конца а дата конца больше даты конца
							// || (
							// 		(strtotime($date_start) < (strtotime($row->contract_period_end)))
							// 		&&
							// 		(strtotime($date_end) > (strtotime($row->contract_period_end))))
						)
						{
							return false;
						}
					}
				}
				else
					return true;
			return true;
		});
    }
}
