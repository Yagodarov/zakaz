<?php

namespace App\Http\Models\Site;

class Maps
{
    public $serveTime = 0;
	public function getLength($addresses, $time)
	{
	  $time = null;
		$yandexKey = @\Config::get("maps.yandex-map")["apiKey"][0];
		$googleKey = @\Config::get("maps.google-map")["apiKey"][0];
    if($googleKey)
    {
      return $this->getLengthGoogle($addresses, $time);
    }
		if ($yandexKey)
		{
			return $this->getLengthYandex($addresses, $time);
		}
		return 0;
	}

	public function getLengthGoogle($addresses, $time)
	{
		$apiKey = \Config::get("maps.google-map")["apiKey"][0];

		$length = 0;
		if ($time < time() || $time == null)
		{
			$time = null;
		}
		foreach($addresses as $key => $value)
		{
			if ($key > 0 && $addresses[$key-1])
			{
				$from = str_replace(' ', '+', $addresses[$key-1]);
				$to = str_replace(' ', '+', $addresses[$key]);
				$url =  'https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins='.$from.'&destinations='.$to.'&key='.$apiKey.'&mode=driving';
				if ($time)
				{
					$url = $url.'&departure_time='.$time;
				}
				$ch = curl_init();
				// set URL and other appropriate options
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_HEADER, 0);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				$output = json_decode(curl_exec($ch));
				$lengthCurrent = @$output->rows[0]->elements[0]->distance->value;
                $this->serveTime = @$output->rows[0]->elements[0]->duration->value;

				if ($lengthCurrent)
				{
					$length += $lengthCurrent;
				}
				else
				{
					// abort(403, $url);
					return $length = 0;
				}
			}
		}

        //var_dump($length); die();
		return $length;
	}

	public function getLengthYandex($addresses, $time)
	{
		$coords = $this->getCoordsYandex($addresses);
		$apiKey = \Config::get("maps.yandex-map")["apiKey"][0];
		$length = 0;
		if ($time < time())
		{
			$time = null;
		}
		foreach($coords as $key => $value)
		{
			if ($key > 0 && $coords[$key-1])
			{
				$url = 'https://api.routing.yandex.net/v1.0.0/distancematrix?origins='.
					$coords[$key-1]['y'].','.$coords[$key-1]['x'].
					"&destinations=".
					$coords[$key]['y'].','.$coords[$key]['x'].
					"&apikey=".$apiKey."&mode=transit";
				if ($time)
				{
					$url = $url."&departure_time=".$time;
				}
				$ch = curl_init();
				// set URL and other appropriate options
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_HEADER, 0);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				$output = json_decode(curl_exec($ch));
				if(isset($output->errors) || !isset($output->rows))
				{

					$url = 'https://api.routing.yandex.net/v1.0.0/distancematrix?origins='.
					$coords[$key-1]['y'].','.$coords[$key-1]['x'].
					"&destinations=".
					$coords[$key]['y'].','.$coords[$key]['x'].
					"&apikey=".$apiKey."&mode=transit";
					curl_setopt($ch, CURLOPT_URL, $url);
					$output = json_decode(curl_exec($ch));
				}
				if(isset($output->errors))
				{
					// $length = 0;break;
					return $this->getLengthGoogle($addresses, $time);
				}
				$length += $output->rows[0]->elements[0]->distance->value;
				curl_close($ch);
			}
		}
		return $length;
	}

	public function getCoordsYandex($arrayOfAddresses)
	{
		$carToStartCoords = [];
		foreach ($arrayOfAddresses as $key => $value) {
			$url = "https://geocode-maps.yandex.ru/1.x/?geocode=".$value."&format=json";
			$ch = curl_init();

			// set URL and other appropriate options
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$output = json_decode(curl_exec($ch));
			$points = explode(" ", $output->response->GeoObjectCollection->featureMember[0]->GeoObject->Point->pos);
			$carToStartCoords[] = [
				'x' => $points[0],
				'y' => $points[1],
			];
			curl_close($ch);
		}
		return $carToStartCoords;
	}
}
