<?php

namespace App\Http\Middleware;

use App\Http\Models\Site\Client;
use Closure;
use Illuminate\Support\Facades\Auth;

class Transfer
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (strlen($request->get('phone')) < 10) {
            return response()->json([
                'message' => 'Отстутствует номер'
            ], 403);
        }
		$user = Client::where([
		    ['phone', 'LIKE', "%".substr($request->get('phone')."%", -9)],
        ])->first();
		if (!$user) {
		    $user = new Client();
		    $user->fill($request->all());
		    $user->save();
		}
		Auth::login($user);
		return $next($request);
    }
}
