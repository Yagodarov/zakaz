<?php

namespace App\Http\Middleware;

use App\Http\Models\Site\Client;
use Closure;
use Illuminate\Support\Facades\Auth;

class AndroidId
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      $uId = $request->get('uid');
		if (!$uId) {
		    return response()->json([
		        'message' => 'Отстутствует uid'
		    ], 403);
		}
		$user = Client::whereHas('uid', function ($query) use ($uId) {
		    $query->where('uid', '=', $uId);
		})->first();
		if (!$user) {
		    $user = new Client();
		    $user->fill($request->all());
		    $user->save();
		    $user->saveUid($request);
		} else {
		    $user->fill($request->all());
		    $user->save();
		}
		Auth::login($user);
		return $next($request);
    }
}
