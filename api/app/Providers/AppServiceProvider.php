<?php

namespace App\Providers;

use App\Http\Models\Site\Maps\Coords\DaData\DaDataCoords;
use App\Http\Models\Site\Maps\Coords\OpenStreetMap\OpenStreetMapCoords;
use App\Http\Models\Site\Maps\Length\MapBox\MapBoxMap;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
	    Schema::defaultStringLength(191);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

        $this->app->bind(MapBoxMap::class, function ($app) {
            return new MapBoxMap(new DaDataCoords());
        });
    }
}
