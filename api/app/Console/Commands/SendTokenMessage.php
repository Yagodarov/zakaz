<?php

namespace App\Console\Commands;

use App\Http\Models\Order;
use Illuminate\Console\Command;
use App\Http\Models\PushNotification;
class SendTokenMessage extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:name';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $order = Order::query()->orderBy('id', 'desc')->first();
        $orderSms = new Order\OrderSms($order);
        var_dump($orderSms->sendSms());
    }
}
