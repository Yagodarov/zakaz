<?php

namespace App\Console\Commands;

use App\Http\Models\Order;
use App\Http\Models\Site\Report\ReportStatus;
use Illuminate\Console\Command;

class GetCheckState extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:state';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $order = Order::first();
        $status = (new ReportStatus($order))->getStatus();
        echo $status;
    }
}
