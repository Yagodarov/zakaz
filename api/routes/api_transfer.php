<?php
/**
 * Created by PhpStorm.
 * User: Acer
 * Date: 19.04.2018
 * Time: 0:34
 */

Route::middleware(['transfer'])->post('client/order', 'API\transfer\OrderController@create');
Route::middleware(['transfer'])->post('client/order-rent', 'API\transfer\OrderController@createRent');
Route::post('client/cars', 'API\transfer\CarController@getCarsForOrderTransfer');
Route::get('client/order', 'API\transfer\OrderController@get');
Route::post('client/order-decline', 'API\transfer\OrderController@decline');
