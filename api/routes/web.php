<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::post('authenticate', 'Site\Auth\LoginController@authenticate');
Route::post('auth-phone', 'Site\Auth\LoginController@authenticatePhone');
Route::post('auth-sms', 'Site\Auth\LoginController@authenticateSms');
Route::group(['middleware' => ['jwt.auth', 'role:driver|admin|legal']], function () {
    Route::post('order-status', 'Site\OrderController@status');

    Route::post('report', 'Site\ReportsController@index');
    Route::get('report/legalPdf', 'Site\ReportsController@legalPdf');
    Route::get('report/legalTable', 'Site\ReportsController@legalTable');
    Route::post('report/payment_or_debt', 'Site\ReportsController@paymentOrDebt');

    Route::get('user/{id}', 'Site\User\UserController@get');
    Route::put('user/{id}', 'Site\User\UserController@update');

    Route::get('profile/{id}', 'ProfileController@get');

    Route::get('user-schedules', 'ScheduleController@get');
    Route::delete('user-schedule/{id}', 'ScheduleController@delete');
    Route::post('user-schedule/', 'ScheduleController@store');
    Route::post('user-schedule-update/{id}', 'ScheduleController@update');


});
Route::group(['middleware' => ['jwt.auth', 'role:driver|admin']], function () {
    Route::post('profile/set-token', 'Auth\LoginController@setPushToken');
    Route::put('order-decline/', 'OrderController@declineDriver');
    Route::get('user-cars/', 'UserCarsController@get');

});
Route::group(['middleware' => ['jwt.auth', 'role:admin|legal']], function () {

    Route::post('order-driver-price', 'OrderController@getDriverPrice');
    Route::post('order', 'OrderController@store');
    Route::post('order-select', 'OrderController@storeSelect');

    Route::get('order-cancel/{id}', 'OrderController@cancelOrder');
    Route::get('order/{id}', 'OrderController@get');

});

Route::group(['middleware' => ['jwt.auth', 'role:admin|legal|driver-manager']], function () {
    Route::get('user-contracts/', 'Site\User\UserContractController@index');
    Route::get('car/{id}', 'Site\CarController@get');
    Route::put('order/{id}', 'Site\OrderController@update');
    Route::put('order-select/{id}', 'Site\OrderController@updateSelect');
});

Route::middleware(['jwt.auth', 'role:admin'])->get('car-repair', 'Site\Car\CarRepairController@index');
Route::middleware(['jwt.auth', 'role:admin'])->post('car-repair-accept', 'Site\Car\CarRepairController@accept');
Route::middleware(['jwt.auth', 'role:admin'])->post('car-repair-decline', 'Site\Car\CarRepairController@decline');

Route::group(['middleware' => ['jwt.auth', 'role:driver|admin|legal|driver-manager']], function () {
    Route::get('orders/', 'Site\OrderController@index');
    Route::get('order/{id}', 'Site\OrderController@get');
    Route::get('order-select/{id}', 'Site\OrderController@getSelect');
    Route::get('users/', 'Site\User\UserController@index');
    Route::get('cars/', 'Site\Car\CarController@index');
    Route::get('orders/', 'Site\OrderController@index');
    Route::post('order-price', 'Site\OrderController@getPrice');
    Route::post('car-order-list', 'Site\Car\CarController@getCarsForOrder');
    Route::post('car-by-order-id/{id}', 'Site\Car\CarController@getCarsByOrderId');
});

Route::group(['middleware' => ['jwt.auth', 'role:admin|driver-manager']], function () {
    Route::get('legal-list/', 'Site\User\UserController@legalList');
});

Route::group(['middleware' => ['jwt.auth', 'role:admin']], function () {
    Route::get('driver_balance', 'Site\DriverBalanceController@list');
    Route::post('driver_balance', 'Site\DriverBalanceController@store');

    Route::get('sms_settings', 'Site\SmsSettingController@list');
    Route::post('sms_settings', 'Site\SmsSettingController@update');

    Route::get('cash_box_settings', 'Site\CashBoxSettingController@list');
    Route::post('cash_box_settings', 'Site\CashBoxSettingController@update');

    Route::put('profile/{id}', 'Site\ProfileController@update');

    Route::get('user-contract/{id}', 'Site\User\UserContractController@get');
    Route::post('user-contract/', 'Site\User\UserContractController@store');
    Route::put('user-contract/{id}', 'Site\User\UserContractController@update');
    Route::delete('user-contract/{id}', 'Site\User\UserContractController@delete');

    Route::delete('user-car/{id}', 'Site\User\UserCarsController@delete');
    Route::get('user-car-block/{id}', 'Site\User\UserCarsController@block');
    Route::get('user-car-default/{id}', 'Site\User\UserCarsController@default');
    Route::post('user-car/', 'Site\User\UserCarsController@store');

    Route::delete('user/{id}', 'Site\User\UserController@delete');
    Route::post('user', 'Site\User\UserController@store');

    Route::get('clients', 'Site\ClientController@index');

    Route::delete('car/{id}', 'Site\CarController@delete');
    Route::post('car', 'Site\CarController@store');
    Route::put('car/{id}', 'Site\CarController@update');

    Route::delete('order/{id}', 'Site\OrderController@delete');

    Route::delete('profile/{id}', 'Site\ProfileController@delete');
    Route::post('profile', 'Site\ProfileController@store');
});

Route::group([
    'prefix' => 'v4',
], function () {
    require base_path('routes/api_v4.php');
});
Route::group([
    'prefix' => 'v3',
], function () {
    require base_path('routes/api_v3.php');
});
Route::group([
    'prefix' => 'v2',
], function () {
    require base_path('routes/api_v2.php');
});
Route::group([
    'prefix' => 'transfer',
], function () {
    require base_path('routes/api_transfer.php');
});
