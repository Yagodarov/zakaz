<?php
/**
 * Created by PhpStorm.
 * User: Acer
 * Date: 19.04.2018
 * Time: 0:34
 */
Route::post('auth-phone', 'API\v4\Auth\LoginController@authenticatePhone');
Route::post('auth-sms', 'API\v4\Auth\LoginController@authenticateSms');

Route::middleware(['jwt.auth', 'role:driver|admin|legal|driver-manager'])->get('orders', 'API\v4\OrderController@index');
Route::middleware(['jwt.auth', 'role:admin|driver|driver-manager'])->post('order-status', 'API\v4\OrderController@status');
Route::middleware(['jwt.auth', 'role:driver|driver-manager'])->post('order-decline', 'API\v4\OrderController@declineOrder');

Route::middleware(['jwt.auth', 'role:driver|driver-manager'])->post('schedule-batch-store', 'API\v4\ScheduleController@massStore');
Route::middleware(['jwt.auth', 'role:driver'])->get('schedule/', 'API\v4\ScheduleController@index');
Route::middleware(['jwt.auth', 'role:driver'])->post('schedule/', 'API\v4\ScheduleController@store');
Route::middleware(['jwt.auth', 'role:driver'])->post('schedule-update', 'API\v4\ScheduleController@update');
Route::middleware(['jwt.auth', 'role:driver'])->post('schedule-delete', 'API\v4\ScheduleController@delete');

Route::middleware(['jwt.auth', 'role:driver'])->post('car-repair', 'API\v4\Car\CarRepairController@create');
Route::middleware(['jwt.auth', 'role:driver'])->delete('car-repair', 'API\v4\Car\CarRepairController@delete');

Route::middleware(['jwt.auth', 'role:driver'])->get('driver-cars', 'API\v4\CarController@driverCars');

Route::middleware(['jwt.auth', 'role:admin|legal|driver-manager'])->get('drivers', 'API\v4\UserController@getDrivers');

Route::middleware(['jwt.auth', 'role:admin|legal|driver|driver-manager'])->get('profile/{id}', 'API\v4\ProfileController@get');

Route::middleware(['jwt.auth', 'role:driver|driver-manager'])->post('update-car-tariff', 'API\v4\CarController@updateTariff');

Route::middleware(['uid'])->post('client/order', 'API\v4\OrderController@createClient');
Route::middleware(['uid'])->get('client/orders', 'API\v4\OrderController@indexClient');
Route::middleware(['uid'])->post('client/order-status', 'API\v4\OrderController@statusClient');
Route::middleware(['uid'])->post('client/order-rent', 'API\v4\OrderController@createRent');
Route::middleware(['uid'])->post('client/cars', 'API\v4\CarController@getCarsForOrder');
