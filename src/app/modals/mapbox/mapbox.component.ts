import {Component, Inject, OnInit} from '@angular/core';
import mapboxgl from 'mapbox-gl';
import MapboxLanguage from '@mapbox/mapbox-gl-language';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import MapboxGeocoder from '@mapbox/mapbox-gl-geocoder';
class DialogData {
}

@Component({
  selector: 'app-mapbox',
  templateUrl: './mapbox.component.html',
  styleUrls: ['./mapbox.component.scss']
})
export class MapboxComponent implements OnInit {
    address: string;
    newMarker: any = null;
  constructor(public dialogRef: MatDialogRef<MapboxComponent>,
              @Inject(MAT_DIALOG_DATA) public data: DialogData) { }

  ngOnInit() {
      this.initMap();
  }

    public choose() {
        this.dialogRef.close(this.address);
    }

  public closeDialog() {
      this.dialogRef.close();
  }

    public mapClickFn(e) {
        const url =
            'https://api.mapbox.com/geocoding/v5/mapbox.places/' +
            e.lngLat.lng +
            ',' +
            e.lngLat.lat +
            '.json?access_token=' +
            'pk.eyJ1IjoidHJhbnNmZXJ1ZmEiLCJhIjoiY2tkZnMxbHhhMTZ0MDJ6cXFnYWtrMjZqbCJ9.ZoiTrMNQApjnjqSKS6blvQ' +
            '&types=address';
        $.get(url, (data) => {
            if (data.features.length > 0) {
                const address = data.features[0].place_name;
                this.address = address;
                console.log(address);
            } else {
                console.log('No address found');
            }
        });
    }

    initMap(): void {
      const accessToken = 'pk.eyJ1IjoidHJhbnNmZXJ1ZmEiLCJhIjoiY2tkZnMxbHhhMTZ0MDJ6cXFnYWtrMjZqbCJ9.ZoiTrMNQApjnjqSKS6blvQ';
        const map = new mapboxgl.Map({
            container: 'map-container', // container ID
            center: [54.7305, 55.9647], // starting position [lng, lat]
            zoom: 10, // starting zoom
            style: 'mapbox://styles/mapbox/streets-v11', // style URL or style object
            locale: 'ru',
            localizeLabels: 'ru',
            hash: true, // sync `center`, `zoom`, `pitch`, and `bearing` with URL
            accessToken: accessToken,
            transformRequest: (url, resourceType) => {
                if (resourceType === 'Source' && url.startsWith('http://localhost')) {
                    return {
                        url: url.replace('http', 'https'),
                        headers: {'my-custom-header': true},
                        credentials: 'include'  // Include cookies for cross-origin requests
                    };
                }
            }
        });
        const language = new MapboxLanguage();
        map.addControl(language);
        map.on('click', (e) => {
            console.log(e);
            if (this.newMarker) {
                this.newMarker.remove();
            }
            this.newMarker = new mapboxgl.Marker()
                .setLngLat([e.lngLat.lng, e.lngLat.lat])
                .addTo(map);
            this.mapClickFn(e);
        });
        const geocoder = new MapboxGeocoder({
            accessToken: accessToken,
            mapboxgl: mapboxgl
        });

        document.getElementById('geocoder').appendChild(geocoder.onAdd(map));
    }
}
