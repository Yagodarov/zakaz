/**
 * Created by Алексей on 09.02.2018.
 */
import {Resolve} from '@angular/router';
import {Schedule} from '../_models/schedule';
import {Observable} from 'rxjs';
import {RouterStateSnapshot} from '@angular/router';
import {ActivatedRouteSnapshot} from '@angular/router';
import {Injectable} from '@angular/core';
import {UserScheduleService} from '../_services/user.schedule.service';

@Injectable()
export class UserScheduleResolver implements Resolve<Schedule[]> {
    constructor(private service: UserScheduleService) {

    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Schedule[]> {
        return this.service.getAll({
            params: {
                user_id: Number(route['_urlSegment']['segments'][1]['path'])
            }
        });
    }

}
