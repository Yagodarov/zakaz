/**
 * Created by Алексей on 09.02.2018.
 */
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {User} from '../_models';
import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';
import {UserService} from '../_services';

@Injectable()
export class DriverEditResolver implements Resolve<User> {
    constructor(private service: UserService) {

    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<User> {
      console.log(route);
      return this.service.getById(Number(route['_urlSegment']['segments'][2]['path']));
    }

}
