/**
 * Created by Алексей on 09.02.2018.
 */
import {Resolve} from "@angular/router";
import {Contract} from "../_models/contract";
import {Observable} from "rxjs";
import {RouterStateSnapshot} from "@angular/router";
import {ActivatedRouteSnapshot} from "@angular/router";
import {Injectable} from "@angular/core";
import {UserContractService} from "../_services/user.contract.service";

@Injectable()
export class UserContractResolver implements Resolve<Contract[]>{
  constructor(private service: UserContractService) {

  }
  resolve(route:ActivatedRouteSnapshot, state:RouterStateSnapshot):Observable<Contract[]> {
    return this.service.getAll({
    	params : {
    		user_id : Number(route['_urlSegment']['segments'][1]['path'])
    	}
  });
  }

}