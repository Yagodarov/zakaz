/**
 * Created by Алексей on 09.02.2018.
 */
import {Resolve} from "@angular/router";
import {Observable} from "rxjs";
import {RouterStateSnapshot} from "@angular/router";
import {ActivatedRouteSnapshot} from "@angular/router";
import {Injectable} from "@angular/core";

import {Car} from "../_models/car";
import {CarService} from "../_services/car.service";

@Injectable()
export class CarsOrderResolver implements Resolve<Car[]>{
    constructor(private carService: CarService) {

    }
    resolve(route:ActivatedRouteSnapshot, state:RouterStateSnapshot):Observable<Car[]> {
        return this.carService.getByOrderId(Number(route.paramMap.get('id')));
    }

}
