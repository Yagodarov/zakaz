/**
 * Created by Алексей on 09.02.2018.
 */
import {Resolve} from "@angular/router";
import {User} from "../_models/user";
import {Observable} from "rxjs";
import {RouterStateSnapshot} from "@angular/router";
import {ActivatedRouteSnapshot} from "@angular/router";
import {Injectable} from "@angular/core";
import {UserService} from "../_services/user.service";

@Injectable()
export class DriversResolver implements Resolve<User[]>{
  constructor(private userService: UserService) {

  }
  resolve(route:ActivatedRouteSnapshot, state:RouterStateSnapshot):Observable<User[]> {
    return this.userService.getAll({params:{role:'driver', all:true}});
  }

}
