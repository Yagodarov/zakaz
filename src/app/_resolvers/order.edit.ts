/**
 * Created by Алексей on 09.02.2018.
 */
import {Resolve} from "@angular/router";
import {Order} from "../_models/order";
import {Observable} from "rxjs";
import {RouterStateSnapshot} from "@angular/router";
import {ActivatedRouteSnapshot} from "@angular/router";
import {Injectable} from "@angular/core";
import {OrderService} from "../_services/order.service";

@Injectable()
export class OrderEditResolver implements Resolve<Order>{
    constructor(private service: OrderService) {

    }
    resolve(route:ActivatedRouteSnapshot, state:RouterStateSnapshot):Observable<Order> {
        return this.service.getById(Number(route.paramMap.get('id')));
    }

}