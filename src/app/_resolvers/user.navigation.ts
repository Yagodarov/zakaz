/**
 * Created by Алексей on 09.02.2018.
 */
import {Resolve, ActivatedRoute} from "@angular/router";
import {User} from "../_models/user";
import {Observable} from "rxjs";
import {RouterStateSnapshot} from "@angular/router";
import {ActivatedRouteSnapshot} from "@angular/router";
import {Injectable} from "@angular/core";
import {UserService} from "../_services/user.service";

@Injectable()
export class UserNavigationResolver implements Resolve<User>{
	id: any;
	constructor(private userService: UserService, private router:ActivatedRoute) {

	}
  resolve(route:ActivatedRouteSnapshot, state:RouterStateSnapshot):Observable<User> {
    return this.userService.getById(Number(route.paramMap.get('id')));
  }

}