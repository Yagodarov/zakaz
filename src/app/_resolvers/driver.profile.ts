/**
 * Created by Алексей on 09.02.2018.
 */
import {Resolve} from "@angular/router";
import {Profile} from "../_models/profile";
import {Observable} from "rxjs";
import {RouterStateSnapshot} from "@angular/router";
import {ActivatedRouteSnapshot} from "@angular/router";
import {Injectable} from "@angular/core";
import {UserProfileService} from "../_services/user-profile.service";

@Injectable()
export class DriverProfileResolver implements Resolve<Profile>{
  constructor(private userProfileService: UserProfileService) {

  }
  resolve(route:ActivatedRouteSnapshot, state:RouterStateSnapshot):Observable<Profile> {
    return this.userProfileService.getById(Number(route['_urlSegment']['segments'][2]['path']));
  }

}
