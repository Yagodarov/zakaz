﻿import {RouterModule} from '@angular/router';

import {HomeComponent} from './pages/home';
import {LoginComponent} from './pages/login';
import {RegisterComponent} from './pages/register';
import {AuthGuard} from './_guards';
import {UsersResolver} from './_resolvers/user.resolve';
import {UserCarsResolver} from './_resolvers/user.cars';
import {UserEditResolver} from './_resolvers/user.edit';
import {UserProfileResolver} from './_resolvers/user.profile';
import {UserContractResolver} from './_resolvers/user.contract';
import {DriversResolver} from './_resolvers/driver';
import {LegalsResolver} from './_resolvers/legal';
import {UserScheduleResolver} from './_resolvers/user.schedule';

import {UserComponent} from './pages/user/user.component';
import {UserEditComponent} from './pages/user/user-edit/user-edit.component';
import {UserProfileComponent} from './pages/user/user-profile/user-profile.component';
import {CarsListResolver} from './_resolvers/car.list';
import {UserNavigationComponent} from './pages/user/user-navigation/user-navigation.component';
import {UserNavigationResolver} from './_resolvers/user.navigation';
import {OrdersResolver} from './_resolvers/order.resolve';
import {OrderEditResolver} from './_resolvers/order.edit';
import {OrderSelectEditResolver} from './_resolvers/order.select.edit';
import {OrdersComponent} from './pages/order/orders/orders.component';
import {OrderCreateComponent} from './pages/order/order-create/order-create.component';
import {OrderEditComponent} from './pages/order/order-edit/order-edit.component';
import {UserCarsComponent} from './pages/user/user-cars/user-cars.component';
import {UserContractsComponent} from './pages/user/user-contracts/user-contracts.component';
import {UserScheduleComponent} from './pages/user/user-schedule/user-schedule.component';
import {OrderSelectComponent} from './pages/order/order-select/order-select.component';
import {OrderSelectEditComponent} from './pages/order/order-select-edit/order-select-edit.component';
import {OrderClientEditComponent} from './pages/order/order-client-edit/order-client-edit.component';
import {ClientsResolver} from './_resolvers/clients';
import {CashBoxSettingsComponent} from './pages/cash-box-settings/cash-box-settings.component';

const appRoutes: any = [
    {
        path: 'reports',
        loadChildren: './pages/reports/reports.module#ReportsModule',
    },
    {
        path: 'drivers',
        loadChildren: './pages/drivers/drivers.module#DriversModule',
    },
    {
        path: 'clients',
        loadChildren: './pages/clients/clients.module#ClientsModule',
    },
    {
        path: 'sms-settings',
        loadChildren: './pages/sms-settings/sms-settings.module#SmsSettingsModule'
    },
    {
        path: 'users',
        component: HomeComponent,
        canActivate: [AuthGuard],
        resolve: {
            data: UsersResolver
        }
    },
    {
        path: 'cash-box-settings',
        component: CashBoxSettingsComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'users/new',
        component: UserComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'users/:id',
        component: UserNavigationComponent,
        canActivate: [AuthGuard],
        resolve: {
            data: UserNavigationResolver
        },
        runGuardsAndResolvers: 'always',
        children: [
            {path: '', redirectTo: 'edit', pathMatch: 'full'},
            {
                path: 'edit',
                component: UserEditComponent,
                runGuardsAndResolvers: 'always',
                resolve: {
                    data: UserEditResolver
                },
            },
            {
                path: 'profile',
                component: UserProfileComponent,
                resolve: {
                    data: UserProfileResolver
                },
            },
            {
                path: 'contract',
                component: UserContractsComponent,
                resolve: {
                    data: UserContractResolver
                },
            },
            {
                path: 'cars',
                component: UserCarsComponent,
                resolve: {
                    data: UserCarsResolver
                },
            },
            {
                path: 'schedule',
                component: UserScheduleComponent,
                resolve: {
                    data: UserScheduleResolver
                },
            },
        ]
    },
    {
        path: 'users/delete/:id',
        component: UserComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'cars',
        loadChildren: 'app/pages/car/car.module#CarModule',
    },
    {
        path: 'orders',
        component: OrdersComponent,
        canActivate: [AuthGuard],
        resolve: {
            data: OrdersResolver,
            carsList: CarsListResolver,
            driversList: DriversResolver,
            legalsList: LegalsResolver,
        }
    },
    {
        path: 'orders/new',
        component: OrderCreateComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'orders/select',
        component: OrderSelectComponent,
        canActivate: [AuthGuard]
    },
    {
        path: 'orders/:id/edit',
        component: OrderEditComponent,
        canActivate: [AuthGuard],
        resolve: {
            data: OrderEditResolver
        }
    },
    {
        path: 'orders/:id/client-edit',
        component: OrderClientEditComponent,
        canActivate: [AuthGuard],
        resolve: {
            data: OrderEditResolver,
            cars: CarsListResolver,
            drivers: DriversResolver,
            clients: ClientsResolver,
            legalsList: LegalsResolver,
        }
    },
    {
        path: 'orders/:id/select-edit',
        component: OrderSelectEditComponent,
        canActivate: [AuthGuard],
        resolve: {
            data: OrderSelectEditResolver
        }
    },
    {
        path: 'orders/delete/:id',
        component: OrdersComponent,
        canActivate: [AuthGuard]
    },
    {path: 'login', component: LoginComponent},
    {path: 'register', component: RegisterComponent},

    // otherwise redirect to home
    {path: '**', redirectTo: ''}
];

export const routing = RouterModule.forRoot(appRoutes);
