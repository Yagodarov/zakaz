﻿import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { AlertService, AuthenticationService } from '../../_services';
import { StorageHelper } from '../../_helpers/storage.helper';
@Component({
    moduleId: module.id.toString(),
    templateUrl: 'login.component.html'
})

export class LoginComponent implements OnInit {
    model: any = {};
    loading = false;
    returnUrl: string;
    error : string;
    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private storage: StorageHelper,
        private authenticationService: AuthenticationService,
        private alertService: AlertService) { }

    ngOnInit() {
        // reset login status
        this.authenticationService.logout();

        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/orders';
    }

    login() {
        this.loading = true;
        this.error = "";
        this.authenticationService.login(this.model.name, this.model.password)
            .subscribe(
                data => {
                    var role = data['roles'];
                    this.router.navigate([this.storage.getReturnUrl()]);
                },
                error => {
                    console.log(error);
                    this.error = error['error'];
                    this.loading = false;
                });
    }
}
