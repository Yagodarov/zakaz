import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportsCompanyPdfComponent } from './reports-company-pdf.component';

describe('ReportsCompanyPdfComponent', () => {
  let component: ReportsCompanyPdfComponent;
  let fixture: ComponentFixture<ReportsCompanyPdfComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportsCompanyPdfComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportsCompanyPdfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
