import {Component, OnInit, Pipe, PipeTransform} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { saveAs } from 'file-saver';
import {ColumnMode} from '@swimlane/ngx-datatable';
import {log} from 'util';
@Component({
  selector: 'app-reports-company-pdf',
  templateUrl: './reports-company-pdf.component.html',
  styleUrls: ['./reports-company-pdf.component.css']
})
export class ReportsCompanyPdfComponent implements OnInit {
  form = {
    date_from: null,
    date_to: null,
    legal: null,
    stamp_sign: null
  };
  companiesList: any = [];
  ColumnMode = ColumnMode;
  rows = [

  ];

  paymentPipe(value: any) {
    const paymentOptions = {
      'cash' : 'Наличные',
      'noncash' : 'Безналичный',
      'card' : 'Банковская карта'
    };
    if (paymentOptions[value]) {
      return paymentOptions[value];
    } else {
      return 'Нет типа оплаты';
    }
  }

  clientPipe(value: any, row: any) {
    if (row) {
      return row['client']['profile']['name'];
    } else {
      return 'Нет клиента';
    }
  }
  carPipe(value: any) {
    if (value) {
      return value['mark'] + ' ' + value['model'] + ' ' + value['color'] + ' ' + value['car_number'];
    } else {
      return 'Нет авто';
    }
  }
  statusPipe(value: any) {
      const statusObject = {
        '0': 'Новый',
        '1': 'В обработке',
        '2': 'Принят',
        '3': 'На исполнении',
        '4': 'Выполнен',
        '5': 'Отменен',
        '6': 'Отменен с оплатой',
        '7': 'Ожидает подтверждения водителем',
    };
      if (statusObject[value]) {
        return statusObject[value];
      } else {
        return 'Неверный статус';
      }
  }

  addressPipe(value: any) {
    let result = '';
    value.forEach(item => {
      result += item['where'];
      result += ' ';
    });
    return result;
  }

  driverPipe(value: any, row: any) {
    if (row['driver'] && row['driver']['profile']) {
      return row['driver']['profile']['name'];
    } else {
      return 'Нет водителя';
    }
  }

  getTotalSum() {
    let total = 0;
    this.rows.forEach((item) => {
      total += item.price;
    });
    return total;
  }

  constructor(
    private http: HttpClient
  ) {
    this.loadCompanies();
  }

  loadCompanies() {
    this.http.get('/api/users?role=legal&all=true').subscribe((data) => {
      this.companiesList = data['users'];
    });
  }

  reportTable() {
    this.rows = [];
    this.http.get('/api/report/legalTable', {
      params: this.form
    }).subscribe((data: any) => {
      this.rows = data['original'];
    });
  }

  reportPdf(print: any) {
      const params = this.form;
      params.stamp_sign = print;
      this.http.get('/api/report/legalPdf', {
        params: params
      }).subscribe((data) => {
        this.getPdf(data).subscribe((res) => {
          saveAs(res, data);
        });
      });
  }

  checkIsFormFilled() {
    return this.form.date_from && this.form.date_to && this.form.legal;
  }

  getPdf($mes) {
    const url = window.location.origin + '/api/storage/app/public/pdf/' + $mes;
    let headers = new HttpHeaders();
    headers = headers.set('Accept', 'application/pdf');
    return this.http.get(url, { headers: headers, responseType: 'blob' });
  }

  ngOnInit() {
  }

}
