import {AfterViewInit, Component, OnInit} from '@angular/core';
import {ColumnMode} from '@swimlane/ngx-datatable';
import {HttpClient} from '@angular/common/http';

declare var $: any;

@Component({
  selector: 'app-reports-dept-payment',
  templateUrl: './reports-dept-payment.component.html',
  styleUrls: ['./reports-dept-payment.component.css']
})
export class ReportsDeptPaymentComponent implements OnInit, AfterViewInit {
  form = {
    date_from: null,
    date_to: null,
    legal: null,
    drivers: null
  };
  companiesList: any = [];
  driversList: any = [];
  ColumnMode = ColumnMode;
  rows = [];
  columns = [
    {prop: 'profile_name', name: 'Водитель'},
    {prop: 'payment', name: 'Оплата'},
  ];

  constructor(private http: HttpClient) {
    this.setDriversList();
    this.loadCompanies();
  }

  setDriversList() {
    this.http.get('/api/users?role=driver&all=true').subscribe((data) => {
      this.driversList.push({
        profile_name: 'Выбрать все',
        id: null
      });
      data['users'].forEach((item) => {
        this.driversList.push(item);
      });
    });
  }

  reportTable() {
    this.rows = [];
    this.http.post('/api/report/payment_or_debt', this.form).subscribe((data: any) => {
      this.rows = Object.values(data['original']);
    });
  }

  initSelect2() {
    this.initSelectClient();
    this.initSelectDriver();
  }

  initSelectDriver() {
    $('#select_driver').select2({
      multiple: 'multiple',
      selectAllText: 'All'
    });
    $('#select_driver').on('change', (data: any) => {
      this.form.drivers = $('#select_driver').val();
    });
    $('#select_driver').on('select2:select', (event) => {
      if (event.params.data.id === 'null') {
        $('#select_driver').select2('destroy').find('option').prop('selected', false).end().select2();
        setTimeout(() => {
          $('#select_driver').select2('destroy').find('option[value!=\'null\']').prop('selected', 'selected').end().select2();
          $('#select_driver').trigger('change');
        });
      }
    });
  }

  initSelectClient() {
    $('#select_client').select2({
      multiple: 'multiple',
      selectAllText: 'All'
    });
    $('#select_client').on('change', (data: any) => {
      this.form.legal = $('#select_client').val();
    });
    $('#select_client').on('select2:select', (event) => {
      if (event.params.data.id === 'null') {
        $('#select_client').select2('destroy').find('option').prop('selected', false).end().select2();
        setTimeout(() => {
          $('#select_client').select2('destroy').find('option[value!=\'null\']').prop('selected', 'selected').end().select2();
          $('#select_client').trigger('change');
        });
      }
    });
  }

  loadCompanies() {
    this.http.get('/api/users?role=legal&all=true').subscribe((data) => {
      this.companiesList.push({
        profile_name: 'Выбрать все',
        id: null
      });
      data['users'].forEach((item) => {
        this.companiesList.push(item);
      });
    });
  }

  ngOnInit() {
  }

  ngAfterViewInit(): void {
    this.initSelect2();
  }
}
