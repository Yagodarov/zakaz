import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {FormsModule} from '@angular/forms';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import { ReportsPaymentComponent } from './reports-payment/reports-payment.component';
import {ReportsComponent} from './reports.component';
import {ReportsCompanyPdfComponent} from './reports-company-pdf/reports-company-pdf.component';
import { ReportsDeptPaymentComponent } from './reports-dept-payment/reports-dept-payment.component';

const routes: Routes = [
  {
    path: '',
    component: ReportsComponent,
  }
];
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    NgxDatatableModule
  ],
  declarations: [ReportsComponent, ReportsPaymentComponent, ReportsCompanyPdfComponent, ReportsDeptPaymentComponent]
})
export class ReportsModule { }
