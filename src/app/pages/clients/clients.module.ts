import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClientsListComponent } from './clients-list/clients-list.component';
import {RouterModule, Routes} from '@angular/router';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';

const ClientRoute: Routes = [
  {
    path : '',
    component : ClientsListComponent,
  }
];

@NgModule({
  declarations: [ClientsListComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(ClientRoute),
    NgxDatatableModule
  ]
})
export class ClientsModule { }
