﻿import { Component, OnInit } from '@angular/core';

import { User } from '../../_models';
import { UserService } from '../../_services';
import { ActivatedRoute } from '@angular/router';

@Component({
    moduleId: module.id.toString(),
    templateUrl: 'home.component.html',
})

export class HomeComponent implements OnInit {
    currentUser: User;
    users: User[] = [];
    rows: number;
    currentPage: number = 0;
    sort: string = "id";
    search = [];
    desc: boolean = true;
    roleNames = (new User()).getRoles();
    roleNamesWithIds = (new User()).getRolesWithIds();
    blocked = [
      {'id' : 0, name : 'Нет'},
      {'id' : 1, name : 'Да'},
    ];
    columns = [
      { name: 'id' , descr: '#', search : { type : 'text' }  },
      { name: 'profile_name' , descr: 'Наименование', search : { type : 'text' }  },
      { name: 'phone' , descr: 'Телефон', search : { type : 'text' }  },
      { name: 'roles', columnName: 'role', descr: 'Роли', format : {func : (data) =>{
        var roles = [];
        data.forEach((item) => {
          var role;
          if (role = this.roleNames.find(x => x.value == item.name).name)
            roles.push(role);
        });
        return roles.join(', ');
      } },
      select : { data : this.roleNamesWithIds },
      },
      { name: 'blocked' , columnName: 'blocked',  descr: 'Блокировка', format : {func : (data) =>{
        return this.blocked.find(x => x.id == data).name;
      } },select : { data : this.blocked }},
      { name: 'email' , descr: 'Эл.почта', search : { type : 'text' } },
    ];
    constructor(private userService: UserService, private route:ActivatedRoute) {
      route.data
        .subscribe(data => {
          console.log(data['data']['count']);
          this.users = data['data']['users'];
          this.rows = data['data']['count'];
        });
      this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    }

    ngOnInit() {

        //this.loadAllUsers();
    }

    deleteUser(id: number) {
        // if (confirm('Удалить пользователя?'))
          this.userService.delete(id).subscribe(() => { this.loadAllUsers() });
    }

    public loadAllUsers() {
        this.userService
          .getAll(this.config())
          .subscribe(
            data => {
              console.log(data);
              this.users = data['users'];
              this.rows = data['count']
            });
    }

    setPage(page){
      this.currentPage = page - 1;
      console.log(this.config());
      this.loadAllUsers();
    }

    setSort(data){
      this.sort = data['key'];
      this.desc = data['desc'];
      this.loadAllUsers();
    }

    setSearch(data){
        this.search = data;
        this.loadAllUsers();
    }

    public config() {
      var params = {
            page:String(this.currentPage),
            orderBy:this.sort,
            desc:this.desc,
        };
        var search = this.search;
        for (let key in search) {
            let value = search[key];
            console.log(key+' '+value);
            params[key] = value;
        }
        return {
            params : params
        };
    }
}
