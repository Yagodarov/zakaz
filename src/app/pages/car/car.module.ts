import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {CarsComponent} from './cars/cars.component';
import {AuthGuard} from '../../_guards';
import {CarsResolver} from '../../_resolvers/car.resolve';
import {CarComponent} from './car-create/car.component';
import {CarEditComponent} from './car-edit/car-edit.component';
import {CarEditResolver} from '../../_resolvers/car.edit';
import {CarService} from '../../_services';
import {CarRepairComponent} from './car-repair/car-repair.component';
import {CarsListResolver} from '../../_resolvers/car.list';
import {PartialsModule} from '../../partials/partials.module';
import {FormsModule} from '@angular/forms';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {JwtInterceptor} from '../../_helpers';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';

const routes: Routes = [
    {
        path: '',
        component: CarsComponent,
        canActivate: [AuthGuard],
        resolve: {
            data: CarsResolver
        }
    }, {
        path: 'new',
        component: CarComponent,
        canActivate: [AuthGuard]
    }, {
        path: 'repair',
        component: CarRepairComponent,
        canActivate: [AuthGuard]
    }, {
        path: ':id/edit',
        component: CarEditComponent,
        canActivate: [AuthGuard],
        resolve: {
            data: CarEditResolver
        }
    }, {
        path: 'delete/:id',
        component: CarComponent,
        canActivate: [AuthGuard]
    },
];

@NgModule({
    declarations: [
        CarsComponent,
        CarEditComponent,
        CarComponent,
        CarRepairComponent,
    ],
    imports: [
        RouterModule.forChild(routes),
        CommonModule,
        PartialsModule,
        FormsModule,
        NgxDatatableModule
    ],
    providers: [
        CarService,
        CarsResolver,
        CarEditResolver,
        CarsListResolver,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: JwtInterceptor,
            multi: true
        },
    ]
})
export class CarModule {
}
