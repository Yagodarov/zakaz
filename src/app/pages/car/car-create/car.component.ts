import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

import {Car} from '../../../_models/car';
import {CarService} from '../../../_services/car.service';

declare var ymaps: any;
declare var $: any;
declare var CustomData: any;
declare var google: any;

/* tslint:disable */
@Component({
  selector: 'car',
  templateUrl: './car.component.html',
  styleUrls: ['./car.component.css']
})
export class CarComponent implements OnInit {

  model = new Car();
  errors: any = [];
  googleInput: any;
  submitted = false;

  constructor(private carService: CarService, private router: Router) {
  }

  onFileChange(event) {
    let reader = new FileReader();
    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      reader.readAsDataURL(file);
      reader.onload = (e: any) => {
        $('#car_img')
            .attr('src', e.target.result)
            .width(250);
        //@ts-ignore
        this.model.file = reader.result.split(',')[1];
      };
    }
  }

  hasError(field) {
    let classList = {
      'has-error': this.errors[field]
    };
    return classList;
  }

  onSubmit() {
    this.carService.create(this.model).subscribe(data => {
          this.router.navigate(['cars'], {queryParams: {successAlert: 1}});
        },
        error => {
          console.log(error['error']);
          this.errors = error['error'];
        });
    console.log(this.model);
  }

  ngOnInit() {
    this.initGoogleSearchInput();
  }

  initGoogleSearchInput() {
    this.googleInput = /** @type {!HTMLInputElement} */(
        $('#address_place').get(0));
    const autocomplete = new google.maps.places.Autocomplete(this.googleInput);

    google.maps.event.addListener(autocomplete, 'place_changed', () => {
      const val = $('#address_place').val();
      console.log(val);
      this.model.address = val;
    });
  }

  initSeelct2() {
    $.fn.select2.amd.require([
      'select2/data/array',
      'select2/utils'
    ], function (ArrayData, Utils) {
      /* tslint:disable */
      function CustomData($element, options): any {
        (<any>CustomData).__super__.constructor.call(this, $element, options);
      }

      /* tslint:enable */
      Utils.Extend(CustomData, ArrayData);

      CustomData.prototype.query = function (params, callback) {
        let result = ymaps.suggest(params.term).then(function (items) {
          const data: any = [];
          const output = {
            results: [],
            more: false
          };
          for (let index = 0; index < items.length; ++index) {
            data.push({
              id: String(items[index]['displayName']),
              text: items[index]['displayName'],
            });
          }
          output.results = data;
          callback(output);
        });
      };

      $('#basic').select2({
        width: '100%',
        closeOnSelect: false,
        dataAdapter: CustomData
      });
      $('#basic').on('select2:select', function (e) {
        console.log(e);
        $('span.select2-search.select2-search--dropdown > input').val($('#basic').val());
      });
    });
  }
}
