import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Car} from '../../../_models';
import {CarService} from "../../../_services";

declare var jQuery: any;
declare var ymaps: any;
declare var $: any;
declare var google: any;

/* tslint:disable */
@Component({
    selector: 'car-edit',
    templateUrl: './car-edit.component.html',
})
export class CarEditComponent implements OnInit {
    model: Car;
    errors: any = [];
    uploaded = false;
    src = '';
    googleInput: any;

    constructor(
        private carService: CarService,
        private router: Router,
        private route: ActivatedRoute,
        private changeDetection: ChangeDetectorRef) {
        this.model = new Car();
        route.data
            .subscribe(data => {
                    this.model = Object.assign(this.model, data.data);
                    console.log(data);
                    console.log(this.model);
                    this.changeDetection.detectChanges();
                },
                error => {
                    console.log(error['error']);
                    this.errors = error['error'];
                });
    }

    onFileChange(event) {
        let reader = new FileReader();
        if (event.target.files && event.target.files.length > 0) {
            let file = event.target.files[0];
            reader.readAsDataURL(file);
            reader.onload = (e: any) => {
                this.uploaded = true;
                //$('#car_img')
                //    .attr('src', e.target.result)
                //    .width(250);
                this.src = e.target.result;
                //@ts-ignore
                this.model.file = reader.result.split(',')[1];
            };
        }
    }

    getImageSource() {
        if (!this.uploaded)
            return this.model.getImagePath() + this.model.id + '/' + this.model.image;
        else
            return this.src;
    }

    hasError(field) {
        let classList = {
            'has-error': this.errors[field]
        };
        return classList;
    }

    onSubmit() {
        this.carService.update(this.model).subscribe(data => {
                this.router.navigate(['cars'], {queryParams: {successAlert: 1}});
            },
            error => {
                console.log(error['error']);
                this.errors = error['error'];
            });
        console.log(this.model);
    }

    initGoogleSearchInput() {
        this.googleInput = /** @type {!HTMLInputElement} */(
            $('#address_place').get(0));
        const autocomplete = new google.maps.places.Autocomplete(this.googleInput);

        google.maps.event.addListener(autocomplete, 'place_changed', () => {
            const val = $('#address_place').val();
            console.log(val);
            this.model.address = val;
        });
    }

    initSelect2(that) {
        $.fn.select2.amd.require([
            'select2/data/array',
            'select2/utils'
        ], function (ArrayData, Utils) {
            function CustomData($element, options) {
                (<any>CustomData).__super__.constructor.call(this, $element, options);
            }

            Utils.Extend(CustomData, ArrayData);

            CustomData.prototype.query = function (params, callback) {
                var result = ymaps.suggest(params.term).then(function (items) {
                    var data = [];
                    var output = {
                        results: [],
                        more: false
                    };
                    for (var index = 0; index < items.length; ++index) {
                        data.push({
                            id: String(items[index]['displayName']),
                            text: items[index]['displayName'],
                        })
                    }
                    output.results = data;
                    callback(output);
                });
            };

            $("#basic").select2({
                width: "100%",
                closeOnSelect: false,
                dataAdapter: CustomData
            });
            $('#basic').on('select2:select', function (e) {
                console.log(e);
                $('span.select2-search.select2-search--dropdown > input').val($("#basic").val());
            });
        });
        var option = new Option(this.model.address, this.model.address, false, false);
        $("#basic").append(option).trigger('change');
    }

    ngOnInit() {
        // this.initSelect2(this);
        this.initGoogleSearchInput();
    }

}
