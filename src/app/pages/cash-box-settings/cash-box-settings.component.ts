import { Component, OnInit } from '@angular/core';
import {TemplateParser} from "../../_models";
import {FormBuilder, FormControl, FormGroup} from "@angular/forms";
import {Alert} from "../../_models/_alert/Alert";
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'app-cash-box-settings',
  templateUrl: './cash-box-settings.component.html',
  styleUrls: ['./cash-box-settings.component.scss']
})
export class CashBoxSettingsComponent implements OnInit {
  templateParser: TemplateParser;
  form: FormGroup;
  alert: Alert = new Alert();
  constructor(
      private http: HttpClient,
      private formBuilder: FormBuilder) {
  }

  getSettings() {
    this.http.get('api/cash_box_settings').subscribe((data: any) => {
      this.initTemplateParser(data);
      this.initFormGroup();
      this.setSettingsValues(data['options']);
      setTimeout(() => {
        this.dragAndDropInit();
      }, 100);
    });
  }

  setResultText(id: any) {
    const textareaElement = <HTMLInputElement>document.getElementById(id + '_textarea');
    const labelElement = <HTMLLabelElement>document.getElementById(id + '_textarea_result');
    if (textareaElement) {
      const parsedText = this.templateParser.parseText(textareaElement.value);
      console.log(parsedText);
      labelElement.innerHTML = parsedText;
    }
  }

  initFormGroup() {
    this.form = this.formBuilder.group({
      'templates': this.createTemplateItems(),
      'cash_box_settings': this.createSettingsFormGroup()
    });
  }

  setSettingsValues(values): void {
    this.form.controls['cash_box_settings'].patchValue(values);
  }

  createSettingsFormGroup() {
    return (new FormGroup({
      'cash_box_login': new FormControl(''),
      'cash_box_password': new FormControl(''),
      'cash_box_company_id': new FormControl(''),
      'cash_box_request_source': new FormControl(''),
    }));
  }

  createTemplateItems() {
    const items = [];
    this.templateParser.getRows().forEach((row: any) => {
      if (items) {
        items.push(new FormGroup({
          'event': new FormControl(row['event']),
          'template': new FormControl(row['template']),
          'id': new FormControl(row['id']),
          'event_id': new FormControl(row['event_id']),
          'send_notify': new FormControl(row['send_notify'])
        }));
      }
    });
    return this.formBuilder.array(items);
  }

  submitForm() {
    console.log(this.form);
    this.http.post('/api/cash_box_settings', this.form.value).subscribe((data) => {
      this.alert.setAlert(data);
    }, (error) => {
      this.alert.setAlert(error);
    });
  }

  initTemplateParser(data) {
    this.templateParser = new TemplateParser(data['cash_box_settings']);
  }

  textAreaChangeEvent(id) {
    this.setResultText(id);
  }

  templateWordButtonClick(template, id) {
    $('#' + id + '_textarea').prop(id + '_textarea');
    const target: any = <HTMLTextAreaElement>document.getElementById(id + '_textarea');
    this.pasteTextAndChangeCaret(target, template);
    const formControl = this.form.controls.templates['controls'].find((item) => {
      return item.value.id === id;
    });
    if (formControl) {
      formControl.controls.template.setValue(target.value);
    }
    this.setResultText(id);
  }

  pasteTextAndChangeCaret(target, template): boolean {
    const finalWord = this.templateParser.getTemplateByWord(template['text']);
    const currentSelectionPosition = target.selectionStart;
    const finalSelectionPosition = currentSelectionPosition + finalWord.length;
    target.setRangeText(finalWord);
    setTimeout(() => {
      target.setSelectionRange(finalSelectionPosition, finalSelectionPosition);
    }, 1);
    return true;
  }

  dragAndDropInit() {
    for (const row of this.templateParser.getRows()) {
      // const item = document.getElementById(row['id']);
      this.setResultText(row['id']);
    }
  }

  ngOnInit(): void {
    this.getSettings();
  }

  ngAfterViewInit(): void {
  }

}
