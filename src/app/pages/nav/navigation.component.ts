import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgClass } from '@angular/common';
import { StorageHelper } from '../../_helpers/storage.helper';
@Component({
  selector: 'navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {

  loginUrl = "login";

  constructor(public router: Router, public storageHelper: StorageHelper) {
      this.router = router;
  }

  ngOnInit() {

  }

  public logout(){
    localStorage.clear();
    console.log(localStorage);
    this.router.navigate([this.loginUrl]);
  }

  public getUserName()
  {
    var storage = JSON.parse(localStorage.getItem('currentUser'));
    if (storage && storage.user)
      return storage.user.name;
    else
      return false;
  }

  public getUserId()
  {
    var storage = JSON.parse(localStorage.getItem('currentUser'));
    if (storage && storage.user)
      return storage.user.id;
    else
      return false;
  }

  navBarClick(){
    console.log(this.storageHelper.getStartUrl());
    this.router.navigate(['/'+this.storageHelper.getStartUrl()]);
  }
}
