import { Component, OnInit } from '@angular/core';
import {Profile} from '../../../_models/profile';
import {UserProfileService} from '../../../_services';
import {ActivatedRoute, Router} from '@angular/router';
import {StorageHelper} from '../../../_helpers/storage.helper';

@Component({
  selector: 'app-driver-profile',
  templateUrl: './driver-profile.component.html',
  styleUrls: ['./driver-profile.component.scss']
})
export class DriverProfileComponent implements OnInit {
  model = new Profile();
  success = false;
  errors: any = [];
  constructor(private service: UserProfileService, private router: Router, private route:ActivatedRoute, public storage:StorageHelper) {

    route.data
        .subscribe(data => {
              console.log(data);
              this.model = Object.assign(this.model, data.data);
              this.model.userModel = Object.assign(this.model.userModel, data.data.user);
            },
            error => {
              console.log(error['error']);
              this.errors = error['error'];
            });
  }

  ngOnInit() {

  }

  onSubmit() {
    this.success = false;
    this.service.update(this.model).subscribe(data => {
          this.success = true;
        },
        error => {
          console.log(error['error']);
          this.errors = error['error'];
        });
    console.log(this.model);
  }

  hasError(field)
  {
    let classList = {
      'has-error' : this.errors[field]
    };
    return classList;
  }

}
