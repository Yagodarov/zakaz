import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DriverEditComponent} from './driver-edit/driver-edit.component';
import {DriversListComponent} from './drivers-list/drivers-list.component';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {RouterModule, Routes} from '@angular/router';
import {DriverNavigationComponent} from './driver-navigation/driver-navigation.component';
import {AuthGuard} from '../../_guards';
import {DriverNavigationResolver} from '../../_resolvers/driver.navigation';
import {DriverEditResolver} from '../../_resolvers/driver.resolve';
import {FormsModule} from '@angular/forms';
import {DriverProfileComponent} from './driver-profile/driver-profile.component';
import {DriverCarsComponent} from './driver-cars/driver-cars.component';
import {DriverScheduleComponent} from './driver-schedule/driver-schedule.component';
import {UserProfileResolver} from '../../_resolvers/user.profile';
import {UserCarsResolver} from '../../_resolvers/user.cars';
import {UserScheduleResolver} from '../../_resolvers/user.schedule';
import {DriverProfileResolver} from '../../_resolvers/driver.profile';
import {DriverCarsResolver} from '../../_resolvers/driver.cars';
import {DriverScheduleResolver} from '../../_resolvers/driver.schedule';
import { DriverBalanceComponent } from './driver-balance/driver-balance.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {MatButtonModule, MatDialogModule, MatInputModule, MatListModule, MatTableModule} from '@angular/material';
import {CarsListResolver} from '../../_resolvers/car.list';
import {JwtInterceptor} from '../../_helpers';
import { DriverBalanceModalComponent } from './driver-balance-modal/driver-balance-modal.component';


const routes: Routes = [
  {
    path: '',
    component: DriversListComponent,
  },
  {
    path: 'edit/:id',
    component: DriverNavigationComponent,
    canActivate: [AuthGuard],
    runGuardsAndResolvers: 'always',
    resolve: {
      data: DriverNavigationResolver
    },
    children: [
      {path: '', redirectTo: 'edit', pathMatch: 'full'},
      {
        path: 'edit',
        component: DriverEditComponent,
        runGuardsAndResolvers: 'always',
        resolve: {
          data: DriverEditResolver
        },
      },
      {
        path: 'profile',
        component: DriverProfileComponent,
        resolve: {
          data: DriverProfileResolver
        },
      },
      {
        path: 'balance',
        component: DriverBalanceComponent,
      },
      {
        path: 'cars',
        component: DriverCarsComponent,
        resolve: {
          data: DriverCarsResolver
        },
      },
      {
        path: 'schedule',
        component: DriverScheduleComponent,
        resolve: {
          data: DriverScheduleResolver
        },
      },
    ]
  }
];

@NgModule({
  declarations: [DriverEditComponent, DriversListComponent, DriverNavigationComponent, DriverProfileComponent, DriverCarsComponent, DriverScheduleComponent, DriverBalanceComponent, DriverBalanceModalComponent],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),
    NgxDatatableModule,
    HttpClientModule,
    MatInputModule,
    MatButtonModule,
    MatDialogModule,
    MatTableModule,
    MatListModule
  ],
  providers: [
    DriverNavigationResolver,
    DriverEditResolver,
    DriverCarsResolver,
    DriverProfileResolver,
    DriverScheduleResolver,
    UserCarsResolver,
    UserScheduleResolver,
    UserProfileResolver,
    UserProfileResolver,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptor,
      multi: true
    },
  ],
  entryComponents: [
    DriverBalanceModalComponent
  ]
})
export class DriversModule {
}
