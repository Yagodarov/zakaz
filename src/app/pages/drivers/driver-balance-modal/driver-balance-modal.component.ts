import {ChangeDetectorRef, Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {HttpClient} from '@angular/common/http';
import {DriverBalance} from '../../../_models/driver-balance';

@Component({
  selector: 'app-driver-balance-modal',
  templateUrl: './driver-balance-modal.component.html',
  styleUrls: ['./driver-balance-modal.component.scss']
})
export class DriverBalanceModalComponent {
  form: any = new DriverBalance();
  errors : any[] = [];
  constructor(public dialogRef: MatDialogRef<DriverBalanceModalComponent>,
              @Inject(MAT_DIALOG_DATA) public data: DriverBalance,
              private cdr : ChangeDetectorRef,
              private htpp: HttpClient) {
  }

  getTitle() {
    if (this.data.type === 'reduce_by_user') {
      return "Списывание баланса";
    } else if (this.data.type === 'increment_by_user'){
      return "Пополнение баланса";
    } else {
      return "Неизвестно";
    }
  }

  submitDialog() {
    this.form.driver_id = this.data.driver_id;
    this.form.type = this.data.type;
    this.htpp.post('api/driver_balance', this.form).subscribe(() => {
      this.dialogRef.close(true);
    }, (data) => {
      this.errors = data['error']['errors'];
      this.cdr.detectChanges();
    });
  }
  closeDialog() {
    this.dialogRef.close(false);
  }
}
