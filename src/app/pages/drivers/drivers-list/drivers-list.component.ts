import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {ColumnMode} from '@swimlane/ngx-datatable';
import {MatTableDataSource} from '@angular/material';
import {User} from '../../../_models';
import {DriverBalance} from '../../../_models/driver-balance';
import { DriverBalanceModalComponent } from "../driver-balance-modal/driver-balance-modal.component";
import { ActivatedRoute } from "@angular/router";
import { MatDialog } from "@angular/material/dialog";
@Component({
  selector: 'app-drivers-list',
  templateUrl: './drivers-list.component.html',
  styleUrls: ['./drivers-list.component.scss']
})
export class DriversListComponent implements OnInit {
  form = {
    date_from: null,
    date_to: null,
    legal: null,
    stamp_sign: null
  };
  driversList : any = [];
  displayedColumns: string[] = [
      'id',
      'profile_name',
      'phone',
      'driver_balance',
      'actions',
  ];
  ColumnMode = ColumnMode;
  constructor(
      private http: HttpClient,
      private dialog: MatDialog
  ) {
    this.loadDrivers();
  }

  openDialog(type, driver_id) {
    const dialogRef = this.dialog.open(DriverBalanceModalComponent, {
      width: '350px',
      data: {
        type: type,
        driver_id: driver_id
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.loadDrivers();
      }
    });
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.driversList.filter = filterValue.trim().toLowerCase();
  }

  deleteDriver(driver_id: number) {
    this.http.delete('/api/users?id=' + driver_id).subscribe((data) => {
      this.loadDrivers();
    });
  }

  loadDrivers() {
    this.http.get('/api/users?role=driver&all=true').subscribe((data) => {
      this.driversList = new MatTableDataSource(data['users']);
    });
  }

  getTotalBalancePipe(driver_balance: DriverBalance[]) {
    let total = 0;
    driver_balance.forEach((balance:any) => {
      balance = new DriverBalance(balance);
      if (balance.getIsIncrement()) {
        total += Number(balance.value);
      } else {
        total = total - Number(balance.value);
      }
    })
    return total;
  }

  ngOnInit() {
  }

}
