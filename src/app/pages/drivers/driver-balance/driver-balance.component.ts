import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ColumnMode} from '@swimlane/ngx-datatable';
import {ActivatedRoute, ActivatedRouteSnapshot} from '@angular/router';
import {MatDialog} from '@angular/material';
import {DriverBalanceModalComponent} from '../driver-balance-modal/driver-balance-modal.component';
import * as moment from 'moment';
import {DriverBalance} from '../../../_models/driver-balance';

@Component({
  selector: 'app-driver-balance',
  templateUrl: './driver-balance.component.html',
  styleUrls: ['./driver-balance.component.scss']
})
export class DriverBalanceComponent implements OnInit {
  form = {
    date_from: null,
    date_to: null,
    legal: null,
    stamp_sign: null
  };
  balanceTypes = [
    {
      type: 'increment_by_user',
      action: 'inc',
      label: 'Пополнение'
    },
    {
      type: 'reduce_by_user',
      action: 'dec',
      label: 'Списание'
    },
    {
      type: 'increment_by_status',
      action: 'inc',
      label: 'Пополнение'
    },
    {
      type: 'decrement_by_order_status',
      action: 'dec',
      label: 'Списание'
    }
  ];
  totalBalance : number = 0;
  balanceList: DriverBalance[] = [];
  displayedColumns: string[] = [
      'type',
      'order_id',
      'user_id',
      'value',
      'created_at',
      'comment',
  ];
  driver_id: string = null;
  ColumnMode = ColumnMode;
  rows: DriverBalance[] = null;
  constructor(
      private http: HttpClient,
      private activatedRoute: ActivatedRoute,
      private dialog: MatDialog
  ) {
    this.driver_id = this.activatedRoute.snapshot['_urlSegment']['segments'][2].path;
    this.loadPayments();
  }

  openDialog(type) {
    const dialogRef = this.dialog.open(DriverBalanceModalComponent, {
      width: '350px',
      data: {
        type: type,
        driver_id: this.driver_id
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.loadPayments();
      }
    });
  }

  getUserNameByProfile(row) {
    if (row) {
      const profile = row.profile;
      if (profile) {
        return profile.name;
      } else {
        return 'Без имени';
      }
    }
    return 'Без имени';
  }

  typePipe(value) {
    return this.balanceTypes.find((item) => { return item.type === value }).label;
  }

  datePipe(value) {
    return moment(value).local().format('DD.MM.YYYY H:m');
  }

  loadPayments() {
    this.http.get('/api/driver_balance', {
      params: {
        driver_id: this.driver_id
      }
    }).subscribe((data) => {
      this.balanceList = data['data']['models'];
      this.getTotalBalance();
    });
  }

  getTotalBalance() {
    this.totalBalance = 0;
    this.balanceList.forEach((item) => {
      if (this.balanceTypes.find((balance) => {
        return item.type === balance.type;
      }).action == 'inc') {
        this.totalBalance = this.totalBalance + Number(item.value);
      } else {
        this.totalBalance = this.totalBalance - Number(item.value);
      }
    });
  }

  ngOnInit() {
  }

}
