import { Component, OnInit } from '@angular/core';
import {User} from '../../../_models';
import {ActivatedRoute} from '@angular/router';
import {UserService} from '../../../_services';

@Component({
  selector: 'app-driver-navigation',
  templateUrl: './driver-navigation.component.html',
  styleUrls: ['./driver-navigation.component.scss']
})
export class DriverNavigationComponent implements OnInit {
  user : User = new User();
  constructor(private route:ActivatedRoute, private userService: UserService) {
    route.data
        .subscribe(data => {
              this.user = Object.assign(this.user, data.data);
            },
            error => {
              console.log(error['error']);
            });
  }

  ngOnInit() {

  }

}
