import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DriverNavigationComponent } from './driver-navigation.component';

describe('DriverNavigationComponent', () => {
  let component: DriverNavigationComponent;
  let fixture: ComponentFixture<DriverNavigationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DriverNavigationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DriverNavigationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
