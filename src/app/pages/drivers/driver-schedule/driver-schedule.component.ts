import { Component, OnInit } from '@angular/core';
import {Schedule} from '../../../_models/schedule';
import {ActivatedRoute} from '@angular/router';
import {UserScheduleService} from '../../../_services';
import * as moment from 'moment';

declare var $: any;
@Component({
  selector: 'app-driver-schedule',
  templateUrl: './driver-schedule.component.html',
  styleUrls: ['./driver-schedule.component.scss']
})
export class DriverScheduleComponent implements OnInit {
  selectedDate: any;
  id: any;
  model = new Schedule;
  errors: any = [];
  modalHeader: String;
  modalAction: String;
  event: any;

  constructor(
      private route: ActivatedRoute,
      private service: UserScheduleService) {
    this.id = +route['_futureSnapshot']['_urlSegment']['segments'][2]['path'];
  }

  ngOnInit() {
    this.route.data
        .subscribe(data => {
          const containerEl = $('#fullCalendar');

          containerEl.fullCalendar({
            locale: 'ru',
            displayEventTime: false,
            dayClick: (date, jsEvent, view) => {
              this.initModalDayClick(date, jsEvent, view);
            },
            eventClick: (calEvent, jsEvent, view) => {
              this.event = calEvent;
              this.initModalEventClick(calEvent, jsEvent, view);
            }
          });

          const models = data['data']['models'];

          for (const model of models) {
            const title = moment(model['date_start']).format('HH:mm') + ' - ' + moment(model['date_end']).format('HH:mm');
            const event = {
              id: model['id'],
              title: title,
              start: moment(model['date_start']).toDate(),
              model: model
            };
            console.log(event);
            $('#fullCalendar').fullCalendar('renderEvent', event, true);
          }
        });
  }

  initModalDayClick(date, jsEvent, view) {
    this.errors = [];

    this.modalHeader = 'Добавление';
    this.modalAction = 'create';
    this.selectedDate = date.format('DD.MM.YYYY');

    this.model.date_start = '00:00';
    this.model.date_end = '23:59';
    this.model.id = null;

    $('#myModal').modal();
  }

  modalDelete() {
    this.service.delete(this.model.id).subscribe(data => {
      $('#fullCalendar').fullCalendar('removeEvents', this.model.id);
      $('#myModal').modal('toggle');
    });
  }

  modalSubmit() {
    this.errors = [];
    const submitModel = Object.assign({}, this.model);

    submitModel.date_start = moment(this.selectedDate, 'DD.MM.YYYY').set({
      h: this.model['date_start'].split(':')[0],
      m: this.model['date_start'].split(':')[1],
    }).format("DD.MM.YYYY HH:mm");
    submitModel.date_end = moment(this.selectedDate, 'DD.MM.YYYY').set({
      h: this.model['date_end'].split(':')[0],
      m: this.model['date_end'].split(':')[1],
    }).format("DD.MM.YYYY HH:mm");
    submitModel.user_id = this.id;
    console.log('you selected date: ' + this.selectedDate);
    console.log(submitModel);
    if (this.model.id) {
      this.service.update(submitModel).subscribe(data => {
        this.addEvent(submitModel, data);
      }, error => {
        this.errors = error['error'];
      });
    } else {
      this.service.create(submitModel).subscribe(data => {
        this.addEvent(submitModel, data);
      }, error => {
        this.errors = error['error'];
      });
    }
  }

  reloadData() {
    this.service.getAll({
      params : {
        user_id : this.id
      }
    }).subscribe((data) => {
      const models = data['models'];
      $('#fullCalendar').fullCalendar('removeEvents', function (...args) {
        return true;
      });
      for (const model of models) {
        const title = moment.unix(model['date_start']).format('HH:mm') + ' - ' + moment.unix(model['date_end']).format('HH:mm');
        const event = {
          id: model['id'],
          title: title,
          start: moment.unix(model['date_start']).toDate(),
          model: model
        };
        $('#fullCalendar').fullCalendar('renderEvent', event, true);
      }
    });
  }

  removeEvents() {
    if (this.model.id) {
      $('#fullCalendar').fullCalendar( 'removeEvents', [this.model.id ]);
    }
  }

  addEvent(submitModel, data) {
    // this.removeEvents();
    const from = moment.unix(data['date_start']).format('HH:mm');
    const to = moment.unix(data['date_end']).format('HH:mm');
    const title = from + ' - ' + to;
    const event = {
      id: data['id'],
      title: title,
      start: moment.unix(data['date_start']).toDate(),
      model: data
    };
    this.reloadData();
    $('#myModal').modal('toggle');
  }

  initModalEventClick(calEvent, jsEvent, view) {
    this.errors = [];

    this.modalHeader = 'Редактирование';
    this.modalAction = 'edit';
    this.selectedDate = calEvent.start.format('DD.MM.YYYY');

    this.model = Object.assign({}, calEvent.model);
    this.model.id = calEvent.model.id;
    this.model.date_start = moment.unix(calEvent.model.date_start).format('HH:mm');
    this.model.date_end = moment.unix(calEvent.model.date_end).format('HH:mm');

    $('#myModal').modal();
  }
}
