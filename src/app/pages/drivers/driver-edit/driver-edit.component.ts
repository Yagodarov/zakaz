import { Component, OnInit } from '@angular/core';
import {User} from '../../../_models';
import {UserService} from '../../../_services';
import {ActivatedRoute, Router} from '@angular/router';
import {StorageHelper} from '../../../_helpers/storage.helper';

@Component({
  selector: 'app-driver-edit',
  templateUrl: './driver-edit.component.html',
  styleUrls: ['./driver-edit.component.scss']
})
export class DriverEditComponent implements OnInit {
  model = new User();
  errors: any = [];
  success = false;
  constructor(
      private userService: UserService,
      private router: Router,
      private route:ActivatedRoute,
      public storage:StorageHelper) {

    route.data
        .subscribe(data => {
              this.model = Object.assign(this.model, data.data);
              data.data.roles.forEach((item) => {
                this.model.rolesUpdate[item['name']] = true;
              });
            },
            error => {
              console.log(error['error']);
              this.errors = error['error'];
            });
  }

  public updateCheckedOptions(option, event) {
    console.log(option);
    console.log(event);
    this.model.rolesUpdate[option] = event.target.checked;
  }

  onSubmit() {
    this.success = false;
    this.errors = [];
    this.userService.update(this.model).subscribe(data => {
          this.router.navigate(['/users/'+this.model.id+'/edit'], {queryParams: {
              'date': Date.now()
            }});
          this.success = true;
        },
        error => {
          console.log(error['error']);
          this.errors = error['error'];
        });
    console.log(this.model);
  }

  ngOnInit() {

  }

  public getUserRole()
  {
    var storage = JSON.parse(localStorage.getItem('currentUser'));
    if (storage && storage.roles)
      return storage.roles;
    else
      return false;
  }

  hasError(field)
  {
    return {
      'has-error' : this.errors[field]
    };
  }
}
