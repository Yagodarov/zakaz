import {Component, OnInit} from '@angular/core';
import {Order} from '../../../_models/order';
import {Router} from '@angular/router';
import {FormArray, FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {UserService} from '../../../_services';
import {OrderService} from '../../../_services';
import {CarService} from '../../../_services';
import {UserContractService} from '../../../_services';
import {StorageHelper} from '../../../_helpers/storage.helper';
import {Car} from '../../../_models';
import {timer} from 'rxjs/internal/observable/timer';
import {map} from 'rxjs/operators';
import {Subscription} from 'rxjs/Subscription';

declare var ymaps: any;
declare var $: any;
declare var google: any;
// declare var require:any;
/* tslint:disable */

@Component({
    selector: 'app-order-select',
    templateUrl: './order-select.component.html',
    styleUrls: ['./order-select.component.css']
})
export class OrderSelectComponent implements OnInit {
    model: FormGroup;
    profileDefault = {
        'transfer': 0,
        'order_cost': 0,
        'request_price': 0
    };
    profile: any = this.profileDefault;
    subscription: Subscription;
    selectedCar: Car;
    usersList = [];
    carsList = [];
    driversList = [];
    errors: any = [];
    addresses: any = [];
    length = '0';
    serve_length = '0';
    statusList = (new Order()).statusList;

    constructor(private router: Router,
                private formBuilder: FormBuilder,
                private userService: UserService,
                private orderService: OrderService,
                private userProfile: UserContractService,
                private carService: CarService,
                public storage: StorageHelper) {

    }

    ngOnInit() {
        this.model = this.formBuilder.group({
            datetime_order: '',
            number: '',
            legal_id: '',
            driver_id: '',
            from: '',
            comment: '',
            payment: 'noncash',
            price: 0,
            length: 25000,
            status: 0,
            car_id: '',
            driver_comment: '',
            addresses: this.formBuilder.array([this.createItem()])
        });

        this.model.get('from').valueChanges.subscribe(values => {
            setTimeout(() => {
                this.getCars();
                this.getPrice()
            }, 50);
        });

        this.model.get('length').valueChanges.subscribe(values => {
            setTimeout(() => {
                this.getCars();
            }, 50);
        });

        this.model.get('car_id').valueChanges.subscribe(values => {
            if (values) {
                setTimeout(() => {
                    this.getProfile(values);
                    this.getPrice();
                }, 50);
            }
        });

        this.model.get('payment').valueChanges.subscribe(values => {
            if (values) {
                setTimeout(() => {
                    this.getCars();
                }, 50);
            }
        });

        this.model.get('datetime_order').valueChanges.subscribe(values => {
            if (values) {
                setTimeout(() => {
                    this.getPrice();
                    this.getCars();
                }, 50);
            }
        });
        var legal_id = this.storage.getUserId();
        this.model.controls["legal_id"].setValue(legal_id);
        setTimeout(
            () => {
                $("#example-basic").steps({
                    headerTag: "h3",
                    bodyTag: "section",
                    transitionEffect: "fade",
                    onStepChanging: (event, currentIndex, newIndex) => {
                        console.log(currentIndex, newIndex);
                        if (currentIndex > newIndex) {
                            return true;
                        }
                        if (newIndex == 1) {
                            return this.validateSteps(1);
                        }
                        if (newIndex == 2) {
                            return (this.model.get('car_id').value && this.model.get('from').value &&
                                this.model.controls.addresses.value[0].name &&
                                this.model.get('datetime_order').value);
                        }
                        return true;
                    },
                    onInit: (event, currentIndex) => {
                        this.initGoogleSearchInput('#from', (value) => {
                            this.model.controls['from'].setValue(value);
                        });
                        this.initGoogleSearchInput('#address_0', (value) => {
                            this.model.controls['addresses']['controls'][0].setValue({name: value});
                        });
                        if (currentIndex == 0) {
                            $('.actions > ul > li:nth-child(1)').css("display", "none");
                        } else {
                            $('.actions > ul > li:nth-child(1)').css("display", "");
                        }
                        (this.model.get('addresses') as FormArray).valueChanges.subscribe(values => {
                            console.log('address change');
                            setTimeout(() => {
                                this.getCars();
                                this.getPrice()
                            }, 50);
                        });
                        console.log(this.model);
                    },
                    onStepChanged: (event, currentIndex, newIndex) => {
                        if (currentIndex == 1 || currentIndex == 2) {
                            $('.actions > ul > li:nth-child(2)').css("display", "none");
                        } else {
                            $('.actions > ul > li:nth-child(2)').css("display", "");
                        }
                        if (currentIndex == 0) {
                            $('.actions > ul > li:nth-child(1)').css("display", "none");
                        } else {
                            $('.actions > ul > li:nth-child(1)').css("display", "");
                        }
                        // this.initGoogleSearchInput('#from', (value) => {
                        //     this.model.controls['from'].setValue(value);
                        // });

                        // this.initSelect2('#address_', 0, this.model);
                        (this.model.get('addresses') as FormArray).valueChanges.subscribe(values => {
                            console.log('address change');
                            setTimeout(() => {
                                this.getCars();
                                this.getPrice()
                            }, 50);
                        });
                    },
                    onFinished: (event, currentIndex) => {
                        this.onSubmit();
                    },
                    labels: {
                        cancel: "Отмена",
                        current: "current step:",
                        pagination: "Pagination",
                        finish: "Подтвердить",
                        next: "Вперед",
                        previous: "Назад",
                        loading: "Loading ..."
                    }
                });
            }, 100);
    }

    validateSteps(step) {
        switch (step) {
            case 1: {
                if (new Date(this.model.get('datetime_order').value) < new Date()) {
                    this.errors['datetime_order'] = ['Дата должна быть больше текущей'];
                    return false;
                } else
                    this.errors = this.errors.splice('datetime_order', 1);
                return (this.model.get('from').value &&
                    this.model.controls.addresses.value[0].name &&
                    this.model.get('datetime_order').value);
            }

            default:
                // code...
                break;
        }
    }

    setCar(car) {
        if (car) {
            this.selectedCar = car;
            this.model.controls['car_id'].setValue(car['id']);
            $("#example-basic").steps("next");
        }
    }

    getCars() {
        if (this.model.get('from').value != '' && this.model.get('datetime_order').value != '') {
            this.carService.getAllOrdering(this.model.value).subscribe(data => {
                var models = data['models'];
                var cars = [];
                for (let item of models) {
                    cars.push(item);
                }
                cars.sort((a: any, b: any) => {
                    if (a.price < b.price) {
                        return -1;
                    } else if (a.price > b.price) {
                        return 1;
                    } else {
                        return 0;
                    }
                });
                this.carsList = cars;
                console.log(this.carsList);
                //this.model.controls["car_id"].setValue(defaultModel);
            });
        }
    }

    createItem(): FormGroup {
        return this.formBuilder.group({
            name: ''
        });
    }

    addItem(): void {
        this.addresses = this.model.get('addresses') as FormArray;
        this.addresses.push(this.createItem());
        this.initGoogleSearchInput('#address_' + (Number(this.addresses.length) - 1), (value) => {
            this.model.controls['addresses']['controls'][(Number(this.addresses.length) - 1)].setValue({name: value});
        });
    }

    removeItem(id): void {
        if (this.addresses.length > 1)
            this.addresses.removeAt(id);
    }

    onSubmit() {
        this.orderService.createSelect(this.model.value).subscribe(data => {
            this.router.navigate(['/orders'], {queryParams: {successAlert: 1}});
        }, error => {
            this.errors = error['error'];
        });
    }


    getPriceDisabled() {
        if (this.storage.hasRole('admin')) {
            return false;
        } else
            return true;
    }

    getProfile(values) {
        this.carService.getById(values).subscribe(
            data => {
                if (data) {
                    this.profile = data;
                }
            },
            data => {
            });
    }

    getOrderCarDisabled() {
        if (!(this.model.controls['datetime_order'].value
            && this.model.controls['from'].value)) {
            return true
        } else {
            return false;
        }
    }

    getPrice() {
        if (this.model.controls['datetime_order'].value && this.model.controls['from'].value
            && this.model.controls['addresses'].value[0].name && this.model.controls['car_id'].value
        )
            console.log(this.subscription);
            if (this.subscription) this.subscription.unsubscribe();
            this.subscription = timer(3000).pipe(map((res) => {
                console.log('request');
            })).subscribe(() => {
                this.getPriceData();
            });

    }

    getPriceData() {
        this.orderService.getDriverPrice(this.model.value).subscribe(data => {
            this.model.controls["price"].setValue(Math.ceil(data['price']));
            this.length = String(Math.ceil(data['length'])) + ' км';
            this.serve_length = String(Math.ceil(data['serve_length'])) + ' км';
        });
    }

    getProfileAttribute(attribute, prefix) {
        if (this.profile) {
            return this.profile[attribute] + prefix;
        } else {
            return 0 + prefix;
        }
    }

    getUserListParams() {
        return {
            params: {
                all: true,
                roles: 'legal'
            }
        }
    }


    initSelect2(selector, id: any = '', model: any) {
        $.fn.select2.amd.require([
            'select2/data/array',
            'select2/utils'
        ], (ArrayData, Utils) => {

            function CustomData($element, options) {
                (<any>CustomData).__super__.constructor.call(this, $element, options);
            }

            Utils.Extend(CustomData, ArrayData);

            CustomData.prototype.query = function (params, callback) {
                var result = ymaps.suggest(params.term).then(function (items) {
                    var data = [];
                    var output = {
                        results: [],
                        more: false
                    };
                    for (var index = 0; index < items.length; ++index) {
                        data.push({
                            id: String(items[index]['displayName']),
                            text: items[index]['displayName'],
                        })
                    }
                    output.results = data;
                    callback(output);
                });
            };

            $(selector + String(id)).select2({
                width: "100%",
                closeOnSelect: false,
                dataAdapter: CustomData,
                minimumInputLength: 2
            });

            $(selector + String(id)).on('select2:select', (e) => {
                var val = e.target.value;
                var selector = e.target.id;
                var id = selector.replace(/[^\d.]/g, '');
                if (id !== '') {

                    (this.model.get('addresses') as FormArray).controls[id].setValue({name: val});
                    this.getPrice();
                } else
                    this.model.controls['from'].setValue(val);
            });
        });
    }

    initGoogleSearchInput(selector, setValue) {
        setTimeout(() => {
            console.log(selector);
            var input = $(selector).get(0);
            var searchBox = new google.maps.places.SearchBox(input);
            var varName = 'var' + (new Date()).getTime();
            eval("window[varName]=new google.maps.places.Autocomplete(input)");
            google.maps.event.addListener(window[varName], 'place_changed', (event) => {
                console.log(event);
                var data = $(selector).val();
                console.log(data);
                setValue(data);
            });
        }, 1);
    }

    hasError(field) {
        let classList = {
            'has-error': this.errors[field]
        };
        return classList;
    }
}
