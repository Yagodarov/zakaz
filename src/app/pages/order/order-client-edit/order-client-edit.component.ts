import {Component, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {Order} from '../../../_models/order';
import {ActivatedRoute, Router} from '@angular/router';
import {CarService, OrderService, UserContractService, UserService} from '../../../_services';
import {StorageHelper} from '../../../_helpers/storage.helper';

@Component({
  selector: 'app-order-client-edit',
  templateUrl: './order-client-edit.component.html',
  styleUrls: ['./order-client-edit.component.scss']
})
export class OrderClientEditComponent implements OnInit {
  model: FormGroup;
  id: any;
  profileDefault = {
    'transfer': 0,
    'order_cost': 0,
    'request_price': 0
  };
  profile: any = this.profileDefault;
  usersList = [];
  clientsList = [];
  carsList = [];
  driversList = [];
  errors: any = [];
  addresses: any = [];
  length = '';
  statusList = (new Order()).statusList;

  constructor(
      private router: Router,
      private route: ActivatedRoute,
      private formBuilder: FormBuilder,
      private userService: UserService,
      private orderService: OrderService,
      private userProfile: UserContractService,
      private carService: CarService,
      public storage: StorageHelper) {
  }

  ngOnInit() {
    console.log(this.route);

    this.initForm();
    this.initResolvers();
    this.id = this.route.snapshot.params['id'];

    this.model.get('driver_id').valueChanges.subscribe(values => {
      this.carsList = [];
      if (values) {
        this.carService.getAll({params: {all: true, driver_id: values}}).subscribe(data => {
          const models = data['models'];
          for (const item of models) {
            this.carsList.push({
              'id': item['id'],
              'name': item['mark'] + ' ' + item['model'] + ' ' + item['car_number']
            });
          }
        });
      }
      else {
        this.model.get('car_id').setValue('');
        this.carsList = [];
      }
    });
  }

  isOrderRent() {
    return this.model.get('order_type').value == 1;
  }

  initResolvers() {
    if (this.storage.hasRole('admin') || this.storage.hasRole('driver-manager')) {
      this.resolveRoute(this.route);
      this.setValueChangeEvents();
    }
  }

  initForm() {
    this.model = this.formBuilder.group({
      id: '',
      datetime_order: '',
      number: '',
      legal_id: '',
      client_id: '',
      driver_id: '',
      driver_comment: '',
      from: '',
      comment: '',
      payment: 'noncash',
      price: 0,
      driver_payment: 0,
      order_type: 0,
      rent_hours: 0,
      status,
      car_id: '',
      addresses: this.formBuilder.array([])
    });
  }

  setValueChangeEvents() {
    (this.model.get('addresses') as FormArray).valueChanges.subscribe(values => {
      this.getPrice();
    });

    this.model.get('datetime_order').valueChanges.subscribe(values => {
      this.getPrice();
    });

    this.model.get('from').valueChanges.subscribe(values => {
      this.getPrice();
    });

    this.model.get('legal_id').valueChanges.subscribe(values => {
      if (values) {
        this.getPrice();
      } else {
        this.profile = this.profileDefault;
      }
    });
  }

  getPriceDisabled() {
    return this.storage.hasRole('admin');
  }

  resolveCars(data) {
    const cars = data['cars']['models'];
    for (const item of cars) {
      if (item['id']) {
        this.carsList.push({
          id: item['id'],
          name: item['mark'] + ' ' + item['model'] + ' ' + item['car_number']
        });
      }
    }
  }
  resolveClients(data) {
    const clients = data['clients']['models'];
    for (const item of clients) {
      if (item['id']) {
        this.clientsList.push({
          id: item['id'],
          name: item['first_name']
        });
      }
    }
  }

  resolveDrivers(data) {
    const users = data['drivers']['users'];
    for (const item of users) {
      if (item['id']) {
        this.driversList.push({
          id: item['id'],
          name: item['profile_name'].split(' ')[0] + ' ' + item['profile_name'].split(' ')[1]
        });
      }
    }
  }

  resolveRoute(route): void {
    route.data
        .subscribe((data) => {
          const httpModel = data['data'];
          Object.keys(httpModel).forEach(k => {
            const control = this.model.get(k);
            if (control instanceof FormControl) {
              control.setValue(httpModel[k], {onlySelf: true});
            }
          });
          this.addresses = this.model.get('addresses') as FormArray;
          let i = 0;
          httpModel['addresses'].forEach((address) => {
            if (address['where'] !== undefined) {
              this.addresses.push(this.formBuilder.group({
                name: address['where']
              }));
              i++;
            }
          });
          const t = httpModel['datetime_order'].split(/[- :]/);
          const date = new Date(Date.UTC(t[0], t[1] - 1, t[2], t[3], t[4], t[5]));
          this.model.get('datetime_order').setValue(date.toISOString().substr(0, 16));
          (this.model.get('addresses') as FormArray).valueChanges.subscribe(values => {
            this.getPrice();
          });

          this.model.get('from').valueChanges.subscribe(values => {
            this.getPrice();
          });
          this.resolveClients(data);
          this.resolveDrivers(data);
          this.resolveLegals(data['legalsList']);
          this.resolveCars(data);

          this.setLength(httpModel['length']);
        });
  }


  resolveLegals(legals) {
    this.usersList = legals['users'];
    this.model.get('legal_id').valueChanges.subscribe(values => {
      if (values > 0) {
        this.getProfile(values);
      } else {
        this.profile = this.profileDefault;
      }
    });
  }

  getProfile(values) {
    this.userProfile.getAll({
      params: {
        user_id: values,
        date_now: true
      }
    }).subscribe(
        data => {
          if (data['models'][0]) {
            this.profile = data['models'][0];
          }
        },
        data => {
        });
  }

    canAction() {
    if (this.storage.hasRole('admin') || this.storage.hasRole('driver-manager')) {
      return true;
    } else {
      if (this.model.controls['status'].value == '5' || this.model.controls['status'].value == '4')
        return false;
      else
        return true;
    }
  }

  createItem(name = ''): FormGroup {
    return this.formBuilder.group({
      name: name
    });
  }

  addItem(name = ''): void {
    this.addresses = this.model.get('addresses') as FormArray;
    this.addresses.push(this.createItem(name));
  }

  removeItem(id): void {
    if (this.addresses.length > 1)
      this.addresses.removeAt(id);
  }

  cancelOrder(): void {
    if (confirm('Отменить заказ?')) {
      this.orderService.cancelOrder(this.model.controls['id'].value).subscribe(data => {
        this.router.navigate(['/orders']);
      }, error => {
        console.log(error);
      });
    }
  }

  onSubmit() {
    this.orderService.update(this.model.value).subscribe(data => {
      this.router.navigate(['/orders']);
    }, error => {
      this.errors = error['error'];
    });
  }

  getProfileAttribute(attribute, prefix) {
    if (this.profile) {
      return this.profile[attribute] + prefix;
    } else {
      return 0 + prefix;
    }
  }

  getPrice() {
    setTimeout(() => {
      this.orderService.getPrice(this.model.value).subscribe(data => {
        this.model.controls['price'].setValue(Math.ceil(data['price']));
        this.setLength(data['length']);
      });
    }, 100);

  }

  setLength(data) {
    this.length = data + ' км';
  }

  getUserListParams() {
    return {
      params: {
        all: true,
        roles: 'legal'
      }
    };
  }

  hasError(field) {
    return {
      'has-error': this.errors[field]
    };
  }
}
