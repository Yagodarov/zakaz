import {Component, OnInit} from '@angular/core';
import {StorageHelper} from '../../../_helpers/storage.helper';
import 'rxjs/add/observable/forkJoin';

declare var ymaps: any;
declare var $: any;
declare var require: any;

@Component({
    selector: 'app-order-select-edit',
    template: `
        <app-order-select-edit-admin
                *ngIf="storage.hasRole('admin') || storage.hasRole('driver-manager')"></app-order-select-edit-admin>
        <app-order-select-edit-legal
                *ngIf="storage.hasRole('legal') && !(storage.hasRole('admin') || storage.hasRole('driver-manager'))"></app-order-select-edit-legal>`,
    styleUrls: ['./css/order-select-edit.component.css']
})
export class OrderSelectEditComponent implements OnInit {
    constructor(

        public storage: StorageHelper) {
    }

    ngOnInit() {

    }
}
