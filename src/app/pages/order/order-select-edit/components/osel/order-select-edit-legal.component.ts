import {Component, OnInit} from '@angular/core';
import {Order} from '../../../../../_models/order';
import {Router, ActivatedRoute} from '@angular/router';
import {FormArray, FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {UserService} from '../../../../../_services';
import {OrderService} from '../../../../../_services';
import {CarService} from '../../../../../_services';
import {UserContractService} from '../../../../../_services';
import {StorageHelper} from '../../../../../_helpers/storage.helper';
import 'rxjs/add/observable/forkJoin';
import {timer} from 'rxjs/internal/observable/timer';
import {map} from 'rxjs/operators';
import {Subscription} from 'rxjs/Subscription';

declare var ymaps: any;
declare var $: any;
declare var require: any;
declare var google: any;

@Component({
    selector: 'app-order-select-edit-legal',
    templateUrl: './order-select-edit-legal.component.html',
    styleUrls: ['./order-select-edit-legal.component.css']
})
export class OrderSelectEditLegalComponent implements OnInit {

    model: FormGroup;
    id: any;
    subscription: Subscription;
    profileDefault = {
        'transfer': 0,
        'order_cost': 0,
        'request_price': 0
    };
    profile: any = this.profileDefault;
    usersList = [];
    carsList = [];
    driversList = [];
    errors: any = [];
    addresses: any = [];
    length = '';
    serve_length = '';
    statusList = (new Order()).statusList;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private formBuilder: FormBuilder,
        private userService: UserService,
        private orderService: OrderService,
        private userProfile: UserContractService,
        private carService: CarService,
        public storage: StorageHelper) {
        console.log(this.profile);
        this.model = this.formBuilder.group({
            id: '',
            datetime_order: '',
            number: '',
            legal_id: '',
            driver_id: '',
            from: '',
            length: 500000,
            comment: '',
            payment: 'noncash',
            price: 0,
            order_type: 0,
            status,
            car_id: '',
            driver_comment: '',
            addresses: this.formBuilder.array([])
        });
        this.resolveRoute(route);
    }

    ngOnInit() {

    }

    getDrivers(date) {
        this.carService.getAll({params: {all: true, datetime_order: date}}).subscribe(data => {
            this.carsList = [];
            const models = data['models'];
            let defaultModel = null;
            this.carsList = [];
            for (const item of models) {
                if (item['default'] === 1 && item['blocked'] === 0) {
                    defaultModel = item['id'];
                }
                this.carsList.push({
                    'id': item['id'],
                    'name': item['mark'] + ' ' + item['model'] + ' ' + item['car_number']
                });
            }
            this.model.setControl('car_id', this.model.controls['car_id'].value);
        });
    }

    getOrderCarDisabled() {
        return(!(this.model.controls['datetime_order'].value
            && this.model.controls['from'].value
            && this.model.controls['addresses'].value[0].name));
    }


    getPriceDisabled() {
        if (this.storage.hasRole('admin')) {
            return false;
        } else
            return true;
    }

    resolveRoute(route): void {
        route.data
            .subscribe(data => {
                // resolve cars
                const cars = data['data']['cars'];
                for (const item of cars) {
                    this.carsList.push({
                        'id': item['id'],
                        'name': item['mark'] + ' ' + item['model'] + ' ' + item['car_number']
                    });
                }

                // resolve profile=
                this.profile = data['data']['profile'];

                // resolve model
                const httpModel = data['data']['model'];
                console.log(httpModel);
                Object.keys(httpModel).forEach(k => {
                    const control = this.model.get(k);
                    if (control instanceof FormControl) {
                        console.log('k=', k);
                        control.setValue(httpModel[k], {onlySelf: true});
                    }
                    if (control instanceof FormArray) {
                        for (const i in httpModel[k]) {
                            this.addresses = this.model.get('addresses') as FormArray;
                            if (i === '0') {
                                this.addresses.push(this.formBuilder.group({
                                    name: httpModel[k][i]['where']
                                }));
                                this.initGoogleSearchInput('#address_' + i, (value) => {
                                    this.model.controls['addresses']['controls'][i].setValue({name: value});
                                });
                            } else {
                                this.addresses.push(this.formBuilder.group({
                                    name: httpModel[k][i]['where']
                                }));
                                this.initGoogleSearchInput('#address_' + i, (value) => {
                                    this.model.controls['addresses']['controls'][i].setValue({name: value});
                                });
                            }
                        }
                    }
                });

                // this.getPrice();

                this.model.controls['car_id'].setValue(httpModel['car_id']);

                const t = httpModel['datetime_order'].split(/[- :]/);
                const date = new Date(Date.UTC(t[0], t[1] - 1, t[2], t[3], t[4], t[5]));

                this.model.get('datetime_order').setValue(date.toISOString().substr(0, 16));
                this.initGoogleSearchInput('#from', (value) => {
                    this.model.controls['from'].setValue(value);
                });
                (this.model.get('addresses') as FormArray).valueChanges.subscribe(values => {
                    setTimeout(() => {
                        this.getPrice();
                    }, 50);
                });

                this.model.get('from').valueChanges.subscribe(values => {
                    setTimeout(() => {
                        this.getPrice();
                    }, 50);
                });

                this.model.get('car_id').valueChanges.subscribe(values => {
                    if (values) {

                        this.getProfile(values);
                        setTimeout(() => {
                            this.getPrice();
                        }, 50);
                    }
                });

                this.model.get('datetime_order').valueChanges.subscribe(values => {
                    if (values) {
                        setTimeout(() => {
                            this.getPrice();
                            this.getDrivers(values);
                        }, 50);
                    }
                });
                this.id = this.model.controls['id'].value;
                this.setLength('length', httpModel['length']);
                this.setLength('serve_length', httpModel['serve_length']);
            });
    }

    setLength(property, value) {
        this[property] = value + ' км';
    }

    canAction() {
        if (this.storage.hasRole('admin') || this.storage.hasRole('driver-manager')) {
            return true;
        } else {
            return (this.model.controls['status'].value === '5' || this.model.controls['status'].value === '4');
        }
    }

    createItem(name = ''): FormGroup {
        return this.formBuilder.group({
            name: name
        });
    }

    addItem(name = ''): void {
        this.addresses = this.model.get('addresses') as FormArray;
        this.addresses.push(this.createItem(name));
        this.initGoogleSearchInput('#address_' + (Number(this.addresses.length) - 1), (value) => {
            this.model.controls['addresses']['controls'][(Number(this.addresses.length) - 1)].setValue({name: value});
        });
    }

    removeItem(id): void {
        if (this.addresses.length > 1) {
            this.addresses.removeAt(id);
        }
    }

    cancelOrder(): void {
        if (confirm('Отменить заказ?')) {
            this.orderService.cancelOrder(this.model.controls['id'].value).subscribe(data => {
                this.router.navigate(['/orders']);
            }, error => {
                console.log(error);
            });
        }
    }

    onSubmit() {
        this.orderService.updateSelect(this.model.value).subscribe(data => {
            this.router.navigate(['/orders']);
        }, error => {
            this.errors = error['error'];
        });
    }

    initSelect2(selector, id: any = '', model: any) {
        $.fn.select2.amd.require([
            'select2/data/array',
            'select2/utils'
        ], (ArrayData, Utils) => {

            function CustomData($element, options) {
                (<any>CustomData).__super__.constructor.call(this, $element, options);
            }

            Utils.Extend(CustomData, ArrayData);

            CustomData.prototype.query = function (params, callback) {
                const result = ymaps.suggest(params.term).then(function (items) {
                    const data = [];
                    const output = {
                        results: [],
                        more: false
                    };
                    for (let index = 0; index < items.length; ++index) {
                        data.push({
                            id: String(items[index]['displayName']),
                            text: items[index]['displayName'],
                        });
                    }
                    output.results = data;
                    callback(output);
                });
            };

            $(selector + String(id)).select2({
                width: '100%',
                closeOnSelect: false,
                dataAdapter: CustomData,
                minimumInputLength: 2
            });

            $(selector + String(id)).on('select2:select', (e) => {
                const val1 = e.target.value;
                const selector1 = e.target.id;
                const id1 = selector1.replace(/[^\d.]/g, '');
                if (id1 !== '') {
                    this.model.controls.addresses.value[id1].name = val1;
                    this.getPrice();
                } else {
                    this.model.controls['from'].setValue(val1);
                }
            });

            if (id !== '') {
                const val = this.addresses.value[id]['name'];
                const newOption = new Option(val, val, false, false);
                $(selector + id).append(newOption).trigger('change');
            } else {
                const val = this.model.controls['from'].value;
                const newOption = new Option(val, val, false, false);
                $(selector).append(newOption).trigger('change');
            }
        });
    }

    getProfileAttribute(attribute, prefix) {
        if (this.profile) {
            return this.profile[attribute] + prefix;
        } else {
            return 0 + prefix;
        }
    }

    getProfile(values) {
        this.carService.getById(values).subscribe(
            data => {
                if (data) {
                    this.profile = data;
                }
            },
            data => {
            });
    }

    getPrice() {
        if (this.model.controls['datetime_order'].value &&
            this.model.controls['from'].value != ''
            && this.model.controls['addresses'].value[0].name  != ''
        ) {
            console.log(this.subscription);
            if (this.subscription) this.subscription.unsubscribe();
            this.subscription = timer(3000).pipe(map((res) => {
                console.log('request');
            })).subscribe(() => {
                this.getPriceData();
            });
        }

    }

    getPriceData() {
        setTimeout(() => {
            this.orderService.getDriverPrice(this.model.value).subscribe(data => {
                this.model.controls['price'].setValue(Math.ceil(data['price']));
                this.length = data['length'] + ' км';
                this.serve_length = data['serve_length'] + ' км';
            });
        }, 100);

    }

    initGoogleSearchInput(selector, setValue) {
        setTimeout(() => {
            console.log(selector);
            const input = $(selector).get(0);
            const searchBox = new google.maps.places.SearchBox(input);
            const varName = 'var' + (new Date()).getTime();
            eval('window[varName]=new google.maps.places.Autocomplete(input)');
            google.maps.event.addListener(window[varName], 'place_changed', (event) => {
                console.log(event);
                const data = $(selector).val();
                console.log(data);
                setValue(data);
            });
        }, 1);
    }

    getUserListParams() {
        return {
            params: {
                all: true,
                roles: 'legal'
            }
        };
    }

    hasError(field) {
        return {
            'has-error': this.errors[field]
        };
    }
}
