import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {FormArray, FormBuilder, FormGroup} from '@angular/forms';
import {UserService} from '../../../_services';
import {OrderService} from '../../../_services';
import {CarService} from '../../../_services';
import {UserContractService} from '../../../_services';
import {StorageHelper} from '../../../_helpers/storage.helper';
import {ClientsService} from '../../../_services';
import {map} from 'rxjs/operators';
import {timer} from 'rxjs/internal/observable/timer';
import {Subscription} from 'rxjs/Subscription';
declare var $: any;

/* tslint:disable */
@Component({
    selector: 'app-order-create',
    templateUrl: './order-create.component.html',
    styleUrls: ['./order-create.component.css']
})
export class OrderCreateComponent implements OnInit {
    model: FormGroup;
    profileDefault = {
        'transfer': 0,
        'order_cost': 0,
        'request_price': 0
    };
    subscription: Subscription;
    profile: any = this.profileDefault;
    usersList = [];
    clientsList = [];
    carsList = [];
    driversList = [];
    errors: any = [];
    addresses: any = [];
    length = '0';
    statusList = [
        "Новый",
        "В обработке",
        "Принят",
        "На исполнении",
        "Выполнен",
        "Отменен",
        "Отменен с оплатой",
        "Ожидает подтверждения водителем"
    ];

    constructor(private router: Router,
                private formBuilder: FormBuilder,
                private userService: UserService,
                private orderService: OrderService,
                private userProfile: UserContractService,
                private carService: CarService,
                private clientService: ClientsService,
                public storage: StorageHelper) {
    }

    ngOnInit() {
        this.model = this.formBuilder.group({
            datetime_order: '',
            number: '',
            legal_id: '',
            driver_id: '',
            client_id: '',
            from: '',
            comment: '',
            driver_payment: 0,
            payment: 'noncash',
            price: 0,
            status: 0,
            order_type: 0,
            car_id: '',
            addresses: this.formBuilder.array([this.createItem()])
        });
        (this.model.get('addresses') as FormArray).valueChanges.subscribe(values => {
            this.getPrice();
        });

        this.model.get('from').valueChanges.subscribe(values => {
            this.getPrice();
        });
        this.model.get('datetime_order').valueChanges.subscribe(values => {
            this.getPrice();
        });
        this.model.get('driver_id').valueChanges.subscribe(values => {
            this.carsList = [];
            if (values)
                this.carService.getAll({params: {all: true, driver_id: values}}).subscribe(data => {
                    var models = data['models'];
                    var defaultModel = null;
                    for (let item of models) {
                        if (item['default'] == 1 && item['blocked'] == 0)
                            defaultModel = item['id'];
                        this.carsList.push({
                            'id': item['id'],
                            'name': item['mark'] + ' ' + item['model'] + ' ' + item['car_number']
                        });
                    }
                    this.model.controls["car_id"].setValue(defaultModel);
                });
            else
                this.carsList = [];
        });

        if (this.storage.hasRole('admin')) {

            // legals get
            this.userService.getLegalsList(this.getUserListParams()).subscribe(data => {
                this.usersList = data['models'];
                this.model.get('legal_id').valueChanges.subscribe(values => {
                    if (values > 0) {
                        this.getProfile(values);
                    } else {
                        this.profile = this.profileDefault;
                    }
                });
            });

            // drivers get
            this.userService.getAll({params: {all: true, blocked: '0', role: 'driver'}}).subscribe(data => {
                var users = data['users'];
                for (let item of users) {
                    if (item['id'])
                        this.driversList.push({
                            id: item['id'],
                            name: item['profile_name'] ? (item['profile_name'].split(" ")[0] + " " + item['profile_name'].split(" ")[1]) : ''
                        });
                }
            });

            // clients get
            this.clientService.getAll({params: {all: true} } ).subscribe(data => {
                var clients = data['models'];
                for (let item of clients) {
                    if (item['id']) {
                        this.clientsList.push({
                            id: item['id'],
                            name: item['first_name']
                        });
                    }
                }
            });
        } else {
            var legal_id = this.storage.getUserId();
            this.getProfile(legal_id);
            this.model.controls["legal_id"].setValue(legal_id);
        }
    }


    createItem(): FormGroup {
        return this.formBuilder.group({
            name: ''
        });
    }

    addItem(): void {
        this.addresses = this.model.get('addresses') as FormArray;
        this.addresses.push(this.createItem());
    }

    removeItem(id): void {
        if (this.addresses.length > 1)
            this.addresses.removeAt(id);
    }

    onSubmit() {
        console.log(this.model);
        this.orderService.create(this.model.value).subscribe(data => {

            this.router.navigate(['/orders']);
        }, error => {
            this.errors = error['error'];
        });
    }

    getPriceDisabled() {
        return this.storage.hasRole('admin');
    }

    getProfile(values) {
        this.userProfile.getAll({
            params: {
                user_id: values,
                date_now: true
            }
        }).subscribe(
            data => {
                if (data['models'][0]) {
                    this.profile = data['models'][0];
                    this.getPrice();
                }
            },
            data => {
            });
    }

    getPrice() {
        if (this.model.controls['datetime_order'].value &&
            this.model.controls['from'].value != ''
            && this.model.controls['addresses'].value[0].name  != ''
        ) {
            console.log(this.subscription);
            if (this.subscription) this.subscription.unsubscribe();
            this.subscription = timer(3000).pipe(map((res) => {
                console.log('request');
            })).subscribe(() => {
                this.getPriceData();
            });
        }
    }

    getPriceData(): void {
        this.orderService.getPrice(this.model.value).subscribe(data => {
            this.model.controls["price"].setValue(data['price']);
            this.length = data['length'] + ' км';
        });
    }

    getUserListParams() {
        return {
            params: {
                all: true,
                roles: 'legal'
            }
        }
    }

    hasError(field) {
        let classList = {
            'has-error': this.errors[field]
        };
        return classList;
    }
}
