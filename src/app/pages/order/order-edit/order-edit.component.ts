import {Component, OnInit} from '@angular/core';
import {Order} from '../../../_models/order';
import {Router, ActivatedRoute} from '@angular/router';
import {FormArray, FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {UserService} from '../../../_services';
import {OrderService} from '../../../_services';
import {CarService} from '../../../_services';
import {UserContractService} from '../../../_services';
import {StorageHelper} from '../../../_helpers/storage.helper';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/forkJoin';
import {toNumber} from 'ngx-bootstrap/timepicker/timepicker.utils';
import {ClientsService} from '../../../_services';
import {timer} from 'rxjs/internal/observable/timer';
import {map} from 'rxjs/operators';
import {Subscription} from 'rxjs/Subscription';

declare var ymaps: any;
declare var $: any;
declare var google: any;

@Component({
    selector: 'app-order-edit',
    templateUrl: './order-edit.component.html',
    styleUrls: ['./order-edit.component.css']
})
export class OrderEditComponent implements OnInit {
    model: FormGroup;
    id: any;
    profileDefault = {
        'transfer': 0,
        'order_cost': 0,
        'request_price': 0
    };
    subscription: Subscription;
    profile: any = this.profileDefault;
    usersList = [];
    clientsList = [];
    carsList = [];
    driversList = [];
    errors: any = [];
    addresses: any = [];
    length = '';
    scenario = '';
    statusList = (new Order()).statusList;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private formBuilder: FormBuilder,
        private userService: UserService,
        private orderService: OrderService,
        private userProfile: UserContractService,
        private carService: CarService,
        private clientsService: ClientsService,
        public storage: StorageHelper) {
    }

    ngOnInit() {
        this.initForm();
        this.initResolvers();
        this.id = this.route.snapshot.params['id'];

        this.model.get('driver_id').valueChanges.subscribe(values => {
            this.carsList = [];
            if (values)
                this.carService.getAll({params: {all: true, driver_id: values}}).subscribe(data => {
                    var models = data['models'];
                    for (let item of models) {
                        this.carsList.push({
                            'id': item['id'],
                            'name': item['mark'] + ' ' + item['model'] + ' ' + item['car_number']
                        });
                    }
                });
            else {
                this.model.get('car_id').setValue('');
                this.carsList = [];
            }
        });
    }

    initResolvers() {
        if (this.storage.hasRole('admin') || this.storage.hasRole('driver-manager')) {
            const source = Observable.forkJoin(
                this.userService.getLegalsList(this.getUserListParams()),
                this.userService.getAll({params: {all: true, blocked: '0', role: 'driver'}}),
                this.clientsService.getAll({params: {all:true}})
                // this.route.data
            ).subscribe(([legals, drivers, clients]) => {
                    this.resolveLegals(legals);
                    this.resolveDrivers(drivers);
                    this.resolveClients(clients);
                    this.resolveRoute(this.route);
                    this.setValueChangeEvents();
                }
            );
        } else if (this.storage.hasRole('legal')) {
            this.resolveRoute(this.route);
            this.setValueChangeEvents();
        } else {
            const legal_id = toNumber(this.storage.getUserId());
            this.getProfile(legal_id);
            this.model.controls['legal_id'].setValue(legal_id);
        }
    }

    resolveClients(clients) {
        clients['models'].forEach((item) => {
            this.clientsList.push({
                id: item['id'],
                name: item['first_name']
            });
        });
    }

    initForm() {
        this.model = this.formBuilder.group({
            id: '',
            datetime_order: '',
            number: '',
            legal_id: '',
            client_id: '',
            driver_id: '',
            driver_comment: '',
            from: '',
            comment: '',
            payment: 'noncash',
            price: 0,
            driver_payment: 0,
            order_type: 0,
            status,
            car_id: '',
            addresses: this.formBuilder.array([])
        });
    }

    setValueChangeEvents() {
        (this.model.get('addresses') as FormArray).valueChanges.subscribe(values => {
            this.getPrice();
        });

        this.model.get('datetime_order').valueChanges.subscribe(values => {
            this.getPrice();
        });

        this.model.get('from').valueChanges.subscribe(values => {
            this.getPrice();
        });

        this.model.get('legal_id').valueChanges.subscribe(values => {
            if (values) {
                this.getPrice();
            } else {
                this.profile = this.profileDefault;
            }
        });
    }

    getPriceDisabled() {
        return this.storage.hasRole('admin');
    }

    resolveLegals(legals) {
        this.usersList = legals['models'];
        this.model.get('legal_id').valueChanges.subscribe(values => {
            if (values > 0) {
                this.getProfile(values);
            } else {
                this.profile = this.profileDefault;
            }
        });
    }

    resolveDrivers(drivers) {
        const users = drivers['users'];
        for (const item of users) {
            if (item['id']) {
                this.driversList.push({
                    id: item['id'],
                    name: item['profile_name'].split(' ')[0] + ' ' + item['profile_name'].split(' ')[1]
                });
            }
        }
    }

    resolveRoute(route): void {
        route.data
            .subscribe((data) => {
                const httpModel = data['data'];
                this.scenario = httpModel['scenario'];
                Object.keys(httpModel).forEach(k => {
                    const control = this.model.get(k);
                    if (control instanceof FormControl) {
                        control.setValue(httpModel[k], {onlySelf: true});
                    }
                });
                this.addresses = this.model.get('addresses') as FormArray;
                let i = 0;
                httpModel['addresses'].forEach((address) => {
                    if (address['where'] !== undefined) {
                        this.addresses.push(this.formBuilder.group({
                            name: address['where']
                        }));
                        i++;
                    }
                });
                const t = httpModel['datetime_order'].split(/[- :]/);
                const date = new Date(Date.UTC(t[0], t[1] - 1, t[2], t[3], t[4], t[5]));
                this.model.get('datetime_order').setValue(date.toISOString().substr(0, 16));
                (this.model.get('addresses') as FormArray).valueChanges.subscribe(values => {
                    this.getPrice();
                });

                this.model.get('from').valueChanges.subscribe(values => {
                    this.getPrice();
                });
                this.setLength(httpModel['length']);
            });
    }

    canAction() {
        if (this.storage.hasRole('admin') || this.storage.hasRole('driver-manager')) {
            return true;
        } else {
            if (this.model.controls['status'].value == '5' || this.model.controls['status'].value == '4')
                return false;
            else
                return true;
        }
    }

    createItem(name = ''): FormGroup {
        return this.formBuilder.group({
            name: name
        });
    }

    addItem(name = ''): void {
        this.addresses = this.model.get('addresses') as FormArray;
        this.addresses.push(this.createItem(name));
    }

    removeItem(id): void {
        if (this.addresses.length > 1)
            this.addresses.removeAt(id);
    }

    cancelOrder(): void {
        if (confirm('Отменить заказ?')) {
            this.orderService.cancelOrder(this.model.controls['id'].value).subscribe(data => {
                this.router.navigate(['/orders']);
            }, error => {
                console.log(error);
            });
        }
    }

    onSubmit() {
        this.orderService.update(this.model.value).subscribe(data => {
            this.router.navigate(['/orders']);
        }, error => {
            this.errors = error['error'];
        });
    }

    initSelect2(selector, id: any = '', model: any) {
        $.fn.select2.amd.require([
            'select2/data/array',
            'select2/utils'
        ], (ArrayData, Utils) => {

            function CustomData($element, options) {
                (<any>CustomData).__super__.constructor.call(this, $element, options);
            }

            Utils.Extend(CustomData, ArrayData);

            CustomData.prototype.query = function (params, callback) {
                var result = ymaps.suggest(params.term).then(function (items) {
                    var data = [];
                    var output = {
                        results: [],
                        more: false
                    };
                    for (var index = 0; index < items.length; ++index) {
                        data.push({
                            id: String(items[index]['displayName']),
                            text: items[index]['displayName'],
                        })
                    }
                    output.results = data;
                    callback(output);
                });
            };

            $(selector + String(id)).select2({
                width: '100%',
                closeOnSelect: false,
                dataAdapter: CustomData,
                minimumInputLength: 2
            });

            $(selector + String(id)).on('select2:select', (e) => {
                var val = e.target.value;
                var selector = e.target.id;
                var id = selector.replace(/[^\d.]/g, '');
                if (id !== '') {
                    this.model.controls.addresses.value[id].name = val;
                    this.getPrice();
                } else
                    this.model.controls['from'].setValue(val);
            });

            if (id !== '') {
                var val = this.addresses.value[id]['name'];
                var newOption = new Option(val, val, false, false);
                $(selector + id).append(newOption).trigger('change');
            } else {
                var val = this.model.controls['from'].value;
                var newOption = new Option(val, val, false, false);
                $(selector).append(newOption).trigger('change');
            }
        });
    }

    getProfileAttribute(attribute, prefix) {
        if (this.profile) {
            return this.profile[attribute] + prefix;
        } else {
            return 0 + prefix;
        }
    }

    getProfile(values) {
        this.userProfile.getAll({
            params: {
                user_id: values,
                date_now: true
            }
        }).subscribe(
            data => {
                if (data['models'][0]) {
                    this.profile = data['models'][0];
                }
            },
            data => {
            });

    }

    getPrice() {
        if (this.model.controls['datetime_order'].value &&
            this.model.controls['from'].value != ''
            && this.model.controls['addresses'].value[0].name  != ''
        ) {
            console.log(this.subscription);
            if (this.subscription) this.subscription.unsubscribe();
            this.subscription = timer(3000).pipe(map((res) => {
                console.log('request');
            })).subscribe(() => {
                this.getPriceData();
            });
        }
    }

    getPriceData(): void {
        this.orderService.getPrice(this.model.value).subscribe(data => {
            this.model.controls['price'].setValue(data['price']);
            this.length = data['length'] + ' км';
        });
    }

    setLength(data) {
        this.length = data + ' км';
    }

    getUserListParams() {
        return {
            params: {
                all: true,
                roles: 'legal'
            }
        };
    }

    hasError(field) {
        return {
            'has-error': this.errors[field]
        };
    }
}
