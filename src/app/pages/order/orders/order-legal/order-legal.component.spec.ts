import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderLegalComponent } from './order-legal.component';

describe('OrderLegalComponent', () => {
  let component: OrderLegalComponent;
  let fixture: ComponentFixture<OrderLegalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderLegalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderLegalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
