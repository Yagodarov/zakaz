import { Component, OnInit } from '@angular/core';
import { Order } from '../../../../_models/order';
import {ActivatedRoute} from "@angular/router";
import { OrderService } from '../../../../_services/order.service';
import {OnChanges, SimpleChange } from '@angular/core';
import {Column} from '../../../../partials/grid/column';
import {Sorter} from '../../../../partials/grid/sorter';
import {ModalForm} from '../modal';
import { StorageHelper } from '../../../../_helpers/storage.helper';
import * as _ from 'underscore';
declare var $: any;
@Component({
  selector: 'app-order-driver',
  templateUrl: './order-driver.component.html',
  styleUrls: ['./order-driver.component.css']
})
export class OrderDriverComponent implements OnInit {

    currentModel: Order;
    pager;
    models: Order[] = [];
    modal : ModalForm = new ModalForm;
    errors = [];
    count: number;
    currentPage: number = 1;
    sort: string = "id";
    desc: boolean = true;
    search = [];
    carsList = [];
    driversList = [];
    legalsList = [];
    statusList = [
        {
            id : 0,
            name : "Новый"
        },
        {
            id : 1,
            name : "В обработке"
        },
        {
            id : 2,
            name : "Принят"
        },
        {
            id : 3,
            name : "На исполнении"
        },
        {
            id : 4,
            name : "Выполнен"
        },
        {
            id : 5,
            name : "Отменен"
        },
        {
            id : 6,
            name : "Отменен с оплатой"
        }
    ];
    columns = [
        { name: 'id' , descr: '#', search : { type : 'text' } },
        { name: 'datetime_order' , descr: 'Дата и время', format : {type : 'date', format: 'dd.MM.yyyy, H:mm'}, search : { type : 'date' }},
        { name: 'from' , descr: 'Откуда', search : { type : 'text' }},
        { name: 'to' , descr: 'Куда', search : { type : 'text' }},
        { name: 'price' , descr: 'Стоимость', search : { type : 'text' }, search2 : { type : 'text' } },
        { name: 'car_name' , columnName: 'car_id', descr: 'Автомобиль', select : { data : this.carsList } },
        { name: 'driver_name' , columnName: 'driver_id' , descr: 'Водитель', select : { data : this.driversList }, format : {row: (row) => {
            return (!(row['driver_name'] == '' || row['driver_name'] == ' ' || row['driver_name'] == null) ?
                "Водитель: "+row['driver_name'] : '')
                +(row['driver_phone'] != null ? "<br>Телефон: "+row['driver_phone'] : '');
        }} },
        { name: 'legal_name' , columnName: 'legal_id', descr: 'Заказчик', select : { data : this.legalsList } },
        { name: 'status' , columnName: 'status', descr: 'Статус', select : { data : this.statusList }, format : {func : (data) => {
            var arr = this.statusList;
            var elementPos = arr.map(function(x) {return x.id; }).indexOf(data);
            return arr[elementPos].name;
        }} },
    ];
    constructor(private service: OrderService, private route:ActivatedRoute, private storage: StorageHelper) {

        route.data
            .subscribe(data => {
                console.log(data);
                for(let item of data['carsList']['models'])
                {
                    this.carsList.push({
                        'id' : item['id'],
                        'name' : [item['mark'],item['model'],item['car_number']].join(' ')
                    });
                }
                for(let item of data['legalsList']['users'])
                {
                    this.legalsList.push({
                        'id' : item['id'],
                        'name' : item['profile_name']
                    });
                }
                for(let item of data['driversList']['users'])
                {
                    this.driversList.push({
                        'id' : item['id'],
                        'name' : item['profile_name']
                    });
                }
                this.models = data['data']['models'];
                this.count = data['data']['count'];
            });
    }

    ngOnInit() {
        this.initPager();
    }

    acceptModel(model:any){
        if (confirm("Принять заказ?"))
        {
            let updateModel = Object.assign({}, model)
            updateModel['status'] = 5;
            this.service.status(updateModel).subscribe(data => {
                model['status'] = 2;
            });
        }
    }

    declineModel(){
        if (this.currentModel['status'] <= 3)
        {
            let updateModel = Object.assign({}, this.currentModel)
            updateModel['status'] = 5;
            updateModel['driver_comment'] = this.modal.driver_comment;
            updateModel['callsign'] = this.modal.callsign;
            this.service.decline(updateModel).subscribe(data => {
                this.loadAllModels();
                $('#myModal').modal('toggle');
                this.modal = new ModalForm;
            }, error => {
                this.errors = error['errors'];
            });
        }
    }

    initDeclineModal(model:any)
    {
        this.modal = new ModalForm;
        this.modal.order_id = model.id;
        this.currentModel = model;
        $('#myModal').modal();
    }

    deleteModel(id: number) {
        if (confirm("Удалить заказ?"))
            this.service.delete(id).subscribe(() => { this.loadAllModels() });
    }

    public loadAllModels() {
        this.service
            .getAll(this.config())
            .subscribe(
                data => {
                    this.models = data['models'];
                    if (this.count != data['count'])
                    {
                        this.count = data['count'];
                        this.pager = this.getPager(this.count, 1);
                    }
                });
    }

    concatIfNorNull(arr, separator)
    {
        var result = '';
        for(let item of arr)
        {
            if (!(item == '' || item == ' ' || item == null || item == undefined))
            {
                result += item + ' ';
            }
        }
        return result;
    }

    setPage(page){
        this.pager.currentPage = page;
        this.currentPage = page - 1;
        console.log(page);
        this.loadAllModels();
    }

    setSearch(data){
        this.search = data;
        this.loadAllModels();
    }

    setSort(){
        this.currentPage = this.pager.currentPage - 1;
        this.loadAllModels();
    }

    inputSearch()
    {
        this.currentPage = 0;
        this.pager.currentPage = 1;
        this.loadAllModels();
    }

    public config() {
        var params = {
            page:String(this.pager.currentPage - 1),
            orderBy:this.sort,
            desc:this.desc,
        };
        var search = this.search;
        for (let key in search) {
            let value = search[key];
            console.log(key+' '+value);
            params[key] = value;
        }
        return {
            params : params
        };
    }

    _sort(key){
        if (this.sort == key) this.desc = !this.desc;
        this.sort = key;
        this.setSort();
    }

    getRowClassByRow(row)
    {
        return [
            'bg-orange',
            'bg-blue',
            'bg-green',
            'bg-yellow',
            '',
            'bg-warning',
            '',
        ][row['status']];
    }

    getPager(totalItems: number, currentPage: number = 1, pageSize: number = 10) {
        // calculate total pages
        let totalPages = Math.ceil(totalItems / pageSize);

        let startPage: number, endPage: number;
        if (totalPages <= 10) {
            // less than 10 total pages so show all
            startPage = 1;
            endPage = totalPages;
        } else {
            // more than 10 total pages so calculate start and end pages
            if (currentPage <= 6) {
                startPage = 1;
                endPage = 10;
            } else if (currentPage + 4 >= totalPages) {
                startPage = totalPages - 9;
                endPage = totalPages;
            } else {
                startPage = currentPage - 5;
                endPage = currentPage + 4;
            }
        }

        // calculate start and end item indexes
        let startIndex = (currentPage - 1) * pageSize;
        let endIndex = Math.min(startIndex + pageSize - 1, totalItems - 1);

        // create an array of pages to ng-repeat in the pager control
        let pages = _.range(startPage, endPage + 1);

        // return object with all pager properties required by the view
        return {
            totalItems: totalItems,
            currentPage: currentPage,
            pageSize: pageSize,
            totalPages: totalPages,
            startPage: startPage,
            endPage: endPage,
            startIndex: startIndex,
            endIndex: endIndex,
            pages: pages
        };
    }

    initPager()
    {
        this.pager = this.getPager(this.count, this.currentPage);
    }
}
