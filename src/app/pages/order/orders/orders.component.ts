import {Component, OnInit} from '@angular/core';
import {StorageHelper} from '../../../_helpers/storage.helper';


@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css'],
  providers: [StorageHelper]
})
export class OrdersComponent {
  constructor(public user: StorageHelper) {
  }
}
