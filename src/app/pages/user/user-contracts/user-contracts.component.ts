import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserContractService} from '../../../_services/user.contract.service';
import { StorageHelper } from '../../../_helpers/storage.helper';
declare var $: any;
@Component({
  selector: 'app-user-contracts',
  templateUrl: './user-contracts.component.html',
  styleUrls: ['./user-contracts.component.css']
})
export class UserContractsComponent implements OnInit {
	id: any;
	models: any[] = [];
	count : 0;
	errors : any = [];
	model : any = [];
	_sort: string = "id";
	desc: boolean = true;
	modalConfig: any = {
		header : 'header',
		submitButton : 'submitButton'
	};
	columns = [
		// { name: 'id' , descr: '#' },
		{ name: 'contract_number', descr: 'Номер договора'},
		{ name: 'contract_period_start', descr: 'Дата от'},
		{ name: 'contract_period_end', descr: 'Дата до'},
		{ name: 'order_cost', descr: 'Минимальная стоимость'},
		{ name: 'transfer', descr: 'Трансфер'},
		{ name: 'request_price', descr: 'Стоимость подачи'},
	];
	constructor(private route:ActivatedRoute, private service:UserContractService, public storage: StorageHelper) {
	this.id = +route['_futureSnapshot']['_urlSegment']['segments'][1]['path'];
	route.data
		.subscribe(data => {
			console.log(data);
			this.models = data['data']['models'];
		});
	}

	ngOnInit() {
	}
	open(id : number = null)
	{
		if (!id)
		{
			this.model = {};
			this.errors = [];
			this.modalConfig = {
				header : 'Добавить договор',
				submitButton : 'Добавить'
			}
			$('#myModal').modal();
		}
		else
		{
			this.model = {};
			this.errors = [];
			this.service.getById(id).subscribe(data => {
				this.modalConfig = {
					header : 'Редактировать договор',
					submitButton : 'Сохранить'
				}
				this.model = data;
				$('#myModal').modal();
			});
		}
	}

	delete(id)
	{
		this.service.delete(id).subscribe(data => {
			this.getAllModels();
		});
	}

	getAllModels()
	{
		this.service.getAll({params:{user_id:this.id}}).subscribe(data => {
			this.models = data['models'];
		})
	}

	sort(name)
	{
		if (this._sort == name) this.desc = !this.desc;
      		this._sort = name;
		this.service.getAll(this.config()).subscribe(data => {
			this.models = data['models'];
		})
	}

	public config() {
      return {
        params:{
        	user_id:this.id,
          	orderBy:this._sort,
          	desc:this.desc
        }
      };
    }

	onSubmit()
	{
		if (this.model.id)
		{
			this.service.update(this.model).subscribe(data => {
				$('#myModal').modal('toggle');
				this.getAllModels();
			}, data => {
				this.errors = data['error'];
			});
		}
		else
		{
			this.model.user_id = this.id;
			this.service.create(this.model).subscribe(data => {
				this.getAllModels();
				$('#myModal').modal('toggle');
			}, data => {
				this.errors = data['error'];
			});
		}
	}

	  hasError(field)
	  {
	      let classList = {
	          'has-error' : this.errors[field]
	      };
	      return classList;
	  }
}
