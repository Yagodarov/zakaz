import { Component, OnInit, TemplateRef, ElementRef, ViewChildren, ViewContainerRef  } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Car } from '../../../_models';
import { UserCar } from '../../../_models';
import { UserCarsService } from '../../../_services/user.car.service';
import { CarService } from '../../../_services/car.service';

@Component({
  selector: 'app-user-cars',
  templateUrl: './user-cars.component.html',
  styleUrls: ['./user-cars.component.css']
})
export class UserCarsComponent implements OnInit {
  success = false;
	form : any = {};
  id: any;
  formList: any;
	models: any[] = [];
  cars: any = [];
	count : 0;
	errors : any = [];
	sort: string = "id";
  desc: boolean = true;
  columns = [
    // { name: 'id' , descr: '#' },
    { name: 'name' , descr: 'Марка, модель, номер' },
    { name: 'blocked' , descr: 'Статус' },
    { name: 'default', descr: 'По умолчанию'}
  ];
  statusList = {
    0 : 'Заблокировать',
    1 : 'Разблокировать'
  };
  constructor(private route:ActivatedRoute, private service:UserCarsService, private carService:CarService) {
    this.id = +route['_futureSnapshot']['_urlSegment']['segments'][1]['path'];
  	 route.data
        .subscribe(data => {
          console.log(data);
          this.models = data['data']['models'];
        });
  }

  default(id: number) {
      this.service.default(id).subscribe(() => {
        this.loadModels();
      });
  }

  delete(id: number) {
      this.service.delete(id).subscribe(() => { this.models = this.models.filter(item => item['id'] !== id); });
  }

  block(id:number) {
    this.service.block(id).subscribe(() => {
        this.loadModels();
    });
  }

  loadModels() {
      this.service.getAll(this.getParams()).subscribe(data => {
        console.log(data);
        this.models = data['models'];
        this.form = {};
      });
  }

  loadCars() {
    this.carService.getAll(this.getCarParams()).subscribe(data => {
      var arr = data['models'];
      console.log(arr);
      arr.forEach((item) => {
        console.log(item);
        this.cars[item.id] = item.mark+' '+item.model+' '+item.car_number;
      });
    });
    console.log(this.cars);
  }

  ngOnInit() {
    this.loadCars();
  }

  onSubmit() {
    this.form.user_id = this.id;
    this.success = null;
    this.errors = [];
    this.service.create(this.form).subscribe(data => {
      this.success = true;
      this.loadModels();
    }, error => {
      this.errors = error['error'];
    });
  }

  getParams() {
    return {params:{
      id:this.id,
      orderBy:this.sort,
      desc:this.desc
    }};
  }

  setSort(data){
      this.sort = data['key'];
      this.desc = data['desc'];
      this.loadModels();
  }

  getCarParams() {
      return {
          params:{
              all:true
          }
      };
  }

  hasError(field)
  {
      let classList = {
          'has-error' : this.errors[field]
      };
      return classList;
  }
}
