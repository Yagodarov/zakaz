import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../../../_services/user.service';
import { User } from '../../../_models/user';
@Component({
  selector: 'app-user-navigation',
  templateUrl: './user-navigation.component.html',
  styleUrls: ['./user-navigation.component.css']
})
export class UserNavigationComponent implements OnInit {
  user : User = new User();
  constructor(private route:ActivatedRoute, private userService: UserService) {
    route.data
      .subscribe(data => {
        this.user = Object.assign(this.user, data.data);
      },
      error => {
        console.log(error['error']);
      });
  }

  ngOnInit() {

  }

}
