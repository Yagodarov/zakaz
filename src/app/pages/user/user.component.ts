import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {User} from '../../_models/user';
import {UserService} from '../../_services/user.service';

@Component({
    selector: 'user',
    templateUrl: './user.component.html',
    styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

    model = new User();
    errors: any = [];
    submitted = false;

    constructor(private userService: UserService, private router: Router) {
    }

    onSubmit() {
        console.log(this.model);
        this.userService.create(this.model).subscribe(data => {
                this.router.navigate(['users']);
            },
            error => {
                console.log(error['error']);
                this.errors = error['error'];
            });
        console.log(this.model);
    }

    ngOnInit() {

    }

    public updateCheckedOptions(option, event) {
        console.log(option);
        console.log(event);
        this.model.rolesUpdate[option] = event.target.checked;
    }

    public getUser() {
        return JSON.parse(localStorage.getItem('currentUser'));
    }

    hasError(field) {
        let classList = {
            'has-error': this.errors[field]
        };
        return classList;
    }
}
