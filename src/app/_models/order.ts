export class Order {
    id: number;
    datetime_order?: any;
    number?: any;
    from?: any;
    comment?: any;
    payment?: any;
    driver_payment?: any;
    price?: any;
    driver_id?: any;
    legal_id?: any;
    car_id?: any;
    status?: any;
    addresses?: any[];
    statusList = [
        "Новый",
        "В обработке",
        "Принят",
        "На исполнении",
        "Выполнен",
        "Отменен",
        "Отменен с оплатой",
        "Ожидает подтверждения водителем",
        "Отменен водителем",
    ];
    statusListObjectArray = [
        {
            id: 0,
            name: 'Новый'
        },
        {
            id: 1,
            name: 'В обработке'
        },
        {
            id: 2,
            name: 'Принят'
        },
        {
            id: 3,
            name: 'На исполнении'
        },
        {
            id: 4,
            name: 'Выполнен'
        },
        {
            id: 5,
            name: 'Отменен'
        },
        {
            id: 6,
            name: 'Отменен с оплатой'
        },
        {
            id: 7,
            name: 'Ожидает подтверждения водителем'
        },
        {
            id: 8,
            name: 'Отменен водителем'
        }
    ];
}
