export class Alert {
  private status: string;
  private message: string;
  private visible: boolean;
  getMessage() {
    return this.message;
  }
  getIsVisible() {
    return this.visible;
  }
  hide() {
    this.visible = false;
  }
  getClasses() {
    return {
      'alert': true,
      'alert-warning': this.visible === false,
      'alert-success': this.visible === true,
      'no-border': true
    };
  }
  getStatus() {
    if (status === 'true') {
      return true;
    } else {
      return false;
    }
  }
  setAlert(data) {
    this.status = data['status'];
    this.message = data['message'];
    this.visible = true;
  }
}
