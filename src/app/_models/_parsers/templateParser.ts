export class TemplateParser {
  rows: Array<any>;
  templates: Array<any> = [
    {
      'id': 1,
      'text': 'ЦВЕТ',
      'example': 'ЧЕРНЫЙ'
    }, {
      'id': 2,
      'text': 'ТС',
      'example': 'НИССАН ТИИДА'
    }, {
      'id': 3,
      'text': 'ГОСНОМЕР',
      'example': 'А 007 МР'
    }, {
      'id': 4,
      'text': 'ВОДИТЕЛЬ',
      'example': 'ИВАН'
    }, {
      'id': 5,
      'text': 'ТЕЛВОДИТЕЛЬ',
      'example': '89167543123'
    }, {
      'id': 7,
      'text': 'СУММА',
      'example': '200'
    }, {
      'id': 8,
      'text': 'НОМЕРЗАКАЗА',
      'example': '123'
    }, {
      'id': 9,
      'text': 'ВРЕМЯПОДАЧИ',
      'example': '17:55'
    }, {
      'id': 10,
      'text': 'ДАТАПОДАЧИ',
      'example': '30.03.1994'
    }, {
      'id': 11,
      'text': 'АДРЕСПОДАЧИ',
      'example': 'АЭРОПОРТ ШЕРЕМЕТЬЕВО'
    }, {
      'id': 12,
      'text': 'АДРЕСКУДА',
      'example': 'КУРСКИЙ ВОКЗАЛ'
    }, {
      'id': 13,
      'text': 'ПРИМЕЧАНИЕ',
      'example': 'ПРИМЕЧАНИЕ'
    }
  ];

  constructor(rows: any) {
    console.log(rows);
    this.setRows(rows);
  }

  public getRows() {
    return this.rows;
  }

  public getTemplates() {
    return this.templates;
  }

  getTemplateByWord(word) {
    return '<' + word + '>';
  }

  addSpacesToWord(text) {
    return text ;
  }

  parseText(text) {
    let resultText = text;
    this.getTemplates().forEach((item) => {
      resultText = resultText.split(this.getTemplateByWord(item.text)).join(this.addSpacesToWord(item.example));
    });
    return resultText;
  }

  protected setRows(data) {
    this.rows = data;
  }
}
