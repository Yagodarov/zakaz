export class Contract {
    id: number;
    user_id: number;
    contract_number: number;
    contract_period_start: string;
    contract_period_end: string;
    request_price: number;
    transfer: number;
    order_cost: number;
}