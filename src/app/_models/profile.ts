import { User } from './user';
export class Profile {
    id: number;
    name: any;
	requisites: any;
	phone: any;
	address: any;
	legal_address: any;
	contract_period_start: any;
	contract_period_end: any;
	contract_number: any;
	first_second_patronymic: any;
	weekend: any;
	request_price: any;
	transfer: any;
	order_cost: any;
	role:any;
	callsign:any;
	userModel: User = new User;
}