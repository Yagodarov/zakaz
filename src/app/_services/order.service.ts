import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Order } from '../_models/order';
@Injectable()
export class OrderService {
    constructor(private http: HttpClient) { }

    getAll(params = {}) {
        return this.http.get<Order[]>('/api/orders', params);
    }

    getPrice(model: Order, params = {}) {
      return this.http.post<any[]>('/api/order-price', model);
    }

    getDriverPrice(model: Order, params = {}) {
      return this.http.post<any[]>('/api/order-driver-price', model);
    }

    status(model: Order){
        return this.http.post<any[]>('/api/order-status', model);
    }

    decline(model: Order){
        return this.http.put<any[]>('/api/order-decline', model);
    }

    cancelOrder(id){
        return this.http.get<Order>('/api/order-cancel/' + id);
    }

    getById(id: number) {
        return this.http.get<Order>('/api/order/' + id);
    }

    getByIdSelect(id: number) {
        return this.http.get<Order>('/api/order-select/' + id);
    }

    create(model: Order) {
        return this.http.post('/api/order', model);
    }

    createSelect(model: Order) {
        return this.http.post('/api/order-select', model);
    }

    update(model: Order) {
        return this.http.put('/api/order/' + model.id, model);
    }

    updateSelect(model: Order) {
        return this.http.put('/api/order-select/' + model.id, model);
    }

    delete(id: number) {
        return this.http.delete('/api/order/' + id);
    }
}
