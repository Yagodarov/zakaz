import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Client } from '../_models';
import {DriverBalance} from '../_models/driver-balance';

@Injectable()
export class DriverBalanceService {
  private apiVersion = '/api/'
  private baseUrl = 'driver_balance/';
  private baseUrlList = 'driver_balances/';
  constructor(private http: HttpClient) { }

  getAll(params = {}) {
    return this.http.get<Client[]>(this.apiVersion + this.baseUrlList, params);
  }

  getById(id: number) {
    return this.http.get<Client>(this.apiVersion + this.baseUrl + id);
  }

  create(model: DriverBalance) {
    return this.http.post(this.apiVersion + this.baseUrl, DriverBalance);
  }

  update(model: DriverBalance) {
    return this.http.put(this.apiVersion + this.baseUrl, DriverBalance);
  }

  delete(id: number) {
    return this.http.delete(this.apiVersion + this.baseUrl + id);
  }
}
