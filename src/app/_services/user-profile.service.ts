import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Profile } from '../_models/profile';

@Injectable()
export class UserProfileService {
    constructor(private http: HttpClient) { }

    getById(id: number) {
        return this.http.get<Profile>('/api/profile/' + id);
    }

    create(profile: Profile) {
        return this.http.post('/api/profile', profile);
    }

    update(profile: Profile) {
        return this.http.put('/api/profile/' + profile.id, profile);
    }

    delete(id: number) {
        return this.http.delete('/api/profile/' + id);
    }
}