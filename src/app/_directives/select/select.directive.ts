import {Directive, ElementRef, Renderer2} from '@angular/core';
import {MapboxComponent} from '../../modals/mapbox/mapbox.component';
import {MatDialog} from '@angular/material';

@Directive({
  selector: '[appSelectDirective]',
})
export class DadataSelectDirective {
  protected div: HTMLDivElement;
  protected divList: HTMLDivElement;

  protected input: HTMLInputElement;

  constructor(private elementRef: ElementRef, private renderer: Renderer2, public dialog: MatDialog) {
    this.generateElements(elementRef);
    this.hideSelectOnClick();
  }


  public generateElements(el: ElementRef) {
    const element = this.elementRef.nativeElement;
    this.divList = document.createElement('div');
    this.input = element;

    this.input.className = 'form-control';
    this.input.style.zIndex = '1';

    this.input.addEventListener('input', (event) => {
      if (event['additional'] && event['additional'] === 'address') {
        return;
      }
      this.handleChanges(event);
    });

    this.input.onfocus = (event) => {
      this.showDivList();
    };

    this.divList.className = 'suggest-item-container';
    this.divList.setAttribute('for', this.elementRef.nativeElement.id);
    this.divList.onclick = (event) => {
      const elementS = event.target as HTMLDivElement;
      const value = elementS.innerHTML;
      this.input.value = value;
      this.divList.style.display = 'none';
      this.dispatchEvent();
    };

    const openModal = document.createElement('div');
    openModal.setAttribute('class', 'mapbox-link');
    openModal.innerHTML = 'Выбрать на карте';
    openModal.addEventListener('click', () => {
        this.openMapBoxModal();
    });
    element.after(openModal);
    element.after(this.divList);
  }

  public openMapBoxModal() {
      const dialogRef = this.dialog.open(MapboxComponent, {
          height: '430px',
          width: '650px',
      });
      dialogRef.afterClosed().subscribe((data) => {
          this.input.value = data;
          this.dispatchEvent();
      });
  }

  public handleChanges(event: Event) {
    const element = event.target as HTMLInputElement;
    const value = element.value;
    this.getSuggestions(value);
  }

  public getSuggestions(text: string) {
    const url = 'https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/address';
    const token = '6ffd71eeb0a996994d2dc9be555c68a962a44c6c';
    const options: RequestInit = {
      method: 'POST',
      mode: 'cors',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Token ' + token
      },
      body: JSON.stringify({query: text})
    };

    fetch(url, options)
      .then(response => response.json())
      .then(result => {
        this.divList.innerHTML = '';
        this.showDivList();
        result.suggestions.forEach((item) => {
          const option = document.createElement('div');
          option.className = 'suggest-item';
          option.innerHTML = item.value;
          this.divList.appendChild(option);
        });

      })
      .catch(error => console.log('error', error));
  }

  public hideSelectOnClick() {
    document.addEventListener('click', (event) => {
      const element = event.target as HTMLElement;
      if (!element.id) {
        this.divList.style.display = 'none';
      }
    });
  }

  protected showDivList() {
    this.divList.style.display = 'block';
  }

  public dispatchEvent() {
    const event: Event = document.createEvent('Event');
    event.initEvent('input', true, true);
    Object.defineProperty(event, 'target', {value: this.elementRef.nativeElement, enumerable: true});
    Object.defineProperty(event, 'additional', {value: 'address'});
    this.elementRef.nativeElement.dispatchEvent(event);
  }
}
