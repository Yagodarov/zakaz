﻿import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {CalendarModule} from 'angular-calendar';
import {AppComponent} from './app.component';
import {routing} from './app.routing';

import {AlertComponent, DadataSelectDirective} from './_directives';
import {AuthGuard} from './_guards';
import {JwtInterceptor} from './_helpers';
// import services
import {
    AlertService,
    AuthenticationService,
    CarService,
    ClientsService,
    OrderService,
    UserCarsService,
    UserContractService,
    UserProfileService,
    UserScheduleService,
    UserService
} from './_services';
import {HomeComponent} from './pages/home';
import {LoginComponent} from './pages/login';
import {RegisterComponent} from './pages/register';
import {UsersResolver} from './_resolvers/user.resolve';
import {UserCarsResolver} from './_resolvers/user.cars';
import {DriversResolver} from './_resolvers/driver';
import {UserNavigationResolver} from './_resolvers/user.navigation';
import {NavigationComponent} from './pages/nav/navigation.component';
import {UserComponent} from './pages/user/user.component';
import {UserEditComponent} from './pages/user/user-edit/user-edit.component';
import {UserEditResolver} from './_resolvers/user.edit';
import {UserContractResolver} from './_resolvers/user.contract';
import {LegalsResolver} from './_resolvers/legal';
import {UserNavigationComponent} from './pages/user/user-navigation/user-navigation.component';
import {UserProfileComponent} from './pages/user/user-profile/user-profile.component';
import {UserProfileResolver} from './_resolvers/user.profile';
import {UserScheduleResolver} from './_resolvers/user.schedule';
import {OrdersResolver} from './_resolvers/order.resolve';
import {OrderEditResolver} from './_resolvers/order.edit';
import {OrdersComponent} from './pages/order/orders/orders.component';
import {OrderSelectEditResolver} from './_resolvers/order.select.edit';
import {OrderCreateComponent} from './pages/order/order-create/order-create.component';
import {OrderEditComponent} from './pages/order/order-edit/order-edit.component';
import {UserCarsComponent} from './pages/user/user-cars/user-cars.component';
import {UserScheduleComponent} from './pages/user/user-schedule/user-schedule.component';
import {StorageHelper} from './_helpers/storage.helper';
import {SafeHtmlPipe} from './_helpers/safe.html';
import {UserContractsComponent} from './pages/user/user-contracts/user-contracts.component';
import {OrderSelectComponent} from './pages/order/order-select/order-select.component';
import {OrderSelectEditComponent} from './pages/order/order-select-edit/order-select-edit.component';
import {OrderSelectEditAdminComponent} from './pages/order/order-select-edit/components/osea/order-select-edit-admin.component';
import {OrderSelectEditLegalComponent} from './pages/order/order-select-edit/components/osel/order-select-edit-legal.component';
import {NotFoundComponent} from './pages/not-found/not-found.component';
import {OrderAdminComponent} from './pages/order/orders/order-admin/order-admin.component';
import {OrderDriverComponent} from './pages/order/orders/order-driver/order-driver.component';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {OrderLegalComponent} from './pages/order/orders/order-legal/order-legal.component';
import {CarsListResolver} from './_resolvers/car.list';
import {PartialsModule} from './partials/partials.module';
import {OrderClientEditComponent} from './pages/order/order-client-edit/order-client-edit.component';
import {ClientsResolver} from './_resolvers/clients';
import {CashBoxSettingsComponent} from './pages/cash-box-settings/cash-box-settings.component';
import { MapboxComponent } from './modals/mapbox/mapbox.component';
import {MatButtonModule, MatDialogModule} from '@angular/material';

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        HttpClientModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        NgxDatatableModule,
        CalendarModule.forRoot(),
        PartialsModule,
        MatDialogModule,
        routing,
        MatButtonModule,
    ],
    declarations: [
        SafeHtmlPipe,
        DadataSelectDirective,
        AppComponent,
        AlertComponent,
        HomeComponent,
        LoginComponent,
        RegisterComponent,
        NavigationComponent,
        UserComponent,
        UserEditComponent,
        UserNavigationComponent,
        UserProfileComponent,
        OrdersComponent,
        OrderCreateComponent,
        OrderEditComponent,
        OrderDriverComponent,
        UserCarsComponent,
        UserScheduleComponent,
        UserContractsComponent,
        OrderSelectComponent,
        OrderSelectEditComponent,
        OrderSelectEditAdminComponent,
        OrderSelectEditLegalComponent,
        NotFoundComponent,
        OrderAdminComponent,
        OrderLegalComponent,
        OrderClientEditComponent,
        CashBoxSettingsComponent,
        MapboxComponent
    ],
    entryComponents: [MapboxComponent],
    providers: [
        StorageHelper,
        AuthGuard,
        AlertService,
        AuthenticationService,
        CarService,
        ClientsService,
        UserProfileService,
        UserContractService,
        UserService,
        UsersResolver,
        UserEditResolver,
        UserCarsResolver,
        UserProfileResolver,
        UserNavigationResolver,
        UserCarsService,
        OrderService,
        ClientsResolver,
        OrdersResolver,
        OrderEditResolver,
        OrderSelectEditResolver,
        UserScheduleService,
        UserScheduleResolver,
        DriversResolver,
        LegalsResolver,
        UserContractResolver,
        CarsListResolver,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: JwtInterceptor,
            multi: true
        },
    ],
    bootstrap: [AppComponent]
})

export class AppModule {
}
