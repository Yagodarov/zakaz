@echo off
echo.
del *bundle*
del *styles.*
del *main.*
del *polyfills.*
del *txt*
del *woff*
del *woff2*
del *eot*
del *ttf*
del *svg*
del *css*
del *.js
del *.png
call ng build --prod
ROBOCOPY dist/ . /s /e /it
rmdir /s /q dist

GOTO DONE

:UNKNOWN
Echo Error no commit comment
goto:eof

:DONE
ECHO Done!
goto:eof
